const { authenticateCustomer } = require('../middleware/auth');
const { Cart } = require('../controllers');
module.exports = (app, passport) => {
    app.route('/api/currentCustomerCart')
        .get( authenticateCustomer, Cart.findCustomerBasket)
        .post( authenticateCustomer, Cart.addToCart)
        .delete( authenticateCustomer, Cart.removeFromCart);
    app.route('/api/updateCartItemCount')
        .put(authenticateCustomer, Cart.updateCount);
}