const { authenticateUser } = require('../middleware/auth');
const { Delivery } = require('../controllers');
module.exports = (app, passport) => {
    app.route('/api/delivery')
        .get( Delivery.find)
            /**
             * @api {get} /api/delivery Fetch deliveries
             * @apiName FetchDelivery
             * @apiGroup Delivery
             * @apiPermission none
             * @apiSampleRequest http://localhost:8080/api/delivery
             * 
             * @apiSuccess {String} id  objectID()
             * @apiSuccess {String} name    Name of delivery
             * @apiSuccess {Bool}   enable  Show or hide delivery
             * @apiSuccess {Object} image   Delivery image
             * @apiSuccess {String} image.target Image objectID()
             * @apiSuccess {Object} image.attributes Image atrybutes
             * @apiSuccess {String} image.attributes.alt Alt of image
             * @apiSuccess {String} image.attributes.title Title of image    
             * @apiSuccess {String} price   Price of delivery
             * @apiSuccess {Number} weight  Weight
             * 
             * @apiVersion 1.0.0
             * 
             * @apiSuccessExample {json} Success
             * HTTP/1.1 200 OK
             *
             *  {
             *      succes: true,
             *      doc: [{
             *          "id": "5a95a2921f4de421bec83ed1",
             *          "name": "DPD",
             *          "enable": true,
             *          "image": {
             *              "target": "5a95a2921f4de421bec83ed1",
             *              "attributes": {
             *                  "alt": "shoes",
             *                  "title": "avesome title"
             *              }
             *          },
             *          "price": "11"
             *          "weight": 0,
             *      }]
             *   }
             *
             * @apiErrorExample {json} List error
             *    HTTP/1.1 500 Internal Server Error
             */
        .post(authenticateUser, Delivery.create)
            /**
             * @api {post} /api/delivery Add delivery
             * @apiName AddDelivery
             * @apiGroup Delivery
             * @apiPermission admin, moderator
             * 
             * @apiParam (JSON) {String} name    Name of delivery
             * @apiParam (JSON) {Bool}   enable=true  Show or hide delivery
             * @apiParam (JSON) {Object} [image]   Delivery image
             * @apiParam (JSON) {String} [image.target] Image objectID()
             * @apiParam (JSON) {Object} [image.attributes] Image atrybutes
             * @apiParam (JSON) {String} [image.attributes.alt] Alt of image
             * @apiParam (JSON) {String} [image.attributes.title] Title of image
             * @apiParam (JSON) {String} [price] Delivery price
             * @apiParam (JSON) {Number} [weight]  Weight
             * 
             * @apiParamExample {json} Request-Example:
             * 
             *     {
             *       "Name": "Buty"
             *     }
             * 
             * @apiSampleRequest http://localhost:8080/api/delivery
             * 
             * @apiSuccess {String} id  objectID()
             * @apiSuccess {String} name    Name of delivery
             * @apiSuccess {Bool}   enable  Show or hide delivery
             * @apiSuccess {Object} image   Delivery image
             * @apiSuccess {String} image.target Image objectID()
             * @apiSuccess {Object} image.attributes Image atrybutes
             * @apiSuccess {String} image.attributes.alt Alt of image
             * @apiSuccess {String} image.attributes.title Title of image    
             * @apiSuccess {String} price   Price of delivery
             * @apiSuccess {Number} weight  Weight
             * 
             * @apiVersion 1.0.0
             * 
             * @apiSuccessExample {json} Success
             * HTTP/1.1 200 OK
             *
             *  {
             *      succes: true,
             *      doc: [{
             *          "id": "5a95a2921f4de421bec83ed1",
             *          "name": "DPD",
             *          "enable": true,
             *          "image": {
             *              "target": "5a95a2921f4de421bec83ed1",
             *              "attributes": {
             *                  "alt": "shoes",
             *                  "title": "avesome title"
             *              }
             *          },
             *          "price": "11"
             *          "weight": 0,
             *      }]
             *   }
             *
             * @apiErrorExample {json} List error
             *    HTTP/1.1 500 Internal Server Error
             */
    app.route('/api/delivery/:id')
        .get(Delivery.findOne)
            /**
             * @api {get} /api/delivery/:id Get delivery
             * @apiName GetDelivery
             * @apiGroup Delivery
             * @apiPermission none
             * @apiSampleRequest http://localhost:8080/api/delivery
             * 
             * @apiParam {String} id Delivery unique ID.
             * 
             * @apiSuccess {String} id  objectID()
             * @apiSuccess {String} name    Name of delivery
             * @apiSuccess {Bool}   enable  Show or hide delivery
             * @apiSuccess {Object} image   Delivery image
             * @apiSuccess {String} image.target Image objectID()
             * @apiSuccess {Object} image.attributes Image atrybutes
             * @apiSuccess {String} image.attributes.alt Alt of image
             * @apiSuccess {String} image.attributes.title Title of image    
             * @apiSuccess {String} price   Price of delivery
             * @apiSuccess {Number} weight  Weight
             * 
             * @apiVersion 1.0.0
             * 
             * @apiSuccessExample {json} Success
             * HTTP/1.1 200 OK
             *
             *  {
             *      succes: true,
             *      doc: {
             *          "id": "5a95a2921f4de421bec83ed1",
             *          "name": "DPD",
             *          "enable": true,
             *          "image": {
             *              "target": "5a95a2921f4de421bec83ed1",
             *              "attributes": {
             *                  "alt": "shoes",
             *                  "title": "avesome title"
             *              }
             *          },
             *          "price": "11"
             *          "weight": 0,
             *      }
             *   }
             *
             * @apiErrorExample {json} List error
             *    HTTP/1.1 500 Internal Server Error
             */
        .put(authenticateUser, Delivery.update)
        /**
             * @api {put} /api/delivery/:id Update delivery
             * @apiName UpdateDelivery
             * @apiGroup Delivery
             * @apiPermission admin, moderator
             * 
             * @apiParam {String} id Delivery unique ID.
             * 
             * @apiParam (JSON) {String} [name]    Name of delivery
             * @apiParam (JSON) {Bool}   [enable]  Show or hide delivery
             * @apiParam (JSON) {Object} [image]   Delivery image
             * @apiParam (JSON) {String} [image.target] Image objectID()
             * @apiParam (JSON) {Object} [image.attributes] Image atrybutes
             * @apiParam (JSON) {String} [image.attributes.alt] Alt of image
             * @apiParam (JSON) {String} [image.attributes.title] Title of image
             * @apiParam (JSON) {String} [price] Delivery price
             * @apiParam (JSON) {Number} [weight]  Weight
             * 
             * @apiParamExample {json} Request-Example:
             * 
             *     {
             *       "Name": "DPD"
             *     }
             * 
             * @apiSampleRequest http://localhost:8080/api/delivery
             * 
             * @apiSuccess {String} id  objectID()
             * @apiSuccess {String} name    Name of delivery
             * @apiSuccess {Bool}   enable  Show or hide delivery
             * @apiSuccess {Object} image   Delivery image
             * @apiSuccess {String} image.target Image objectID()
             * @apiSuccess {Object} image.attributes Image atrybutes
             * @apiSuccess {String} image.attributes.alt Alt of image
             * @apiSuccess {String} image.attributes.title Title of image    
             * @apiSuccess {String} price   Price of delivery
             * @apiSuccess {Number} weight  Weight
             * 
             * @apiVersion 1.0.0
             * 
             * @apiSuccessExample {json} Success
             * HTTP/1.1 200 OK
             *
             *  {
             *      succes: true,
             *      doc: [{
             *          "id": "5a95a2921f4de421bec83ed1",
             *          "name": "DPD",
             *          "enable": true,
             *          "image": {
             *              "target": "5a95a2921f4de421bec83ed1",
             *              "attributes": {
             *                  "alt": "shoes",
             *                  "title": "avesome title"
             *              }
             *          },
             *          "price": "11"
             *          "weight": 0,
             *      }]
             *   }
             *
             * @apiErrorExample {json} List error
             *    HTTP/1.1 500 Internal Server Error
             */
        .delete(authenticateUser, Delivery.destroy)
            /**
             * @api {delete} /api/delivery/:id Delete delivery
             * @apiName DeleteDelivery
             * @apiGroup Delivery
             * @apiPermission none
             * @apiSampleRequest http://localhost:8080/api/delivery
             * 
             * @apiParam {String} id Delivery unique ID.
             * 
             * @apiSuccess {String} id  objectID()
             * @apiSuccess {String} name    Name of delivery
             * @apiSuccess {Bool}   enable  Show or hide delivery
             * @apiSuccess {Object} image   Delivery image
             * @apiSuccess {String} image.target Image objectID()
             * @apiSuccess {Object} image.attributes Image atrybutes
             * @apiSuccess {String} image.attributes.alt Alt of image
             * @apiSuccess {String} image.attributes.title Title of image    
             * @apiSuccess {String} price   Price of delivery
             * @apiSuccess {Number} weight  Weight
             * 
             * @apiVersion 1.0.0
             * 
             * @apiSuccessExample {json} Success
             * HTTP/1.1 200 OK
             *
             *  {
             *      succes: true,
             *      doc: {
             *          "id": "5a95a2921f4de421bec83ed1",
             *          "name": "DPD",
             *          "enable": true,
             *          "image": {
             *              "target": "5a95a2921f4de421bec83ed1",
             *              "attributes": {
             *                  "alt": "shoes",
             *                  "title": "avesome title"
             *              }
             *          },
             *          "price": "11"
             *          "weight": 0,
             *      }
             *   }
             *
             * @apiErrorExample {json} List error
             *    HTTP/1.1 500 Internal Server Error
             */
}