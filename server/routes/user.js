const { authenticateUser } = require('../middleware/auth');
const { Auth, User } = require('../controllers');
module.exports = (app, passport) => {
    app.route('/api/user')
        .get(User.find)
        .post(authenticateUser, User.create)
    app.route('/api/user/:id')
        .get(authenticateUser, User.findOne)
        .put(authenticateUser, User.update)
        .delete(authenticateUser, User.destroy)
}