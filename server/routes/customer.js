const { authenticateUser } = require('../middleware/auth');
const { Customer } = require('../controllers');
module.exports = (app, passport) => {
    app.route('/api/customer')
        .get( authenticateUser, Customer.find)
        .post(authenticateUser, Customer.create)
    app.route('/api/customer/:id')
        .get(authenticateUser, Customer.findOne)
        .put(authenticateUser, Customer.update)
        .delete(authenticateUser, Customer.destroy)
}