const { authenticateUser } = require('../middleware/auth');
const { Product } = require('../controllers');
module.exports = (app) => {
    app.route('/api/product')
            /**
         * @api {get} /api/products get products list
         * @apiGroup Products
         * @apiPermission none
         * @apiSuccess {String} id  objectID()
         * @apiSuccess {String} name    Product Name   
         * @apiSuccess {String} body    Product Description   
         * @apiSuccess {Bool} active Product Published
         * @apiSuccess {String} status  enum: "enabled", "disabled", "outOfStock"
         * @apiSuccess {String} categoryId  ID category
         * @apiSuccess {String} manufacturerId  ID manufacturer
         * @apiSuccess {Number} tax enum: 0.23, 0.07, 0.21
         * @apiSuccess {Object} created Data and user
         * @apiSuccess {String} created.userId  User Id
         * @apiSuccess {Date} created.date  Crated Date
         * @apiSuccess {Date} available     Available Date
         * @apiSuccess {Number} stock   Product stock
         * @apiSuccess {Number} price   Product Price
         * @apiSuccess {Object} promotion   product published
         * @apiSuccess {Bool} promotion.active  active promotion
         * @apiSuccess {Number} promotion.newPrice  active promotion
         * @apiSuccess {Date} promotion.startDate   Promotion started
         * @apiSuccess {Date} promotion.expiryDate  Promotion expiry
         * @apiSuccess {Object[]} images    Images
         * @apiSuccess {String} images.imageId  Image Id
         * @apiSuccess {String} images.alt  Image alt
         * @apiSuccess {String} images.title    Image title
         * @apiSuccess {String[]} relatedIds    Array of related Products
         * @apiSuccess {String} relatedIds  Array of related Products
         * @apiSuccess {String} path    path
         * @apiVersion 1.0.0
         * @apiSuccessExample {json} Success
         * HTTP/1.1 200 OK
         *
         *  {
         *      succes: true,
         *      doc: [{
         *          "id": "5a95a2921f4de421bec83ed1",
         *          "name": "Buty Adidas"
         *          "body": "ospi produktu"
         *          "active": true,
         *          "status": "enabled",
         *          "categoryId": "5a80a0dc37737c024b0ff4ca",
         *          "manufacturerId": "5a95a2921f4de421bec83ed1",
         *          "tax": 0.23,
         *          "created": {
         *              "userId": "5a95a2921f4de421bec83ed1",
         *              "date": "2018-03-24T11:38:04.360Z",
         *          },
         *          "available": "2018-03-24T11:38:04.360Z",
         *          "stock": 20,
         *          "price": 22.2,
         *          "promotion": {
         *              "active": false,
         *              "newPrice": 19,
         *              "startDate": "2018-03-24T11:38:04.360Z"
         *              "expiryDate": "2018-03-29T11:38:04.360Z"
         *          },
         *          "images": [
         *              {
         *                  "imageId": "5a95a2921f4de421bec83ed1",
         *                  "alt": "alt image",
         *                  "title": "title image"
         *              }
         *          ],
         *          "relatedIds": [
         *              "5a95a2921f4de421bec83ed1", "5a95a2921f4de421bec83ed1"
         *          ]
         *          "path": "/catalog/buty-adidas",
         *      }]
         *   }
         * @apiErrorExample {json} List error
         *    HTTP/1.1 500 Internal Server Error
         */
        .get( Product.find)
        .post( authenticateUser, Product.create)
    app.route('/api/product/:id')
        .get( Product.findOne )
        .put( authenticateUser, Product.update )
        .delete( authenticateUser, Product.destroy )
    app.route('/api/product/:id/review' )
        .get( Product.review)
    app.route('/api/product/:id/basket' )
        .get( authenticateUser, Product.basket )
    app.route('/api/product/:id/order')
        .get( authenticateUser, Product.order )
}