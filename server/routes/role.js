const { authenticateUser } = require('../middleware/auth');
const { Role } = require('../controllers');
module.exports = (app) => {
    app.route('/api/role')
        .get(authenticateUser, Role.find)
            /**
             * @api {get} /api/role Fetch Roles
             * @apiName FetchRole
             * @apiGroup Role
             * @apiPermission Admin
             * @apiSampleRequest http://localhost:8080/api/role
             * 
             * @apiSuccess {String} id  objectID()
             * @apiSuccess {String} name    Name of role
             * @apiSuccess {String} hash    Hash of role
             * 
             * @apiVersion 1.0.0
             * 
             * @apiSuccessExample {json} Success
             * HTTP/1.1 200 OK
             *
             *  {
             *      succes: true,
             *      doc: [{
             *          "id": "5a95a2921f4de421bec83ed1",
             *          "name": "Admin",
             *          "hash": "21232F297A57A5A743894A0E4A801FC3"
             *      }]
             *   }
             *
             * @apiErrorExample {json} List error
             *    HTTP/1.1 500 Internal Server Error
             */
        .post(authenticateUser, Role.create)
            /**
             * @api {post} /api/role Add role
             * @apiName AddRole
             * @apiGroup Role
             * @apiPermission admin, moderator
             * 
             * @apiParam (JSON) {String} name  Name of role
             * 
             * @apiParamExample {json} Request-Example:
             * 
             *     {
             *       "Name": "Admin"
             *     }
             * 
             * @apiSampleRequest http://localhost:8080/api/role
             * 
             * @apiSuccess {String} id  objectID()
             * @apiSuccess {String} name    Name of role
             * @apiSuccess {String} hash    Hash of role
             * 
             * @apiVersion 1.0.0
             * 
             * @apiSuccessExample {json} Success
             * HTTP/1.1 200 OK
             *
             *  {
             *      succes: true,
             *      doc: {
             *          "id": "5a95a2921f4de421bec83ed1",
             *          "name": "Admin",
             *          "hash": "21232F297A57A5A743894A0E4A801FC3"
             *      }
             *   }
             *
             * @apiErrorExample {json} List error
             *    HTTP/1.1 500 Internal Server Error
             */
    app.route('/api/role/:id')
        .get(authenticateUser, Role.findOne)
            /**
             * @api {get} /api/role/:id Get role
             * @apiName GetRole
             * @apiGroup Role
             * @apiPermission none
             * @apiSampleRequest http://localhost:8080/api/role
             * 
             * @apiParam {String} id Role unique ID.
             * 
             * @apiSuccess {String} id  objectID()
             * @apiSuccess {String} name    Name of role
             * @apiSuccess {String} hash    Hash of role
             * 
             * @apiVersion 1.0.0
             * 
             * @apiSuccessExample {json} Success
             * HTTP/1.1 200 OK
             *
             *  {
             *      succes: true,
             *      doc: {
             *          "id": "5a95a2921f4de421bec83ed1",
             *          "name": "Admin",
             *          "hash": "21232F297A57A5A743894A0E4A801FC3"
             *      }
             *   }
             *
             * @apiErrorExample {json} List error
             *    HTTP/1.1 500 Internal Server Error
             */
        .put(authenticateUser, Role.update)
        /**
             * @api {put} /api/role/:id Update role
             * @apiName UpdateRole
             * @apiGroup Role
             * @apiPermission admin, moderator
             * 
             * @apiParam {String} id Role unique ID.
             * 
             * @apiParam (JSON) {String} [name]    Name of role
             * 
             * @apiParamExample {json} Request-Example:
             * 
             *     {
             *       "Name": "Moderator"
             *     }
             * 
             * @apiSampleRequest http://localhost:8080/api/role
             * 
             * @apiSuccess {String} id  objectID()
             * @apiSuccess {String} name    Name of role
             * @apiSuccess {String} hash    Hash of role 
             * 
             * @apiVersion 1.0.0
             * 
             * @apiSuccessExample {json} Success
             * HTTP/1.1 200 OK
             *
             *  {
             *      succes: true,
             *      doc: {
             *          "id": "5a95a2921f4de421bec83ed1",
             *          "name": "Admin",
             *          "hash": "21232F297A57A5A743894A0E4A801FC3"
             *      }
             *   }
             *
             * @apiErrorExample {json} List error
             *    HTTP/1.1 500 Internal Server Error
             */
        .delete(authenticateUser, Role.destroy)
            /**
             * @api {delete} /api/role/:id Delete role
             * @apiName DeleteRole
             * @apiGroup Role
             * @apiPermission none
             * @apiSampleRequest http://localhost:8080/api/role
             * 
             * @apiParam {String} id Role unique ID.
             * 
             * @apiSuccess {String} id  objectID()
             * @apiSuccess {String} name    Name of role
             * @apiSuccess {String} hash    Hash of role 
             * 
             * @apiVersion 1.0.0
             * 
             * @apiSuccessExample {json} Success
             * HTTP/1.1 200 OK
             *
             *  {
             *      succes: true,
             *      doc: {
             *          "id": "5a95a2921f4de421bec83ed1",
             *          "name": "Admin",
             *          "hash": "21232F297A57A5A743894A0E4A801FC3"
             *      }
             *   }
             *
             * @apiErrorExample {json} List error
             *    HTTP/1.1 500 Internal Server Error
             */
}