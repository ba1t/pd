const { authenticateUser } = require('../middleware/auth');
const { Image } = require('../controllers');
module.exports = (app, passport) => {
    app.route('/api/image')
        .post(authenticateUser, Image.create)
    app.route('/api/image/:id')
        .get(Image.findOne)
}