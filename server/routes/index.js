module.exports = (app) => { 
    require('./auth')(app);
    require('./user')(app);
    require('./product')(app);
    require('./tax')(app);
    require('./category')(app);
    require('./role')(app);
    require('./manufacturer')(app);
    require('./customer')(app);
    require('./delivery')(app);
    require('./status')(app);
    require('./image')(app);
    require('./payment')(app);
    require('./cart')(app);
 }