const { authenticateUser } = require('../middleware/auth');
const { Category } = require('../controllers');
module.exports = (app, passport) => {
    app.route('/api/category')
        .get( Category.find)
            /**
             * @api {get} /api/category Fetch categories
             * @apiName FetchCategory
             * @apiGroup Category
             * @apiPermission none
             * @apiSampleRequest http://localhost:8080/api/category
             * 
             * @apiSuccess {String} id  objectID()
             * @apiSuccess {String} name    Name of category
             * @apiSuccess {Bool}   enable  Show or hide category
             * @apiSuccess {Object} image   Category image
             * @apiSuccess {String} image.target Image objectID()
             * @apiSuccess {Object} image.attributes Image atrybutes
             * @apiSuccess {String} image.attributes.alt Alt of image
             * @apiSuccess {String} image.attributes.title Title of image    
             * @apiSuccess {Number} weight  Weight
             * @apiSuccess {String} parentCategory  Parent objectID()      
             * @apiSuccess {String} path    path
             * 
             * @apiVersion 1.0.0
             * 
             * @apiSuccessExample {json} Success
             * HTTP/1.1 200 OK
             *
             *  {
             *      succes: true,
             *      doc: [{
             *          "id": "5a95a2921f4de421bec83ed1",
             *          "name": "shoes",
             *          "enable": true,
             *          "image": {
             *              "target": "5a95a2921f4de421bec83ed1",
             *              "attributes": {
             *                  "alt": "shoes",
             *                  "title": "avesome title"
             *              }
             *          },
             *          "weight": 0,
             *          "parentCategory": "5a95a2921f4de421bec83ed1",
             *          "path": "/catalog/shoes",
             *      }]
             *   }
             *
             * @apiErrorExample {json} List error
             *    HTTP/1.1 500 Internal Server Error
             */
        .post(authenticateUser, Category.create)
            /**
             * @api {post} /api/category Create category
             * @apiGroup Category
             * @apiPermission admin, moderator
             * 
             * @apiParam (JSON) {String} name    Name of category
             * @apiParam (JSON) {Bool}   enable=true  Show or hide category
             * @apiParam (JSON) {Object} [image]   Category image
             * @apiParam (JSON) {String} [image.target] Image objectID()
             * @apiParam (JSON) {Object} [image.attributes] Image atrybutes
             * @apiParam (JSON) {String} [image.attributes.alt] Alt of image
             * @apiParam (JSON) {String} [image.attributes.title] Title of image    
             * @apiParam (JSON) {Number} [weight]  Weight
             * @apiParam (JSON) {String} [parentCategory]  Parent objectID()      
             * @apiParam (JSON) {String} [path]    path
             * 
             * @apiParamExample {json} Request-Example:
             * 
             *     {
             *       "Name": "Buty"
             *     }
             * 
             * @apiSampleRequest http://localhost:8080/api/category
             * @apiSuccess {String} id  objectID()
             * @apiSuccess {Bool}   enable  Show or hide category
             * @apiSuccess {String} name    Name             
             * @apiSuccess {String} path    path
             * @apiSuccess {Number} weight  Weight
             * @apiVersion 1.0.0
             * 
             * @apiSuccessExample {json} Success
             * HTTP/1.1 200 OK
             *
             *  {
             *      succes: true,
             *      doc: {
             *          "id": "5a95a2921f4de421bec83ed1",
             *          "enable": true,
             *          "name": "shoes",
             *          "path": "/catalog/shoes",
             *          "weight": 0
             *      }
             *   }
             *
             * @apiErrorExample {json} List error
             *    HTTP/1.1 500 Internal Server Error
             */
    app.route('/api/category/:id')
        .get(Category.findOne)
            /**
             * @api {get} /api/category/:id Get Category
             * @apiName GetCategory
             * @apiGroup Category
             * @apiPermission none
             * @apiSampleRequest http://localhost:8080/api/category
             * 
             * @apiParam {String} id Users unique ID.
             * 
             * @apiSuccess {String} id  objectID()
             * @apiSuccess {String} name    Name of category
             * @apiSuccess {Bool}   enable  Show or hide category
             * @apiSuccess {Object} image   Category image
             * @apiSuccess {String} image.target Image objectID()
             * @apiSuccess {Object} image.attributes Image atrybutes
             * @apiSuccess {String} image.attributes.alt Alt of image
             * @apiSuccess {String} image.attributes.title Title of image    
             * @apiSuccess {Number} weight  Weight
             * @apiSuccess {String} parentCategory  Parent objectID()      
             * @apiSuccess {String} path    path
             * 
             * @apiVersion 1.0.0
             * 
             * @apiSuccessExample {json} Success
             * HTTP/1.1 200 OK
             *
             *  {
             *      succes: true,
             *      doc: {
             *          "id": "5a95a2921f4de421bec83ed1",
             *          "name": "shoes",
             *          "enable": true,
             *          "image": {
             *              "target": "5a95a2921f4de421bec83ed1",
             *              "attributes": {
             *                  "alt": "shoes",
             *                  "title": "avesome title"
             *              }
             *          },
             *          "weight": 0,
             *          "parentCategory": "5a95a2921f4de421bec83ed1",
             *          "path": "/catalog/shoes",
             *      }
             *   }
             *
             * @apiErrorExample {json} List error
             *    HTTP/1.1 500 Internal Server Error
             */
        .put(authenticateUser, Category.update)
            /**
             * @api {put} /api/category/:id Update category
             * @apiName UpdateCategory
             * @apiGroup Category
             * @apiPermission Admin, Moderator
             * @apiSampleRequest http://localhost:8080/api/category
             * 
             * @apiParam {String} id Users unique ID.
             * @apiParam (JSON) {String} [name]    Name of category
             * @apiParam (JSON) {Bool}   [enable]  Show or hide category
             * @apiParam (JSON) {Object} [image]   Category image
             * @apiParam (JSON) {String} [image.target] Image objectID()
             * @apiParam (JSON) {Object} [image.attributes] Image atrybutes
             * @apiParam (JSON) {String} [image.attributes.alt] Alt of image
             * @apiParam (JSON) {String} [image.attributes.title] Title of image    
             * @apiParam (JSON) {Number} [weight]  Weight
             * @apiParam (JSON) {String} [parentCategory]  Parent objectID()      
             * @apiParam (JSON) {String} [path]    path
             * 
             * @apiParamExample {json} Request-Example:
             * 
             *     {
             *       "Name": "Buty"
             *     }
             * 
             * @apiSuccess {String} id  objectID()
             * @apiSuccess {String} name    Name of category
             * @apiSuccess {Bool}   enable  Show or hide category
             * @apiSuccess {Object} image   Category image
             * @apiSuccess {String} image.target Image objectID()
             * @apiSuccess {Object} image.attributes Image atrybutes
             * @apiSuccess {String} image.attributes.alt Alt of image
             * @apiSuccess {String} image.attributes.title Title of image    
             * @apiSuccess {Number} weight  Weight
             * @apiSuccess {String} parentCategory  Parent objectID()      
             * @apiSuccess {String} path    path
             * 
             * @apiVersion 1.0.0
             * 
             * @apiSuccessExample {json} Success
             * HTTP/1.1 200 OK
             *
             *  {
             *      succes: true,
             *      doc: {
             *          "id": "5a95a2921f4de421bec83ed1",
             *          "name": "shoes",
             *          "enable": true,
             *          "image": {
             *              "target": "5a95a2921f4de421bec83ed1",
             *              "attributes": {
             *                  "alt": "shoes",
             *                  "title": "avesome title"
             *              }
             *          },
             *          "weight": 0,
             *          "parentCategory": "5a95a2921f4de421bec83ed1",
             *          "path": "/catalog/shoes",
             *      }
             *   }
             *
             * @apiErrorExample {json} List error
             *    HTTP/1.1 500 Internal Server Error
             */
        .delete(authenticateUser, Category.destroy)
            /**
             * @api {delete} /api/category/:id Delete Category
             * @apiName DeleteCategory
             * @apiGroup Category
             * @apiPermission Admin, Moderator
             * @apiSampleRequest http://localhost:8080/api/category
             * 
             * @apiParam {String} id Users unique ID.
             * 
             * @apiSuccess {String} id  objectID()
             * @apiSuccess {String} name    Name of category
             * @apiSuccess {Bool}   enable  Show or hide category
             * @apiSuccess {Object} image   Category image
             * @apiSuccess {String} image.target Image objectID()
             * @apiSuccess {Object} image.attributes Image atrybutes
             * @apiSuccess {String} image.attributes.alt Alt of image
             * @apiSuccess {String} image.attributes.title Title of image    
             * @apiSuccess {Number} weight  Weight
             * @apiSuccess {String} parentCategory  Parent objectID()      
             * @apiSuccess {String} path    path
             * 
             * @apiVersion 1.0.0
             * 
             * @apiSuccessExample {json} Success
             * HTTP/1.1 200 OK
             *
             *  {
             *      succes: true,
             *      doc: {
             *          "id": "5a95a2921f4de421bec83ed1",
             *          "name": "shoes",
             *          "enable": true,
             *          "image": {
             *              "target": "5a95a2921f4de421bec83ed1",
             *              "attributes": {
             *                  "alt": "shoes",
             *                  "title": "avesome title"
             *              }
             *          },
             *          "weight": 0,
             *          "parentCategory": "5a95a2921f4de421bec83ed1",
             *          "path": "/catalog/shoes",
             *      }
             *   }
             *
             * @apiErrorExample {json} List error
             *    HTTP/1.1 500 Internal Server Error
             */
}