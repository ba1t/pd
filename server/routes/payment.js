const { authenticateUser } = require('../middleware/auth');
const { Payment } = require('../controllers');
module.exports = (app, passport) => {
    app.route('/api/payment')
        .get( Payment.find)
            /**
             * @api {get} /api/payment Fetch deliveries
             * @apiName FetchPayment
             * @apiGroup Payment
             * @apiPermission none
             * @apiSampleRequest http://localhost:8080/api/payment
             * 
             * @apiSuccess {String} id  objectID()
             * @apiSuccess {String} name    Name of payment
             * @apiSuccess {Bool}   enable  Show or hide payment
             * @apiSuccess {Object} image   Payment image
             * @apiSuccess {String} image.target Image objectID()
             * @apiSuccess {Object} image.attributes Image atrybutes
             * @apiSuccess {String} image.attributes.alt Alt of image
             * @apiSuccess {String} image.attributes.title Title of image    
             * @apiSuccess {String} price   Price of payment
             * @apiSuccess {Number} weight  Weight
             * 
             * @apiVersion 1.0.0
             * 
             * @apiSuccessExample {json} Success
             * HTTP/1.1 200 OK
             *
             *  {
             *      succes: true,
             *      doc: [{
             *          "id": "5a95a2921f4de421bec83ed1",
             *          "name": "DPD",
             *          "enable": true,
             *          "image": {
             *              "target": "5a95a2921f4de421bec83ed1",
             *              "attributes": {
             *                  "alt": "shoes",
             *                  "title": "avesome title"
             *              }
             *          },
             *          "price": "11"
             *          "weight": 0,
             *      }]
             *   }
             *
             * @apiErrorExample {json} List error
             *    HTTP/1.1 500 Internal Server Error
             */
        .post(authenticateUser, Payment.create)
            /**
             * @api {post} /api/payment Add payment
             * @apiName AddPayment
             * @apiGroup Payment
             * @apiPermission admin, moderator
             * 
             * @apiParam (JSON) {String} name    Name of payment
             * @apiParam (JSON) {Bool}   enable=true  Show or hide payment
             * @apiParam (JSON) {Object} [image]   Payment image
             * @apiParam (JSON) {String} [image.target] Image objectID()
             * @apiParam (JSON) {Object} [image.attributes] Image atrybutes
             * @apiParam (JSON) {String} [image.attributes.alt] Alt of image
             * @apiParam (JSON) {String} [image.attributes.title] Title of image
             * @apiParam (JSON) {String} [price] Payment price
             * @apiParam (JSON) {Number} [weight]  Weight
             * 
             * @apiParamExample {json} Request-Example:
             * 
             *     {
             *       "Name": "Buty"
             *     }
             * 
             * @apiSampleRequest http://localhost:8080/api/payment
             * 
             * @apiSuccess {String} id  objectID()
             * @apiSuccess {String} name    Name of payment
             * @apiSuccess {Bool}   enable  Show or hide payment
             * @apiSuccess {Object} image   Payment image
             * @apiSuccess {String} image.target Image objectID()
             * @apiSuccess {Object} image.attributes Image atrybutes
             * @apiSuccess {String} image.attributes.alt Alt of image
             * @apiSuccess {String} image.attributes.title Title of image    
             * @apiSuccess {String} price   Price of payment
             * @apiSuccess {Number} weight  Weight
             * 
             * @apiVersion 1.0.0
             * 
             * @apiSuccessExample {json} Success
             * HTTP/1.1 200 OK
             *
             *  {
             *      succes: true,
             *      doc: [{
             *          "id": "5a95a2921f4de421bec83ed1",
             *          "name": "DPD",
             *          "enable": true,
             *          "image": {
             *              "target": "5a95a2921f4de421bec83ed1",
             *              "attributes": {
             *                  "alt": "shoes",
             *                  "title": "avesome title"
             *              }
             *          },
             *          "price": "11"
             *          "weight": 0,
             *      }]
             *   }
             *
             * @apiErrorExample {json} List error
             *    HTTP/1.1 500 Internal Server Error
             */
    app.route('/api/payment/:id')
        .get(Payment.findOne)
            /**
             * @api {get} /api/payment/:id Get payment
             * @apiName GetPayment
             * @apiGroup Payment
             * @apiPermission none
             * @apiSampleRequest http://localhost:8080/api/payment
             * 
             * @apiParam {String} id Payment unique ID.
             * 
             * @apiSuccess {String} id  objectID()
             * @apiSuccess {String} name    Name of payment
             * @apiSuccess {Bool}   enable  Show or hide payment
             * @apiSuccess {Object} image   Payment image
             * @apiSuccess {String} image.target Image objectID()
             * @apiSuccess {Object} image.attributes Image atrybutes
             * @apiSuccess {String} image.attributes.alt Alt of image
             * @apiSuccess {String} image.attributes.title Title of image    
             * @apiSuccess {String} price   Price of payment
             * @apiSuccess {Number} weight  Weight
             * 
             * @apiVersion 1.0.0
             * 
             * @apiSuccessExample {json} Success
             * HTTP/1.1 200 OK
             *
             *  {
             *      succes: true,
             *      doc: {
             *          "id": "5a95a2921f4de421bec83ed1",
             *          "name": "DPD",
             *          "enable": true,
             *          "image": {
             *              "target": "5a95a2921f4de421bec83ed1",
             *              "attributes": {
             *                  "alt": "shoes",
             *                  "title": "avesome title"
             *              }
             *          },
             *          "price": "11"
             *          "weight": 0,
             *      }
             *   }
             *
             * @apiErrorExample {json} List error
             *    HTTP/1.1 500 Internal Server Error
             */
        .put(authenticateUser, Payment.update)
        /**
             * @api {put} /api/payment/:id Update payment
             * @apiName UpdatePayment
             * @apiGroup Payment
             * @apiPermission admin, moderator
             * 
             * @apiParam {String} id Payment unique ID.
             * 
             * @apiParam (JSON) {String} [name]    Name of payment
             * @apiParam (JSON) {Bool}   [enable]  Show or hide payment
             * @apiParam (JSON) {Object} [image]   Payment image
             * @apiParam (JSON) {String} [image.target] Image objectID()
             * @apiParam (JSON) {Object} [image.attributes] Image atrybutes
             * @apiParam (JSON) {String} [image.attributes.alt] Alt of image
             * @apiParam (JSON) {String} [image.attributes.title] Title of image
             * @apiParam (JSON) {String} [price] Payment price
             * @apiParam (JSON) {Number} [weight]  Weight
             * 
             * @apiParamExample {json} Request-Example:
             * 
             *     {
             *       "Name": "DPD"
             *     }
             * 
             * @apiSampleRequest http://localhost:8080/api/payment
             * 
             * @apiSuccess {String} id  objectID()
             * @apiSuccess {String} name    Name of payment
             * @apiSuccess {Bool}   enable  Show or hide payment
             * @apiSuccess {Object} image   Payment image
             * @apiSuccess {String} image.target Image objectID()
             * @apiSuccess {Object} image.attributes Image atrybutes
             * @apiSuccess {String} image.attributes.alt Alt of image
             * @apiSuccess {String} image.attributes.title Title of image    
             * @apiSuccess {String} price   Price of payment
             * @apiSuccess {Number} weight  Weight
             * 
             * @apiVersion 1.0.0
             * 
             * @apiSuccessExample {json} Success
             * HTTP/1.1 200 OK
             *
             *  {
             *      succes: true,
             *      doc: [{
             *          "id": "5a95a2921f4de421bec83ed1",
             *          "name": "DPD",
             *          "enable": true,
             *          "image": {
             *              "target": "5a95a2921f4de421bec83ed1",
             *              "attributes": {
             *                  "alt": "shoes",
             *                  "title": "avesome title"
             *              }
             *          },
             *          "price": "11"
             *          "weight": 0,
             *      }]
             *   }
             *
             * @apiErrorExample {json} List error
             *    HTTP/1.1 500 Internal Server Error
             */
        .delete(authenticateUser, Payment.destroy)
            /**
             * @api {delete} /api/payment/:id Delete payment
             * @apiName DeletePayment
             * @apiGroup Payment
             * @apiPermission none
             * @apiSampleRequest http://localhost:8080/api/payment
             * 
             * @apiParam {String} id Payment unique ID.
             * 
             * @apiSuccess {String} id  objectID()
             * @apiSuccess {String} name    Name of payment
             * @apiSuccess {Bool}   enable  Show or hide payment
             * @apiSuccess {Object} image   Payment image
             * @apiSuccess {String} image.target Image objectID()
             * @apiSuccess {Object} image.attributes Image atrybutes
             * @apiSuccess {String} image.attributes.alt Alt of image
             * @apiSuccess {String} image.attributes.title Title of image    
             * @apiSuccess {String} price   Price of payment
             * @apiSuccess {Number} weight  Weight
             * 
             * @apiVersion 1.0.0
             * 
             * @apiSuccessExample {json} Success
             * HTTP/1.1 200 OK
             *
             *  {
             *      succes: true,
             *      doc: {
             *          "id": "5a95a2921f4de421bec83ed1",
             *          "name": "DPD",
             *          "enable": true,
             *          "image": {
             *              "target": "5a95a2921f4de421bec83ed1",
             *              "attributes": {
             *                  "alt": "shoes",
             *                  "title": "avesome title"
             *              }
             *          },
             *          "price": "11"
             *          "weight": 0,
             *      }
             *   }
             *
             * @apiErrorExample {json} List error
             *    HTTP/1.1 500 Internal Server Error
             */
}