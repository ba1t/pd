const { authenticateUser } = require('../middleware/auth');
const { Tax } = require('../controllers');
module.exports = (app) => {
    app.route('/api/tax')
        .get(authenticateUser, Tax.find)
            /**
             * @api {get} /api/tax Fetch Taxes
             * @apiName FetchTax
             * @apiGroup Tax
             * @apiPermission Admin
             * @apiSampleRequest http://localhost:8080/api/tax
             * 
             * @apiSuccess {String} id  objectID()
             * @apiSuccess {String} name    Name of tax
             * @apiSuccess {String} taxRate    Rate of tax
             * 
             * @apiVersion 1.0.0
             * 
             * @apiSuccessExample {json} Success
             * HTTP/1.1 200 OK
             *
             *  {
             *      succes: true,
             *      doc: [{
             *          "id": "5a95a2921f4de421bec83ed1",
             *          "name": "Admin",
             *          "taxRate": "0.23"
             *      }]
             *   }
             *
             * @apiErrorExample {json} List error
             *    HTTP/1.1 500 Internal Server Error
             */
        .post(authenticateUser, Tax.create)
            /**
             * @api {post} /api/tax Add tax
             * @apiName AddTax
             * @apiGroup Tax
             * @apiPermission admin, moderator
             * 
             * @apiParam (JSON) {String} name  Name of tax
             * @apiParam (JSON) {String} taxRate  Rate of tax
             * @apiParamExample {json} Request-Example:
             * 
             *     {
             *       "Name": "23%",
             *       "taxRate": 0.23
             *     }
             * 
             * @apiSampleRequest http://localhost:8080/api/tax
             * 
             * @apiSuccess {String} id  objectID()
             * @apiSuccess {String} name    Name of tax
             * @apiSuccess {String} taxRate    Rate of tax
             * 
             * @apiVersion 1.0.0
             * 
             * @apiSuccessExample {json} Success
             * HTTP/1.1 200 OK
             *
             *  {
             *      succes: true,
             *      doc: {
             *          "id": "5a95a2921f4de421bec83ed1",
             *          "name": "Admin",
             *          "taxRate": "0.23"
             *      }
             *   }
             *
             * @apiErrorExample {json} List error
             *    HTTP/1.1 500 Internal Server Error
             */
    app.route('/api/tax/:id')
        .get(authenticateUser, Tax.findOne)
            /**
             * @api {get} /api/tax/:id Get tax
             * @apiName GetTax
             * @apiGroup Tax
             * @apiPermission none
             * @apiSampleRequest http://localhost:8080/api/tax
             * 
             * @apiParam {String} id Tax unique ID.
             * 
             * @apiSuccess {String} id  objectID()
             * @apiSuccess {String} name    Name of tax
             * @apiSuccess {String} taxRate    Rate of tax
             * 
             * @apiVersion 1.0.0
             * 
             * @apiSuccessExample {json} Success
             * HTTP/1.1 200 OK
             *
             *  {
             *      succes: true,
             *      doc: {
             *          "id": "5a95a2921f4de421bec83ed1",
             *          "name": "Admin",
             *          "taxRate": "0.23"
             *      }
             *   }
             *
             * @apiErrorExample {json} List error
             *    HTTP/1.1 500 Internal Server Error
             */
        .put(authenticateUser, Tax.update)
        /**
             * @api {put} /api/tax/:id Update tax
             * @apiName UpdateTax
             * @apiGroup Tax
             * @apiPermission admin, moderator
             * 
             * @apiParam {String} id Tax unique ID.
             * 
             * @apiParam (JSON) {String} [name]  Name of tax
             * @apiParam (JSON) {String} [taxRate]  Rate of tax
             * 
             * @apiParamExample {json} Request-Example:
             * 
             *     {
             *       "Name": "23%",
             *       "taxRate": 0.23
             *     }
             * 
             * @apiSampleRequest http://localhost:8080/api/tax
             * 
             * @apiSuccess {String} id  objectID()
             * @apiSuccess {String} name    Name of tax
             * @apiSuccess {String} taxRate    Rate of tax 
             * 
             * @apiVersion 1.0.0
             * 
             * @apiSuccessExample {json} Success
             * HTTP/1.1 200 OK
             *
             *  {
             *      succes: true,
             *      doc: {
             *          "id": "5a95a2921f4de421bec83ed1",
             *          "name": "Admin",
             *          "taxRate": "0.23"
             *      }
             *   }
             *
             * @apiErrorExample {json} List error
             *    HTTP/1.1 500 Internal Server Error
             */
        .delete(authenticateUser, Tax.destroy)
            /**
             * @api {delete} /api/tax/:id Delete tax
             * @apiName DeleteTax
             * @apiGroup Tax
             * @apiPermission none
             * @apiSampleRequest http://localhost:8080/api/tax
             * 
             * @apiParam {String} id Tax unique ID.
             * 
             * @apiSuccess {String} id  objectID()
             * @apiSuccess {String} name    Name of tax
             * @apiSuccess {String} taxRate    Rate of tax 
             * 
             * @apiVersion 1.0.0
             * 
             * @apiSuccessExample {json} Success
             * HTTP/1.1 200 OK
             *
             *  {
             *      succes: true,
             *      doc: {
             *          "id": "5a95a2921f4de421bec83ed1",
             *          "name": "Admin",
             *          "taxRate": "0.23"
             *      }
             *   }
             *
             * @apiErrorExample {json} List error
             *    HTTP/1.1 500 Internal Server Error
             */
}