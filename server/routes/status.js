const { authenticateUser } = require('../middleware/auth');
const { OrderStatus, ProductStatus } = require('../controllers');
module.exports = (app, passport) => {
    app.route('/api/status/order')
        .get( OrderStatus.find)
        .post(authenticateUser, OrderStatus.create)
    app.route('/api/status/order/:id')
        .get(OrderStatus.findOne)
        .put(authenticateUser, OrderStatus.update)
        .delete(authenticateUser, OrderStatus.destroy)


        
    app.route('/api/status/product')
        .get( ProductStatus.find)
        .post(authenticateUser, ProductStatus.create)
    app.route('/api/status/product/:id')
        .get(ProductStatus.findOne)
        .put(authenticateUser, ProductStatus.update)
        .delete(authenticateUser, ProductStatus.destroy)
}