const { AuthUser, AuthCustomer } = require('../controllers');
const { authenticateUser, authenticateCustomer } = require('../middleware/auth')
module.exports = (app) => {
    app.post('/api/login', AuthUser.login)
    app.get('/api/logout', authenticateUser, AuthUser.logout)    
    app.get('/api/auth',authenticateUser, AuthUser.auth);
    app.get('/api/profile', (req,res) => (req.user));

    app.post('/cart/login', AuthCustomer.login, )
    app.get('/cart/logout', authenticateCustomer, AuthCustomer.logout)   
    app.get('/cart/auth', authenticateCustomer, AuthCustomer.auth);
    app.get('/cart/profile', (req,res) => (req.customer));
    app.get('/cart/register', AuthCustomer.register);
    // app.post('/api/auth/reminder', Auth.forgotPassword)
}