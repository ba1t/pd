const { authenticateUser } = require('../middleware/auth');
const { Manufacturer } = require('../controllers');
module.exports = (app, passport) => {
    app.route('/api/manufacturer')
        .get( Manufacturer.find)
            /**
             * @api {get} /api/manufacturer Fetch manufactures
             * @apiName FetchManufacturer
             * @apiGroup Manufacturer
             * @apiPermission none
             * @apiSampleRequest http://localhost:8080/api/manufacturer
             * 
             * @apiSuccess {String} id  objectID()
             * @apiSuccess {String} name    Name of manufacturer
             * @apiSuccess {Bool}   enable  Show or hide manufacturer
             * @apiSuccess {Object} image   Manufacturer image
             * @apiSuccess {String} image.target Image objectID()
             * @apiSuccess {Object} image.attributes Image atrybutes
             * @apiSuccess {String} image.attributes.alt Alt of image
             * @apiSuccess {String} image.attributes.title Title of image    
             * 
             * @apiVersion 1.0.0
             * 
             * @apiSuccessExample {json} Success
             * HTTP/1.1 200 OK
             *
             *  {
             *      succes: true,
             *      doc: [{
             *          "id": "5a95a2921f4de421bec83ed1",
             *          "name": "DPD",
             *          "enable": true,
             *          "image": {
             *              "target": "5a95a2921f4de421bec83ed1",
             *              "attributes": {
             *                  "alt": "shoes",
             *                  "title": "avesome title"
             *              }
             *          }
             *      }]
             *   }
             *
             * @apiErrorExample {json} List error
             *    HTTP/1.1 500 Internal Server Error
             */
        .post(authenticateUser, Manufacturer.create)
            /**
             * @api {post} /api/manufacturer Add manufacturer
             * @apiName AddManufacturer
             * @apiGroup Manufacturer
             * @apiPermission admin, moderator
             * 
             * @apiParam (JSON) {String} name    Name of manufacturer
             * @apiParam (JSON) {Object} [image]   Manufacturer image
             * @apiParam (JSON) {String} [image.target] Image objectID()
             * @apiParam (JSON) {Object} [image.attributes] Image atrybutes
             * @apiParam (JSON) {String} [image.attributes.alt] Alt of image
             * @apiParam (JSON) {String} [image.attributes.title] Title of image
             * 
             * @apiParamExample {json} Request-Example:
             * 
             *     {
             *       "Name": "Adidas"
             *     }
             * 
             * @apiSampleRequest http://localhost:8080/api/manufacturer
             * 
             * @apiSuccess {String} id  objectID()
             * @apiSuccess {String} name    Name of manufacturer
             * @apiSuccess {Object} image   Manufacturer image
             * @apiSuccess {String} image.target Image objectID()
             * @apiSuccess {Object} image.attributes Image atrybutes
             * @apiSuccess {String} image.attributes.alt Alt of image
             * @apiSuccess {String} image.attributes.title Title of image    
             * 
             * @apiVersion 1.0.0
             * 
             * @apiSuccessExample {json} Success
             * HTTP/1.1 200 OK
             *
             *  {
             *      succes: true,
             *      doc: [{
             *          "id": "5a95a2921f4de421bec83ed1",
             *          "name": "DPD",
             *          "enable": true,
             *          "image": {
             *              "target": "5a95a2921f4de421bec83ed1",
             *              "attributes": {
             *                  "alt": "shoes",
             *                  "title": "avesome title"
             *              }
             *          }
             *      }]
             *   }
             *
             * @apiErrorExample {json} List error
             *    HTTP/1.1 500 Internal Server Error
             */
    app.route('/api/manufacturer/:id')
        .get(Manufacturer.findOne)
            /**
             * @api {get} /api/manufacturer/:id Get manufacturer
             * @apiName GetManufacturer
             * @apiGroup Manufacturer
             * @apiPermission none
             * @apiSampleRequest http://localhost:8080/api/manufacturer
             * 
             * @apiParam {String} id Manufacturer unique ID.
             * 
             * @apiSuccess {String} id  objectID()
             * @apiSuccess {String} name    Name of manufacturer
             * @apiSuccess {Object} image   Manufacturer image
             * @apiSuccess {String} image.target Image objectID()
             * @apiSuccess {Object} image.attributes Image atrybutes
             * @apiSuccess {String} image.attributes.alt Alt of image
             * @apiSuccess {String} image.attributes.title Title of image    
             * 
             * @apiVersion 1.0.0
             * 
             * @apiSuccessExample {json} Success
             * HTTP/1.1 200 OK
             *
             *  {
             *      succes: true,
             *      doc: {
             *          "id": "5a95a2921f4de421bec83ed1",
             *          "name": "DPD",
             *          "enable": true,
             *          "image": {
             *              "target": "5a95a2921f4de421bec83ed1",
             *              "attributes": {
             *                  "alt": "shoes",
             *                  "title": "avesome title"
             *              }
             *          }
             *      }
             *   }
             *
             * @apiErrorExample {json} List error
             *    HTTP/1.1 500 Internal Server Error
             */
        .put(authenticateUser, Manufacturer.update)
        /**
             * @api {put} /api/manufacturer/:id Update manufacturer
             * @apiName UpdateManufacturer
             * @apiGroup Manufacturer
             * @apiPermission admin, moderator
             * 
             * @apiParam {String} id Manufacturer unique ID.
             * 
             * @apiParam (JSON) {String} [name]    Name of manufacturer
             * @apiParam (JSON) {Object} [image]   Manufacturer image
             * @apiParam (JSON) {String} [image.target] Image objectID()
             * @apiParam (JSON) {Object} [image.attributes] Image atrybutes
             * @apiParam (JSON) {String} [image.attributes.alt] Alt of image
             * @apiParam (JSON) {String} [image.attributes.title] Title of image
             * 
             * @apiParamExample {json} Request-Example:
             * 
             *     {
             *       "Name": "Adidas"
             *     }
             * 
             * @apiSampleRequest http://localhost:8080/api/manufacturer
             * 
             * @apiSuccess {String} id  objectID()
             * @apiSuccess {String} name    Name of manufacturer
             * @apiSuccess {Object} image   Manufacturer image
             * @apiSuccess {String} image.target Image objectID()
             * @apiSuccess {Object} image.attributes Image atrybutes
             * @apiSuccess {String} image.attributes.alt Alt of image
             * @apiSuccess {String} image.attributes.title Title of image    
             * 
             * @apiVersion 1.0.0
             * 
             * @apiSuccessExample {json} Success
             * HTTP/1.1 200 OK
             *
             *  {
             *      succes: true,
             *      doc: [{
             *          "id": "5a95a2921f4de421bec83ed1",
             *          "name": "DPD",
             *          "enable": true,
             *          "image": {
             *              "target": "5a95a2921f4de421bec83ed1",
             *              "attributes": {
             *                  "alt": "shoes",
             *                  "title": "avesome title"
             *              }
             *          }
             *      }]
             *   }
             *
             * @apiErrorExample {json} List error
             *    HTTP/1.1 500 Internal Server Error
             */
        .delete(authenticateUser, Manufacturer.destroy)
            /**
             * @api {delete} /api/manufacturer/:id Delete manufacturer
             * @apiName DeleteManufacturer
             * @apiGroup Manufacturer
             * @apiPermission none
             * @apiSampleRequest http://localhost:8080/api/manufacturer
             * 
             * @apiParam {String} id Manufacturer unique ID.
             * 
             * @apiSuccess {String} id  objectID()
             * @apiSuccess {String} name    Name of manufacturer
             * @apiSuccess {Object} image   Manufacturer image
             * @apiSuccess {String} image.target Image objectID()
             * @apiSuccess {Object} image.attributes Image atrybutes
             * @apiSuccess {String} image.attributes.alt Alt of image
             * @apiSuccess {String} image.attributes.title Title of image    
             * 
             * @apiVersion 1.0.0
             * 
             * @apiSuccessExample {json} Success
             * HTTP/1.1 200 OK
             *
             *  {
             *      succes: true,
             *      doc: {
             *          "id": "5a95a2921f4de421bec83ed1",
             *          "name": "DPD",
             *          "enable": true,
             *          "image": {
             *              "target": "5a95a2921f4de421bec83ed1",
             *              "attributes": {
             *                  "alt": "shoes",
             *                  "title": "avesome title"
             *              }
             *          }
             *      }
             *   }
             *
             * @apiErrorExample {json} List error
             *    HTTP/1.1 500 Internal Server Error
             */
}