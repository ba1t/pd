'use strict';
const { Order } = require('../models');

module.exports = {
    find(req, res) {
        Order.find({})
            .then( doc => ( 
                Order.countDocuments({}).then(count => (
                    res.json({ success: true, doc, count }) ))
                )).catch(err => ( res.json({ success: false, err: err.message }) ))
    },
    findOne(req, res) {
        Order.findById(req.params.id)
            .then( doc => ( res.json({ success: true, doc }) ))
            .catch (err =>( res.json({ success: false, err: err.message }) ))
    },
    create(req, res) {
        new Order(req.body)
            .save()
            .then( doc => ( res.json({ success: true, doc }) ))
            .catch(err => ( res.json({ success: false, err: err.message }) ))
    },
    update(req, res) {
        Order.findByIdAndUpdate(req.params.id, req.body)
            .then(doc => ( res.json({ success: true, doc }) ))
            .catch (err =>( res.json({ success: false, err: err.message }) ))
    },
    destroy(req, res) {
        Order.findByIdAndRemove(req.params.id)
            .then(doc => ( res.json({ success: true, doc }) ))
            .catch (err =>( res.json({ success: false, err: err.message }) ))
    }
};
