const { Customer } = require('../models');
module.exports = {
    auth: (req, res) => {
        res.json({
            isAuth:true,
            id:req.customer._id,
            email:req.customer.email,
            firstName:req.customer.firstName,
            lastname:req.customer.lastname
        })
    },
    login: (req,res)=>{ 
        Customer.findOne({'email':req.body.email},(err,customer)=>{
            if(!customer) return res.json({isAuth:false,message:'Auth failed, email not found'})
    
            customer.comparePassword(req.body.password,(err,isMatch)=>{
                if(!isMatch) return res.json({
                    isAuth:false,
                    message:'Wrong password'
                });
    
                customer.generateToken((err,customer)=>{
                    if(err) return res.status(400).send(err);
                    res.cookie('authCustomer',customer.token).json({
                        isAuth:true,
                        id:customer._id,
                        email:customer.email
                    })
                })
            })
        })
    },
    logout: (req,res)=>{
        req.customer.deleteToken(req.token,(err,customer)=>{
            if(err) return res.status(400).send(err);
            res.redirect('/')
        })
    },
    register: (req,res)=>{
        const customer = new Customer(req.body);
        customer.save((err,doc)=>{
            if(err) return res.json({success:false});
            res.status(200).json({
                success:true,
                doc
            })
        })
    },
    forgotPassword: (req, res) => {
        
    },
    changePassword: (req, res) => {
        
    }

}