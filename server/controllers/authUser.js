const { User } = require('../models');
module.exports = {
    auth: (req, res) => {
        res.json({
            isAuth:true,
            id:req.user._id,
            email:req.user.email,
            firstName:req.user.firstName,
            lastname:req.user.lastname,
            role: req.user.roles
        })
    },
    login: (req,res)=>{ 
        User.findOne({'email':req.body.email},(err,user)=>{
            if(!user) return res.json({isAuth:false,message:'Auth failed, email not found'})
    
            user.comparePassword(req.body.password,(err,isMatch)=>{
                if(!isMatch) return res.json({
                    isAuth:false,
                    message:'Wrong password'
                });
    
                user.generateToken((err,user)=>{
                    if(err) return res.status(400).send(err);
                    res.cookie('authUser',user.token).json({
                        isAuth:true,
                        id:user._id,
                        email:user.email
                    })
                })
            })
        })
    },
    logout: (req,res)=>{
        req.user.deleteToken(req.token,(err,user)=>{
            if(err) return res.status(400).send(err);
            res.sendStatus(200)
        })
    },
    register: (req,res)=>{
        const user = new User({email: "bait@onet.pl", password: "bait123" });
        user.save((err,doc)=>{
            if(err) return res.json({success:false});
            res.status(200).json({
                success:true,
                user:doc
            })
        })
    },
    forgotPassword: (req, res) => {
        
    },
    changePassword: (req, res) => {
        
    }

}