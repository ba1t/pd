'use strict';
const { Tax } = require('../models');

module.exports = {
    find(req, res) {
        Tax.find({})
            .then(find => ( res.json({ success: true, doc: find }) ))
            .catch(err => ( res.json({ success: false, err: err.message }) ))
    },
    findOne(req, res) {
        Tax.findById(req.params.id)
            .then( findOne => ( res.json({ success: true, doc: findOne }) ))
            .catch (err =>( res.json({ success: false, err: err.message }) ))
    },
    create(req, res) {
        new Tax(req.body)
            .save()
            .then( create => ( res.json({ success: true, doc: create }) ))
            .catch(err => ( res.json({ success: false, err: err.message }) ))
    },
    update(req, res) {
        Tax.findByIdAndUpdate(req.params.id, req.body)
            .then(update => ( res.json({ success: true, doc: update }) ))
            .catch (err =>( res.json({ success: false, err: err.message }) ))
    },
    destroy(req, res) {
        Tax.findByIdAndRemove(req.params.id)
            .then(destroy => ( res.json({ success: true, doc: destroy }) ))
            .catch (err =>( res.json({ success: false, err: err.message }) ))
    }
};
