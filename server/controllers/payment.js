'use strict';
const { Payment } = require('../models');

module.exports = {
    find(req, res) {
        Payment.find({})
            .then( doc => ( 
                Payment.countDocuments({}).then(count => (
                    res.json({ success: true, doc, count }) ))
                )).catch(err => ( res.json({ success: false, err: err.message }) ))
    },
    findOne(req, res) {
        Payment.findById(req.params.id)
            .then( doc => ( res.json({ success: true, doc }) ))
            .catch (err =>( res.json({ success: false, err: err.message }) ))
    },
    create(req, res) {
        new Payment(req.body)
            .save()
            .then( doc => ( res.json({ success: true, doc }) ))
            .catch(err => ( res.json({ success: false, err: err.message }) ))
    },
    update(req, res) {
        Payment.findByIdAndUpdate(req.params.id, req.body)
            .then(doc => ( res.json({ success: true, doc }) ))
            .catch (err =>( res.json({ success: false, err: err.message }) ))
    },
    destroy(req, res) {
        Payment.findByIdAndRemove(req.params.id)
            .then(doc => ( res.json({ success: true, doc }) ))
            .catch (err =>( res.json({ success: false, err: err.message }) ))
    }
};
