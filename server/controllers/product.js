'use strict';
const { Product } = require('../models');

module.exports = {
    find(req, res) {
        let skip = parseInt(req.query.skip);
        let limit = parseInt(req.query.limit);
        let order = req.query.order;
        
        Product.find({})
            .populate({path: 'additionalCategoryIds', model: 'Category', select: 'name'})
            .populate({path: 'categoryId', model: 'Category'})
            .populate({path: 'manufacturerId', model: 'Manufacturer'})
            .populate({path: 'taxId', model: 'Tax'})
            .populate({path: 'image.target', model: 'Image', select: 'path'})            
            .skip(skip).sort({date:-1}).limit(limit)
            .then(find => (
                Product.count({}).then(number => (
                    res.json({ success: true, doc: find, count: number }) 
                )))
            ).catch(err => ( res.json({ success: false, err: err.message }) ))
    },
    findOne(req, res) {
        Product.findById(req.params.id)
            .populate({path: 'additionalCategoryIds', model: 'Category', select: 'name'})
            .populate({path: 'categoryId', model: 'Category'})
            .populate({path: 'manufacturerId', model: 'Manufacturer'})
            .populate({path: 'taxId', model: 'Tax'})
            .then( findOne => ( res.json({ success: true, doc: findOne }) ))
            .catch (err =>( res.json({ success: false, err: err.message }) ))
    },
    create(req, res) {
        console.log(req.body);
        new Product(req.body)
            .save()
            .then( create => ( res.json({ success: true, doc: create }) ))
            .catch(err => ( res.json({ success: false, err: err.message }) ))
    },
    update(req, res) {
        Product.findByIdAndUpdate(req.params.id, req.body, {new: true})
            .then(update => ( res.json({ success: true, doc: update }) ))
            .catch (err =>( res.json({ success: false, err: err.message }) ))
    },
    destroy(req, res) {
        Product.findByIdAndRemove(req.params.id)
            .then(destroy => ( res.json({ success: true, doc: destroy }) ))
            .catch (err =>( res.json({ success: false, err: err.message }) ))
    },
    review(req, res) {
        Product.find({})
            .then( review => ( res.json({ success: true, doc: review }) ))
            .catch (err =>( res.json({ success: false, err: err.message }) ))
    },
    basket(req, res) {
        Product.find({})
            .then( basket => ( res.json({ success: true, doc: basket }) ))
            .catch (err =>( res.json({ success: false, err: err.message }) ))
    },    
    order(req, res) {
        Product.find({})
            .then( order => ( res.json({ success: true, doc: order }) ))
            .catch (err =>( res.json({ success: false, err: err.message }) ))
    }
};
