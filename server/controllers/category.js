'use strict';
const { Category } = require('../models');

module.exports = {
    find(req, res) {
        Category.find({})
            .populate({path: 'parentCategory', model: 'Category', select: 'name'})
            .populate({path: 'image.target', model: 'Image', select: 'path'})           
            .then( doc => ( 
                Category.countDocuments({}).then(count => (
                    res.json({ success: true, doc, count }) ))
                )).catch(err => ( res.json({ success: false, err: err.message }) ))
    },
    findOne(req, res) {
        Category.findById(req.params.id)
            .populate({path: 'image.target', model: 'Image', select: 'path'})           
            .then( doc => ( res.json({ success: true, doc }) ))
            .catch (err =>( res.json({ success: false, err: err.message }) ))
    },
    create(req, res) {
        new Category(req.body)
            .save()
            .then( doc => ( res.json({ success: true, doc }) ))
            .catch(err => ( res.json({ success: false, err: err.message }) ))
    },
    update(req, res) {
        Category.findByIdAndUpdate(req.params.id, req.body)
            .then(doc => ( res.json({ success: true, doc }) ))
            .catch (err =>( res.json({ success: false, err: err.message }) ))
    },
    destroy(req, res) {
        Category.findByIdAndRemove(req.params.id)
            .then(doc => ( res.json({ success: true, doc }) ))
            .catch (err =>( res.json({ success: false, err: err.message }) ))
    }
};
