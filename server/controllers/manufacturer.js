'use strict';
const { Manufacturer } = require('../models');

module.exports = {
    find(req, res) {
        Manufacturer.find({})
            .populate({path: 'image.target', model: 'Image', select: 'path'})       

            .then( doc => ( 
                Manufacturer.count({}).then(count => (
                    res.json({ success: true, doc, count }) ))
                ))
            .catch( err => ( res.json({ success: false, err: err.message }) ))
    },
    findOne(req, res) {
        Manufacturer.findById(req.params.id)
            .populate({path: 'image.target', model: 'Image', select: 'path'})       
            .then( doc => ( res.json({ success: true, doc }) ))
            .catch( err =>( res.json({ success: false, err: err.message }) ))
    },
    create(req, res) {
        new Manufacturer(req.body)
            .save()
            .then( doc => ( res.json({ success: true, doc }) ))
            .catch( err => ( res.json({ success: false, err: err.message }) ))
    },
    update(req, res) {
        Manufacturer.findByIdAndUpdate(req.params.id, req.body)
            .then( doc => ( res.json({ success: true, doc }) ))
            .catch( err =>( res.json({ success: false, err: err.message }) ))
    },
    destroy(req, res) {
        Manufacturer.findByIdAndRemove(req.params.id)
            .then( doc => ( res.json({ success: true, doc }) ))
            .catch( err =>( res.json({ success: false, err: err.message }) ))
    }
};
