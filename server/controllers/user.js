'use strict';
const { User } = require('../models');

module.exports = {
    find(req, res) {
        User.find({})
            .populate({path: 'roles', model: 'Role', select: 'name'})
            .then(find => ( res.json({ success: true, doc: find }) ))
            .catch(err => ( res.json({ success: false, err: err.message }) ))
    },
    findOne(req, res) {
        User.findById(req.params.id)
            .populate({path: 'roles', model: 'Role', select: 'name'})
            .then( findOne => ( res.json({ success: true, doc: findOne }) ))
            .catch (err =>( res.json({ success: false, err: err.message }) ))
    },
    create(req, res) {
        new User(req.body)
            .save()
            .then( create => ( res.json({ success: true, doc: create }) ))
            .catch(err => ( res.json({ success: false, err: err.message }) ))
    },
    update(req, res) {
        User.findByIdAndUpdate(req.params.id, req.body)
            .then(update => ( res.json({ success: true, doc: update }) ))
            .catch (err =>( res.json({ success: false, err: err.message }) ))
    },
    destroy(req, res) {
        User.findByIdAndRemove(req.params.id)
            .then(destroy => ( res.json({ success: true, doc: destroy }) ))
            .catch (err =>( res.json({ success: false, err: err.message }) ))
    }
};
