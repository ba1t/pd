'use strict';
const { Delivery } = require('../models');

module.exports = {
    find(req, res) {
        Delivery.find({})
            .then( doc => ( 
                Delivery.countDocuments({}).then(count => (
                    res.json({ success: true, doc, count }) ))
                )).catch(err => ( res.json({ success: false, err: err.message }) ))
    },
    findOne(req, res) {
        Delivery.findById(req.params.id)
            .then( doc => ( res.json({ success: true, doc }) ))
            .catch (err =>( res.json({ success: false, err: err.message }) ))
    },
    create(req, res) {
        new Delivery(req.body)
            .save()
            .then( doc => ( res.json({ success: true, doc }) ))
            .catch(err => ( res.json({ success: false, err: err.message }) ))
    },
    update(req, res) {
        Delivery.findByIdAndUpdate(req.params.id, req.body)
            .then(doc => ( res.json({ success: true, doc }) ))
            .catch (err =>( res.json({ success: false, err: err.message }) ))
    },
    destroy(req, res) {
        Delivery.findByIdAndRemove(req.params.id)
            .then(doc => ( res.json({ success: true, doc }) ))
            .catch (err =>( res.json({ success: false, err: err.message }) ))
    }
};
