'use strict';
const { Customer } = require('../models');
const mongoose = require('mongoose');
module.exports = {
    find(req, res) {
        Customer.find({})
            .then( doc => ( 
                Customer.countDocuments({}).then(count => (
                    res.json({ success: true, doc, count }) ))
                )).catch(err => ( res.json({ success: false, err: err.message }) ))
    },
    findCustomerBasket(req, res) {
        Customer.findById(req.customer._id)
            .populate({path: 'cart.item', model: 'Product'}) 
            .then( doc => (res.json({success: true, doc: {id: doc._id, cart: doc.cart} } )))
            .catch (err =>( res.json({ success: false, err: err.message }) ))
    },
    addToCart(req,res) {
        Customer.findByIdAndUpdate(
            req.customer._id,
            {$push: {cart: {item: mongoose.Types.ObjectId(req.params.productId), count: !!req.params.count ? req.params.count : 1}}},
            {safe: true, upsert: true})            
            .then( doc => (res.json({success: true, doc: {id: doc._id, cart: doc.cart} } )))
            .catch (err =>( res.json({ success: false, err: err.message }) ))
    },
    updateCount(req, res) {

        Customer.update({"_id": req.customer._id, "cart.item": req.params.productId}, 
            {$set: {"cart.$.count": req.params.count }})
            .then( doc => (res.json({success: true, doc: {id: doc._id, cart: doc.cart} } )))
            .catch (err =>( res.json({ success: false, err: err.message }) ))
    },
    removeFromCart(req, res) {
        Customer.findByIdAndUpdate(
            req.customer._id,
            {$pull: { cart: {item: req.params.productId}}},
            {safe: true, upsert: true})
            .then( doc => (res.json({success: true, doc: {id: doc._id, cart: doc.cart} } )))
            .catch (err =>( res.json({ success: false, err: err.message }) ))
    },
    findOne(req, res) {
        Customer.findById(req.params.id)
            .then( doc => ( res.json({ success: true, doc }) ))
            .catch (err =>( res.json({ success: false, err: err.message }) ))
    },
    create(req, res) {
        new Customer(req.body)
            .save()
            .then( doc => ( res.json({ success: true, doc }) ))
            .catch(err => ( res.json({ success: false, err: err.message }) ))
    },
    update(req, res) {
        Customer.findByIdAndUpdate(req.params.id, req.body)
            .then(doc => ( res.json({ success: true, doc }) ))
            .catch (err =>( res.json({ success: false, err: err.message }) ))
    },
    destroy(req, res) {
        Customer.findByIdAndRemove(req.params.id)
            .then(doc => ( res.json({ success: true, doc }) ))
            .catch (err =>( res.json({ success: false, err: err.message }) ))
    }
};
