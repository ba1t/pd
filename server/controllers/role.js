'use strict';
const { Role } = require('../models');

module.exports = {
    find(req, res) {
        Role.find({})
            .then(find => ( res.json({ success: true, doc: find }) ))
            .catch(err => ( res.json({ success: false, err: err.message }) ))
    },
    findOne(req, res) {
        Role.findById(req.params.id)
            .then( findOne => ( res.json({ success: true, doc: findOne }) ))
            .catch (err =>( res.json({ success: false, err: err.message }) ))
    },
    create(req, res) {
        new Role(req.body)
            .save()
            .then( create => ( res.json({ success: true, doc: create }) ))
            .catch(err => ( res.json({ success: false, err: err.message }) ))
    },
    update(req, res) {
        Role.findByIdAndUpdate(req.params.id, req.body)
            .then(update => ( res.json({ success: true, doc: update }) ))
            .catch (err =>( res.json({ success: false, err: err.message }) ))
    },
    destroy(req, res) {
        Role.findByIdAndRemove(req.params.id)
            .then(destroy => ( res.json({ success: true, doc: destroy }) ))
            .catch (err =>( res.json({ success: false, err: err.message }) ))
    }
};
