const {User, Customer } = require('../models');


module.exports = {
    authenticateUser: (req,res,next) => {
        let token = req.cookies.authUser;
        User.findByToken(token,(err,user)=>{
            if(err) throw err;
            if(!user) {
                res.send(false)
            }else {
                req.userToken = token;
                req.user = user;
                next();
            }
        })
    },

    authenticateCustomer: (req,res,next) => {
        let token = req.cookies.authCustomer;
        
        Customer.findByToken(token,(err,customer)=>{
            if(err) throw err;
            if(!customer) {
                res.send(false);
            } else {
                req.customerToken = token;
                req.customer = customer;
                next();
            }
            
        })
    }
}