define({ "api": [
  {
    "type": "delete",
    "url": "/api/category/:id",
    "title": "Delete Category",
    "name": "DeleteCategory",
    "group": "Category",
    "permission": [
      {
        "name": "Admin, Moderator"
      }
    ],
    "sampleRequest": [
      {
        "url": "http://localhost:8080/api/category"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "id",
            "description": "<p>Users unique ID.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "id",
            "description": "<p>objectID()</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "name",
            "description": "<p>Name of category</p>"
          },
          {
            "group": "Success 200",
            "type": "Bool",
            "optional": false,
            "field": "enable",
            "description": "<p>Show or hide category</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "image",
            "description": "<p>Category image</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "image.target",
            "description": "<p>Image objectID()</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "image.attributes",
            "description": "<p>Image atrybutes</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "image.attributes.alt",
            "description": "<p>Alt of image</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "image.attributes.title",
            "description": "<p>Title of image</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "weight",
            "description": "<p>Weight</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "parentCategory",
            "description": "<p>Parent objectID()</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "path",
            "description": "<p>path</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success",
          "content": "HTTP/1.1 200 OK\n\n {\n     succes: true,\n     doc: {\n         \"id\": \"5a95a2921f4de421bec83ed1\",\n         \"name\": \"shoes\",\n         \"enable\": true,\n         \"image\": {\n             \"target\": \"5a95a2921f4de421bec83ed1\",\n             \"attributes\": {\n                 \"alt\": \"shoes\",\n                 \"title\": \"avesome title\"\n             }\n         },\n         \"weight\": 0,\n         \"parentCategory\": \"5a95a2921f4de421bec83ed1\",\n         \"path\": \"/catalog/shoes\",\n     }\n  }",
          "type": "json"
        }
      ]
    },
    "version": "1.0.0",
    "error": {
      "examples": [
        {
          "title": "List error",
          "content": "HTTP/1.1 500 Internal Server Error",
          "type": "json"
        }
      ]
    },
    "filename": "routes/category.js",
    "groupTitle": "Category"
  },
  {
    "type": "get",
    "url": "/api/category",
    "title": "Fetch categories",
    "name": "FetchCategory",
    "group": "Category",
    "permission": [
      {
        "name": "none"
      }
    ],
    "sampleRequest": [
      {
        "url": "http://localhost:8080/api/category"
      }
    ],
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "id",
            "description": "<p>objectID()</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "name",
            "description": "<p>Name of category</p>"
          },
          {
            "group": "Success 200",
            "type": "Bool",
            "optional": false,
            "field": "enable",
            "description": "<p>Show or hide category</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "image",
            "description": "<p>Category image</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "image.target",
            "description": "<p>Image objectID()</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "image.attributes",
            "description": "<p>Image atrybutes</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "image.attributes.alt",
            "description": "<p>Alt of image</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "image.attributes.title",
            "description": "<p>Title of image</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "weight",
            "description": "<p>Weight</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "parentCategory",
            "description": "<p>Parent objectID()</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "path",
            "description": "<p>path</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success",
          "content": "HTTP/1.1 200 OK\n\n {\n     succes: true,\n     doc: [{\n         \"id\": \"5a95a2921f4de421bec83ed1\",\n         \"name\": \"shoes\",\n         \"enable\": true,\n         \"image\": {\n             \"target\": \"5a95a2921f4de421bec83ed1\",\n             \"attributes\": {\n                 \"alt\": \"shoes\",\n                 \"title\": \"avesome title\"\n             }\n         },\n         \"weight\": 0,\n         \"parentCategory\": \"5a95a2921f4de421bec83ed1\",\n         \"path\": \"/catalog/shoes\",\n     }]\n  }",
          "type": "json"
        }
      ]
    },
    "version": "1.0.0",
    "error": {
      "examples": [
        {
          "title": "List error",
          "content": "HTTP/1.1 500 Internal Server Error",
          "type": "json"
        }
      ]
    },
    "filename": "routes/category.js",
    "groupTitle": "Category"
  },
  {
    "type": "get",
    "url": "/api/category/:id",
    "title": "Get Category",
    "name": "GetCategory",
    "group": "Category",
    "permission": [
      {
        "name": "none"
      }
    ],
    "sampleRequest": [
      {
        "url": "http://localhost:8080/api/category"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "id",
            "description": "<p>Users unique ID.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "id",
            "description": "<p>objectID()</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "name",
            "description": "<p>Name of category</p>"
          },
          {
            "group": "Success 200",
            "type": "Bool",
            "optional": false,
            "field": "enable",
            "description": "<p>Show or hide category</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "image",
            "description": "<p>Category image</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "image.target",
            "description": "<p>Image objectID()</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "image.attributes",
            "description": "<p>Image atrybutes</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "image.attributes.alt",
            "description": "<p>Alt of image</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "image.attributes.title",
            "description": "<p>Title of image</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "weight",
            "description": "<p>Weight</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "parentCategory",
            "description": "<p>Parent objectID()</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "path",
            "description": "<p>path</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success",
          "content": "HTTP/1.1 200 OK\n\n {\n     succes: true,\n     doc: {\n         \"id\": \"5a95a2921f4de421bec83ed1\",\n         \"name\": \"shoes\",\n         \"enable\": true,\n         \"image\": {\n             \"target\": \"5a95a2921f4de421bec83ed1\",\n             \"attributes\": {\n                 \"alt\": \"shoes\",\n                 \"title\": \"avesome title\"\n             }\n         },\n         \"weight\": 0,\n         \"parentCategory\": \"5a95a2921f4de421bec83ed1\",\n         \"path\": \"/catalog/shoes\",\n     }\n  }",
          "type": "json"
        }
      ]
    },
    "version": "1.0.0",
    "error": {
      "examples": [
        {
          "title": "List error",
          "content": "HTTP/1.1 500 Internal Server Error",
          "type": "json"
        }
      ]
    },
    "filename": "routes/category.js",
    "groupTitle": "Category"
  },
  {
    "type": "post",
    "url": "/api/category",
    "title": "Create category",
    "group": "Category",
    "permission": [
      {
        "name": "admin, moderator"
      }
    ],
    "parameter": {
      "fields": {
        "JSON": [
          {
            "group": "JSON",
            "type": "String",
            "optional": false,
            "field": "name",
            "description": "<p>Name of category</p>"
          },
          {
            "group": "JSON",
            "type": "Bool",
            "optional": false,
            "field": "enable",
            "defaultValue": "true",
            "description": "<p>Show or hide category</p>"
          },
          {
            "group": "JSON",
            "type": "Object",
            "optional": true,
            "field": "image",
            "description": "<p>Category image</p>"
          },
          {
            "group": "JSON",
            "type": "String",
            "optional": true,
            "field": "image.target",
            "description": "<p>Image objectID()</p>"
          },
          {
            "group": "JSON",
            "type": "Object",
            "optional": true,
            "field": "image.attributes",
            "description": "<p>Image atrybutes</p>"
          },
          {
            "group": "JSON",
            "type": "String",
            "optional": true,
            "field": "image.attributes.alt",
            "description": "<p>Alt of image</p>"
          },
          {
            "group": "JSON",
            "type": "String",
            "optional": true,
            "field": "image.attributes.title",
            "description": "<p>Title of image</p>"
          },
          {
            "group": "JSON",
            "type": "Number",
            "optional": true,
            "field": "weight",
            "description": "<p>Weight</p>"
          },
          {
            "group": "JSON",
            "type": "String",
            "optional": true,
            "field": "parentCategory",
            "description": "<p>Parent objectID()</p>"
          },
          {
            "group": "JSON",
            "type": "String",
            "optional": true,
            "field": "path",
            "description": "<p>path</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request-Example:",
          "content": "\n{\n  \"Name\": \"Buty\"\n}",
          "type": "json"
        }
      ]
    },
    "sampleRequest": [
      {
        "url": "http://localhost:8080/api/category"
      }
    ],
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "id",
            "description": "<p>objectID()</p>"
          },
          {
            "group": "Success 200",
            "type": "Bool",
            "optional": false,
            "field": "enable",
            "description": "<p>Show or hide category</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "name",
            "description": "<p>Name</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "path",
            "description": "<p>path</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "weight",
            "description": "<p>Weight</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success",
          "content": "HTTP/1.1 200 OK\n\n {\n     succes: true,\n     doc: {\n         \"id\": \"5a95a2921f4de421bec83ed1\",\n         \"enable\": true,\n         \"name\": \"shoes\",\n         \"path\": \"/catalog/shoes\",\n         \"weight\": 0\n     }\n  }",
          "type": "json"
        }
      ]
    },
    "version": "1.0.0",
    "error": {
      "examples": [
        {
          "title": "List error",
          "content": "HTTP/1.1 500 Internal Server Error",
          "type": "json"
        }
      ]
    },
    "filename": "routes/category.js",
    "groupTitle": "Category",
    "name": "PostApiCategory"
  },
  {
    "type": "put",
    "url": "/api/category/:id",
    "title": "Update category",
    "name": "UpdateCategory",
    "group": "Category",
    "permission": [
      {
        "name": "Admin, Moderator"
      }
    ],
    "sampleRequest": [
      {
        "url": "http://localhost:8080/api/category"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "id",
            "description": "<p>Users unique ID.</p>"
          }
        ],
        "JSON": [
          {
            "group": "JSON",
            "type": "String",
            "optional": true,
            "field": "name",
            "description": "<p>Name of category</p>"
          },
          {
            "group": "JSON",
            "type": "Bool",
            "optional": true,
            "field": "enable",
            "description": "<p>Show or hide category</p>"
          },
          {
            "group": "JSON",
            "type": "Object",
            "optional": true,
            "field": "image",
            "description": "<p>Category image</p>"
          },
          {
            "group": "JSON",
            "type": "String",
            "optional": true,
            "field": "image.target",
            "description": "<p>Image objectID()</p>"
          },
          {
            "group": "JSON",
            "type": "Object",
            "optional": true,
            "field": "image.attributes",
            "description": "<p>Image atrybutes</p>"
          },
          {
            "group": "JSON",
            "type": "String",
            "optional": true,
            "field": "image.attributes.alt",
            "description": "<p>Alt of image</p>"
          },
          {
            "group": "JSON",
            "type": "String",
            "optional": true,
            "field": "image.attributes.title",
            "description": "<p>Title of image</p>"
          },
          {
            "group": "JSON",
            "type": "Number",
            "optional": true,
            "field": "weight",
            "description": "<p>Weight</p>"
          },
          {
            "group": "JSON",
            "type": "String",
            "optional": true,
            "field": "parentCategory",
            "description": "<p>Parent objectID()</p>"
          },
          {
            "group": "JSON",
            "type": "String",
            "optional": true,
            "field": "path",
            "description": "<p>path</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request-Example:",
          "content": "\n{\n  \"Name\": \"Buty\"\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "id",
            "description": "<p>objectID()</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "name",
            "description": "<p>Name of category</p>"
          },
          {
            "group": "Success 200",
            "type": "Bool",
            "optional": false,
            "field": "enable",
            "description": "<p>Show or hide category</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "image",
            "description": "<p>Category image</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "image.target",
            "description": "<p>Image objectID()</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "image.attributes",
            "description": "<p>Image atrybutes</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "image.attributes.alt",
            "description": "<p>Alt of image</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "image.attributes.title",
            "description": "<p>Title of image</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "weight",
            "description": "<p>Weight</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "parentCategory",
            "description": "<p>Parent objectID()</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "path",
            "description": "<p>path</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success",
          "content": "HTTP/1.1 200 OK\n\n {\n     succes: true,\n     doc: {\n         \"id\": \"5a95a2921f4de421bec83ed1\",\n         \"name\": \"shoes\",\n         \"enable\": true,\n         \"image\": {\n             \"target\": \"5a95a2921f4de421bec83ed1\",\n             \"attributes\": {\n                 \"alt\": \"shoes\",\n                 \"title\": \"avesome title\"\n             }\n         },\n         \"weight\": 0,\n         \"parentCategory\": \"5a95a2921f4de421bec83ed1\",\n         \"path\": \"/catalog/shoes\",\n     }\n  }",
          "type": "json"
        }
      ]
    },
    "version": "1.0.0",
    "error": {
      "examples": [
        {
          "title": "List error",
          "content": "HTTP/1.1 500 Internal Server Error",
          "type": "json"
        }
      ]
    },
    "filename": "routes/category.js",
    "groupTitle": "Category"
  },
  {
    "type": "post",
    "url": "/api/delivery",
    "title": "Add delivery",
    "name": "AddDelivery",
    "group": "Delivery",
    "permission": [
      {
        "name": "admin, moderator"
      }
    ],
    "parameter": {
      "fields": {
        "JSON": [
          {
            "group": "JSON",
            "type": "String",
            "optional": false,
            "field": "name",
            "description": "<p>Name of delivery</p>"
          },
          {
            "group": "JSON",
            "type": "Bool",
            "optional": false,
            "field": "enable",
            "defaultValue": "true",
            "description": "<p>Show or hide delivery</p>"
          },
          {
            "group": "JSON",
            "type": "Object",
            "optional": true,
            "field": "image",
            "description": "<p>Delivery image</p>"
          },
          {
            "group": "JSON",
            "type": "String",
            "optional": true,
            "field": "image.target",
            "description": "<p>Image objectID()</p>"
          },
          {
            "group": "JSON",
            "type": "Object",
            "optional": true,
            "field": "image.attributes",
            "description": "<p>Image atrybutes</p>"
          },
          {
            "group": "JSON",
            "type": "String",
            "optional": true,
            "field": "image.attributes.alt",
            "description": "<p>Alt of image</p>"
          },
          {
            "group": "JSON",
            "type": "String",
            "optional": true,
            "field": "image.attributes.title",
            "description": "<p>Title of image</p>"
          },
          {
            "group": "JSON",
            "type": "String",
            "optional": true,
            "field": "price",
            "description": "<p>Delivery price</p>"
          },
          {
            "group": "JSON",
            "type": "Number",
            "optional": true,
            "field": "weight",
            "description": "<p>Weight</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request-Example:",
          "content": "\n{\n  \"Name\": \"Buty\"\n}",
          "type": "json"
        }
      ]
    },
    "sampleRequest": [
      {
        "url": "http://localhost:8080/api/delivery"
      }
    ],
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "id",
            "description": "<p>objectID()</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "name",
            "description": "<p>Name of delivery</p>"
          },
          {
            "group": "Success 200",
            "type": "Bool",
            "optional": false,
            "field": "enable",
            "description": "<p>Show or hide delivery</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "image",
            "description": "<p>Delivery image</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "image.target",
            "description": "<p>Image objectID()</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "image.attributes",
            "description": "<p>Image atrybutes</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "image.attributes.alt",
            "description": "<p>Alt of image</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "image.attributes.title",
            "description": "<p>Title of image</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "price",
            "description": "<p>Price of delivery</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "weight",
            "description": "<p>Weight</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success",
          "content": "HTTP/1.1 200 OK\n\n {\n     succes: true,\n     doc: [{\n         \"id\": \"5a95a2921f4de421bec83ed1\",\n         \"name\": \"DPD\",\n         \"enable\": true,\n         \"image\": {\n             \"target\": \"5a95a2921f4de421bec83ed1\",\n             \"attributes\": {\n                 \"alt\": \"shoes\",\n                 \"title\": \"avesome title\"\n             }\n         },\n         \"price\": \"11\"\n         \"weight\": 0,\n     }]\n  }",
          "type": "json"
        }
      ]
    },
    "version": "1.0.0",
    "error": {
      "examples": [
        {
          "title": "List error",
          "content": "HTTP/1.1 500 Internal Server Error",
          "type": "json"
        }
      ]
    },
    "filename": "routes/delivery.js",
    "groupTitle": "Delivery"
  },
  {
    "type": "delete",
    "url": "/api/delivery/:id",
    "title": "Delete delivery",
    "name": "DeleteDelivery",
    "group": "Delivery",
    "permission": [
      {
        "name": "none"
      }
    ],
    "sampleRequest": [
      {
        "url": "http://localhost:8080/api/delivery"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "id",
            "description": "<p>Delivery unique ID.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "id",
            "description": "<p>objectID()</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "name",
            "description": "<p>Name of delivery</p>"
          },
          {
            "group": "Success 200",
            "type": "Bool",
            "optional": false,
            "field": "enable",
            "description": "<p>Show or hide delivery</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "image",
            "description": "<p>Delivery image</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "image.target",
            "description": "<p>Image objectID()</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "image.attributes",
            "description": "<p>Image atrybutes</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "image.attributes.alt",
            "description": "<p>Alt of image</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "image.attributes.title",
            "description": "<p>Title of image</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "price",
            "description": "<p>Price of delivery</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "weight",
            "description": "<p>Weight</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success",
          "content": "HTTP/1.1 200 OK\n\n {\n     succes: true,\n     doc: {\n         \"id\": \"5a95a2921f4de421bec83ed1\",\n         \"name\": \"DPD\",\n         \"enable\": true,\n         \"image\": {\n             \"target\": \"5a95a2921f4de421bec83ed1\",\n             \"attributes\": {\n                 \"alt\": \"shoes\",\n                 \"title\": \"avesome title\"\n             }\n         },\n         \"price\": \"11\"\n         \"weight\": 0,\n     }\n  }",
          "type": "json"
        }
      ]
    },
    "version": "1.0.0",
    "error": {
      "examples": [
        {
          "title": "List error",
          "content": "HTTP/1.1 500 Internal Server Error",
          "type": "json"
        }
      ]
    },
    "filename": "routes/delivery.js",
    "groupTitle": "Delivery"
  },
  {
    "type": "get",
    "url": "/api/delivery",
    "title": "Fetch deliveries",
    "name": "FetchDelivery",
    "group": "Delivery",
    "permission": [
      {
        "name": "none"
      }
    ],
    "sampleRequest": [
      {
        "url": "http://localhost:8080/api/delivery"
      }
    ],
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "id",
            "description": "<p>objectID()</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "name",
            "description": "<p>Name of delivery</p>"
          },
          {
            "group": "Success 200",
            "type": "Bool",
            "optional": false,
            "field": "enable",
            "description": "<p>Show or hide delivery</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "image",
            "description": "<p>Delivery image</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "image.target",
            "description": "<p>Image objectID()</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "image.attributes",
            "description": "<p>Image atrybutes</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "image.attributes.alt",
            "description": "<p>Alt of image</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "image.attributes.title",
            "description": "<p>Title of image</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "price",
            "description": "<p>Price of delivery</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "weight",
            "description": "<p>Weight</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success",
          "content": "HTTP/1.1 200 OK\n\n {\n     succes: true,\n     doc: [{\n         \"id\": \"5a95a2921f4de421bec83ed1\",\n         \"name\": \"DPD\",\n         \"enable\": true,\n         \"image\": {\n             \"target\": \"5a95a2921f4de421bec83ed1\",\n             \"attributes\": {\n                 \"alt\": \"shoes\",\n                 \"title\": \"avesome title\"\n             }\n         },\n         \"price\": \"11\"\n         \"weight\": 0,\n     }]\n  }",
          "type": "json"
        }
      ]
    },
    "version": "1.0.0",
    "error": {
      "examples": [
        {
          "title": "List error",
          "content": "HTTP/1.1 500 Internal Server Error",
          "type": "json"
        }
      ]
    },
    "filename": "routes/delivery.js",
    "groupTitle": "Delivery"
  },
  {
    "type": "get",
    "url": "/api/delivery/:id",
    "title": "Get delivery",
    "name": "GetDelivery",
    "group": "Delivery",
    "permission": [
      {
        "name": "none"
      }
    ],
    "sampleRequest": [
      {
        "url": "http://localhost:8080/api/delivery"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "id",
            "description": "<p>Delivery unique ID.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "id",
            "description": "<p>objectID()</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "name",
            "description": "<p>Name of delivery</p>"
          },
          {
            "group": "Success 200",
            "type": "Bool",
            "optional": false,
            "field": "enable",
            "description": "<p>Show or hide delivery</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "image",
            "description": "<p>Delivery image</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "image.target",
            "description": "<p>Image objectID()</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "image.attributes",
            "description": "<p>Image atrybutes</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "image.attributes.alt",
            "description": "<p>Alt of image</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "image.attributes.title",
            "description": "<p>Title of image</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "price",
            "description": "<p>Price of delivery</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "weight",
            "description": "<p>Weight</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success",
          "content": "HTTP/1.1 200 OK\n\n {\n     succes: true,\n     doc: {\n         \"id\": \"5a95a2921f4de421bec83ed1\",\n         \"name\": \"DPD\",\n         \"enable\": true,\n         \"image\": {\n             \"target\": \"5a95a2921f4de421bec83ed1\",\n             \"attributes\": {\n                 \"alt\": \"shoes\",\n                 \"title\": \"avesome title\"\n             }\n         },\n         \"price\": \"11\"\n         \"weight\": 0,\n     }\n  }",
          "type": "json"
        }
      ]
    },
    "version": "1.0.0",
    "error": {
      "examples": [
        {
          "title": "List error",
          "content": "HTTP/1.1 500 Internal Server Error",
          "type": "json"
        }
      ]
    },
    "filename": "routes/delivery.js",
    "groupTitle": "Delivery"
  },
  {
    "type": "put",
    "url": "/api/delivery/:id",
    "title": "Update delivery",
    "name": "UpdateDelivery",
    "group": "Delivery",
    "permission": [
      {
        "name": "admin, moderator"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "id",
            "description": "<p>Delivery unique ID.</p>"
          }
        ],
        "JSON": [
          {
            "group": "JSON",
            "type": "String",
            "optional": true,
            "field": "name",
            "description": "<p>Name of delivery</p>"
          },
          {
            "group": "JSON",
            "type": "Bool",
            "optional": true,
            "field": "enable",
            "description": "<p>Show or hide delivery</p>"
          },
          {
            "group": "JSON",
            "type": "Object",
            "optional": true,
            "field": "image",
            "description": "<p>Delivery image</p>"
          },
          {
            "group": "JSON",
            "type": "String",
            "optional": true,
            "field": "image.target",
            "description": "<p>Image objectID()</p>"
          },
          {
            "group": "JSON",
            "type": "Object",
            "optional": true,
            "field": "image.attributes",
            "description": "<p>Image atrybutes</p>"
          },
          {
            "group": "JSON",
            "type": "String",
            "optional": true,
            "field": "image.attributes.alt",
            "description": "<p>Alt of image</p>"
          },
          {
            "group": "JSON",
            "type": "String",
            "optional": true,
            "field": "image.attributes.title",
            "description": "<p>Title of image</p>"
          },
          {
            "group": "JSON",
            "type": "String",
            "optional": true,
            "field": "price",
            "description": "<p>Delivery price</p>"
          },
          {
            "group": "JSON",
            "type": "Number",
            "optional": true,
            "field": "weight",
            "description": "<p>Weight</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request-Example:",
          "content": "\n{\n  \"Name\": \"DPD\"\n}",
          "type": "json"
        }
      ]
    },
    "sampleRequest": [
      {
        "url": "http://localhost:8080/api/delivery"
      }
    ],
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "id",
            "description": "<p>objectID()</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "name",
            "description": "<p>Name of delivery</p>"
          },
          {
            "group": "Success 200",
            "type": "Bool",
            "optional": false,
            "field": "enable",
            "description": "<p>Show or hide delivery</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "image",
            "description": "<p>Delivery image</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "image.target",
            "description": "<p>Image objectID()</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "image.attributes",
            "description": "<p>Image atrybutes</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "image.attributes.alt",
            "description": "<p>Alt of image</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "image.attributes.title",
            "description": "<p>Title of image</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "price",
            "description": "<p>Price of delivery</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "weight",
            "description": "<p>Weight</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success",
          "content": "HTTP/1.1 200 OK\n\n {\n     succes: true,\n     doc: [{\n         \"id\": \"5a95a2921f4de421bec83ed1\",\n         \"name\": \"DPD\",\n         \"enable\": true,\n         \"image\": {\n             \"target\": \"5a95a2921f4de421bec83ed1\",\n             \"attributes\": {\n                 \"alt\": \"shoes\",\n                 \"title\": \"avesome title\"\n             }\n         },\n         \"price\": \"11\"\n         \"weight\": 0,\n     }]\n  }",
          "type": "json"
        }
      ]
    },
    "version": "1.0.0",
    "error": {
      "examples": [
        {
          "title": "List error",
          "content": "HTTP/1.1 500 Internal Server Error",
          "type": "json"
        }
      ]
    },
    "filename": "routes/delivery.js",
    "groupTitle": "Delivery"
  },
  {
    "type": "post",
    "url": "/api/manufacturer",
    "title": "Add manufacturer",
    "name": "AddManufacturer",
    "group": "Manufacturer",
    "permission": [
      {
        "name": "admin, moderator"
      }
    ],
    "parameter": {
      "fields": {
        "JSON": [
          {
            "group": "JSON",
            "type": "String",
            "optional": false,
            "field": "name",
            "description": "<p>Name of manufacturer</p>"
          },
          {
            "group": "JSON",
            "type": "Object",
            "optional": true,
            "field": "image",
            "description": "<p>Manufacturer image</p>"
          },
          {
            "group": "JSON",
            "type": "String",
            "optional": true,
            "field": "image.target",
            "description": "<p>Image objectID()</p>"
          },
          {
            "group": "JSON",
            "type": "Object",
            "optional": true,
            "field": "image.attributes",
            "description": "<p>Image atrybutes</p>"
          },
          {
            "group": "JSON",
            "type": "String",
            "optional": true,
            "field": "image.attributes.alt",
            "description": "<p>Alt of image</p>"
          },
          {
            "group": "JSON",
            "type": "String",
            "optional": true,
            "field": "image.attributes.title",
            "description": "<p>Title of image</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request-Example:",
          "content": "\n{\n  \"Name\": \"Adidas\"\n}",
          "type": "json"
        }
      ]
    },
    "sampleRequest": [
      {
        "url": "http://localhost:8080/api/manufacturer"
      }
    ],
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "id",
            "description": "<p>objectID()</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "name",
            "description": "<p>Name of manufacturer</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "image",
            "description": "<p>Manufacturer image</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "image.target",
            "description": "<p>Image objectID()</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "image.attributes",
            "description": "<p>Image atrybutes</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "image.attributes.alt",
            "description": "<p>Alt of image</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "image.attributes.title",
            "description": "<p>Title of image</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success",
          "content": "HTTP/1.1 200 OK\n\n {\n     succes: true,\n     doc: [{\n         \"id\": \"5a95a2921f4de421bec83ed1\",\n         \"name\": \"DPD\",\n         \"enable\": true,\n         \"image\": {\n             \"target\": \"5a95a2921f4de421bec83ed1\",\n             \"attributes\": {\n                 \"alt\": \"shoes\",\n                 \"title\": \"avesome title\"\n             }\n         }\n     }]\n  }",
          "type": "json"
        }
      ]
    },
    "version": "1.0.0",
    "error": {
      "examples": [
        {
          "title": "List error",
          "content": "HTTP/1.1 500 Internal Server Error",
          "type": "json"
        }
      ]
    },
    "filename": "routes/manufacturer.js",
    "groupTitle": "Manufacturer"
  },
  {
    "type": "delete",
    "url": "/api/manufacturer/:id",
    "title": "Delete manufacturer",
    "name": "DeleteManufacturer",
    "group": "Manufacturer",
    "permission": [
      {
        "name": "none"
      }
    ],
    "sampleRequest": [
      {
        "url": "http://localhost:8080/api/manufacturer"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "id",
            "description": "<p>Manufacturer unique ID.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "id",
            "description": "<p>objectID()</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "name",
            "description": "<p>Name of manufacturer</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "image",
            "description": "<p>Manufacturer image</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "image.target",
            "description": "<p>Image objectID()</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "image.attributes",
            "description": "<p>Image atrybutes</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "image.attributes.alt",
            "description": "<p>Alt of image</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "image.attributes.title",
            "description": "<p>Title of image</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success",
          "content": "HTTP/1.1 200 OK\n\n {\n     succes: true,\n     doc: {\n         \"id\": \"5a95a2921f4de421bec83ed1\",\n         \"name\": \"DPD\",\n         \"enable\": true,\n         \"image\": {\n             \"target\": \"5a95a2921f4de421bec83ed1\",\n             \"attributes\": {\n                 \"alt\": \"shoes\",\n                 \"title\": \"avesome title\"\n             }\n         }\n     }\n  }",
          "type": "json"
        }
      ]
    },
    "version": "1.0.0",
    "error": {
      "examples": [
        {
          "title": "List error",
          "content": "HTTP/1.1 500 Internal Server Error",
          "type": "json"
        }
      ]
    },
    "filename": "routes/manufacturer.js",
    "groupTitle": "Manufacturer"
  },
  {
    "type": "get",
    "url": "/api/manufacturer",
    "title": "Fetch manufactures",
    "name": "FetchManufacturer",
    "group": "Manufacturer",
    "permission": [
      {
        "name": "none"
      }
    ],
    "sampleRequest": [
      {
        "url": "http://localhost:8080/api/manufacturer"
      }
    ],
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "id",
            "description": "<p>objectID()</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "name",
            "description": "<p>Name of manufacturer</p>"
          },
          {
            "group": "Success 200",
            "type": "Bool",
            "optional": false,
            "field": "enable",
            "description": "<p>Show or hide manufacturer</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "image",
            "description": "<p>Manufacturer image</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "image.target",
            "description": "<p>Image objectID()</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "image.attributes",
            "description": "<p>Image atrybutes</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "image.attributes.alt",
            "description": "<p>Alt of image</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "image.attributes.title",
            "description": "<p>Title of image</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success",
          "content": "HTTP/1.1 200 OK\n\n {\n     succes: true,\n     doc: [{\n         \"id\": \"5a95a2921f4de421bec83ed1\",\n         \"name\": \"DPD\",\n         \"enable\": true,\n         \"image\": {\n             \"target\": \"5a95a2921f4de421bec83ed1\",\n             \"attributes\": {\n                 \"alt\": \"shoes\",\n                 \"title\": \"avesome title\"\n             }\n         }\n     }]\n  }",
          "type": "json"
        }
      ]
    },
    "version": "1.0.0",
    "error": {
      "examples": [
        {
          "title": "List error",
          "content": "HTTP/1.1 500 Internal Server Error",
          "type": "json"
        }
      ]
    },
    "filename": "routes/manufacturer.js",
    "groupTitle": "Manufacturer"
  },
  {
    "type": "get",
    "url": "/api/manufacturer/:id",
    "title": "Get manufacturer",
    "name": "GetManufacturer",
    "group": "Manufacturer",
    "permission": [
      {
        "name": "none"
      }
    ],
    "sampleRequest": [
      {
        "url": "http://localhost:8080/api/manufacturer"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "id",
            "description": "<p>Manufacturer unique ID.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "id",
            "description": "<p>objectID()</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "name",
            "description": "<p>Name of manufacturer</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "image",
            "description": "<p>Manufacturer image</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "image.target",
            "description": "<p>Image objectID()</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "image.attributes",
            "description": "<p>Image atrybutes</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "image.attributes.alt",
            "description": "<p>Alt of image</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "image.attributes.title",
            "description": "<p>Title of image</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success",
          "content": "HTTP/1.1 200 OK\n\n {\n     succes: true,\n     doc: {\n         \"id\": \"5a95a2921f4de421bec83ed1\",\n         \"name\": \"DPD\",\n         \"enable\": true,\n         \"image\": {\n             \"target\": \"5a95a2921f4de421bec83ed1\",\n             \"attributes\": {\n                 \"alt\": \"shoes\",\n                 \"title\": \"avesome title\"\n             }\n         }\n     }\n  }",
          "type": "json"
        }
      ]
    },
    "version": "1.0.0",
    "error": {
      "examples": [
        {
          "title": "List error",
          "content": "HTTP/1.1 500 Internal Server Error",
          "type": "json"
        }
      ]
    },
    "filename": "routes/manufacturer.js",
    "groupTitle": "Manufacturer"
  },
  {
    "type": "put",
    "url": "/api/manufacturer/:id",
    "title": "Update manufacturer",
    "name": "UpdateManufacturer",
    "group": "Manufacturer",
    "permission": [
      {
        "name": "admin, moderator"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "id",
            "description": "<p>Manufacturer unique ID.</p>"
          }
        ],
        "JSON": [
          {
            "group": "JSON",
            "type": "String",
            "optional": true,
            "field": "name",
            "description": "<p>Name of manufacturer</p>"
          },
          {
            "group": "JSON",
            "type": "Object",
            "optional": true,
            "field": "image",
            "description": "<p>Manufacturer image</p>"
          },
          {
            "group": "JSON",
            "type": "String",
            "optional": true,
            "field": "image.target",
            "description": "<p>Image objectID()</p>"
          },
          {
            "group": "JSON",
            "type": "Object",
            "optional": true,
            "field": "image.attributes",
            "description": "<p>Image atrybutes</p>"
          },
          {
            "group": "JSON",
            "type": "String",
            "optional": true,
            "field": "image.attributes.alt",
            "description": "<p>Alt of image</p>"
          },
          {
            "group": "JSON",
            "type": "String",
            "optional": true,
            "field": "image.attributes.title",
            "description": "<p>Title of image</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request-Example:",
          "content": "\n{\n  \"Name\": \"Adidas\"\n}",
          "type": "json"
        }
      ]
    },
    "sampleRequest": [
      {
        "url": "http://localhost:8080/api/manufacturer"
      }
    ],
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "id",
            "description": "<p>objectID()</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "name",
            "description": "<p>Name of manufacturer</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "image",
            "description": "<p>Manufacturer image</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "image.target",
            "description": "<p>Image objectID()</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "image.attributes",
            "description": "<p>Image atrybutes</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "image.attributes.alt",
            "description": "<p>Alt of image</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "image.attributes.title",
            "description": "<p>Title of image</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success",
          "content": "HTTP/1.1 200 OK\n\n {\n     succes: true,\n     doc: [{\n         \"id\": \"5a95a2921f4de421bec83ed1\",\n         \"name\": \"DPD\",\n         \"enable\": true,\n         \"image\": {\n             \"target\": \"5a95a2921f4de421bec83ed1\",\n             \"attributes\": {\n                 \"alt\": \"shoes\",\n                 \"title\": \"avesome title\"\n             }\n         }\n     }]\n  }",
          "type": "json"
        }
      ]
    },
    "version": "1.0.0",
    "error": {
      "examples": [
        {
          "title": "List error",
          "content": "HTTP/1.1 500 Internal Server Error",
          "type": "json"
        }
      ]
    },
    "filename": "routes/manufacturer.js",
    "groupTitle": "Manufacturer"
  },
  {
    "type": "get",
    "url": "/api/products",
    "title": "get products list",
    "group": "Products",
    "permission": [
      {
        "name": "none"
      }
    ],
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "id",
            "description": "<p>objectID()</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "name",
            "description": "<p>Product Name</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "body",
            "description": "<p>Product Description</p>"
          },
          {
            "group": "Success 200",
            "type": "Bool",
            "optional": false,
            "field": "active",
            "description": "<p>Product Published</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "status",
            "description": "<p>enum: &quot;enabled&quot;, &quot;disabled&quot;, &quot;outOfStock&quot;</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "categoryId",
            "description": "<p>ID category</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "manufacturerId",
            "description": "<p>ID manufacturer</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "tax",
            "description": "<p>enum: 0.23, 0.07, 0.21</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "created",
            "description": "<p>Data and user</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "created.userId",
            "description": "<p>User Id</p>"
          },
          {
            "group": "Success 200",
            "type": "Date",
            "optional": false,
            "field": "created.date",
            "description": "<p>Crated Date</p>"
          },
          {
            "group": "Success 200",
            "type": "Date",
            "optional": false,
            "field": "available",
            "description": "<p>Available Date</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "stock",
            "description": "<p>Product stock</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "price",
            "description": "<p>Product Price</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "promotion",
            "description": "<p>product published</p>"
          },
          {
            "group": "Success 200",
            "type": "Bool",
            "optional": false,
            "field": "promotion.active",
            "description": "<p>active promotion</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "promotion.newPrice",
            "description": "<p>active promotion</p>"
          },
          {
            "group": "Success 200",
            "type": "Date",
            "optional": false,
            "field": "promotion.startDate",
            "description": "<p>Promotion started</p>"
          },
          {
            "group": "Success 200",
            "type": "Date",
            "optional": false,
            "field": "promotion.expiryDate",
            "description": "<p>Promotion expiry</p>"
          },
          {
            "group": "Success 200",
            "type": "Object[]",
            "optional": false,
            "field": "images",
            "description": "<p>Images</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "images.imageId",
            "description": "<p>Image Id</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "images.alt",
            "description": "<p>Image alt</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "images.title",
            "description": "<p>Image title</p>"
          },
          {
            "group": "Success 200",
            "type": "String[]",
            "optional": false,
            "field": "relatedIds",
            "description": "<p>Array of related Products</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "path",
            "description": "<p>path</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success",
          "content": "HTTP/1.1 200 OK\n\n {\n     succes: true,\n     doc: [{\n         \"id\": \"5a95a2921f4de421bec83ed1\",\n         \"name\": \"Buty Adidas\"\n         \"body\": \"ospi produktu\"\n         \"active\": true,\n         \"status\": \"enabled\",\n         \"categoryId\": \"5a80a0dc37737c024b0ff4ca\",\n         \"manufacturerId\": \"5a95a2921f4de421bec83ed1\",\n         \"tax\": 0.23,\n         \"created\": {\n             \"userId\": \"5a95a2921f4de421bec83ed1\",\n             \"date\": \"2018-03-24T11:38:04.360Z\",\n         },\n         \"available\": \"2018-03-24T11:38:04.360Z\",\n         \"stock\": 20,\n         \"price\": 22.2,\n         \"promotion\": {\n             \"active\": false,\n             \"newPrice\": 19,\n             \"startDate\": \"2018-03-24T11:38:04.360Z\"\n             \"expiryDate\": \"2018-03-29T11:38:04.360Z\"\n         },\n         \"images\": [\n             {\n                 \"imageId\": \"5a95a2921f4de421bec83ed1\",\n                 \"alt\": \"alt image\",\n                 \"title\": \"title image\"\n             }\n         ],\n         \"relatedIds\": [\n             \"5a95a2921f4de421bec83ed1\", \"5a95a2921f4de421bec83ed1\"\n         ]\n         \"path\": \"/catalog/buty-adidas\",\n     }]\n  }",
          "type": "json"
        }
      ]
    },
    "version": "1.0.0",
    "error": {
      "examples": [
        {
          "title": "List error",
          "content": "HTTP/1.1 500 Internal Server Error",
          "type": "json"
        }
      ]
    },
    "filename": "routes/product.js",
    "groupTitle": "Products",
    "name": "GetApiProducts"
  },
  {
    "type": "post",
    "url": "/api/role",
    "title": "Add role",
    "name": "AddRole",
    "group": "Role",
    "permission": [
      {
        "name": "admin, moderator"
      }
    ],
    "parameter": {
      "fields": {
        "JSON": [
          {
            "group": "JSON",
            "type": "String",
            "optional": false,
            "field": "name",
            "description": "<p>Name of role</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request-Example:",
          "content": "\n{\n  \"Name\": \"Admin\"\n}",
          "type": "json"
        }
      ]
    },
    "sampleRequest": [
      {
        "url": "http://localhost:8080/api/role"
      }
    ],
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "id",
            "description": "<p>objectID()</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "name",
            "description": "<p>Name of role</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "hash",
            "description": "<p>Hash of role</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success",
          "content": "HTTP/1.1 200 OK\n\n {\n     succes: true,\n     doc: {\n         \"id\": \"5a95a2921f4de421bec83ed1\",\n         \"name\": \"Admin\",\n         \"hash\": \"21232F297A57A5A743894A0E4A801FC3\"\n     }\n  }",
          "type": "json"
        }
      ]
    },
    "version": "1.0.0",
    "error": {
      "examples": [
        {
          "title": "List error",
          "content": "HTTP/1.1 500 Internal Server Error",
          "type": "json"
        }
      ]
    },
    "filename": "routes/role.js",
    "groupTitle": "Role"
  },
  {
    "type": "delete",
    "url": "/api/role/:id",
    "title": "Delete role",
    "name": "DeleteRole",
    "group": "Role",
    "permission": [
      {
        "name": "none"
      }
    ],
    "sampleRequest": [
      {
        "url": "http://localhost:8080/api/role"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "id",
            "description": "<p>Role unique ID.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "id",
            "description": "<p>objectID()</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "name",
            "description": "<p>Name of role</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "hash",
            "description": "<p>Hash of role</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success",
          "content": "HTTP/1.1 200 OK\n\n {\n     succes: true,\n     doc: {\n         \"id\": \"5a95a2921f4de421bec83ed1\",\n         \"name\": \"Admin\",\n         \"hash\": \"21232F297A57A5A743894A0E4A801FC3\"\n     }\n  }",
          "type": "json"
        }
      ]
    },
    "version": "1.0.0",
    "error": {
      "examples": [
        {
          "title": "List error",
          "content": "HTTP/1.1 500 Internal Server Error",
          "type": "json"
        }
      ]
    },
    "filename": "routes/role.js",
    "groupTitle": "Role"
  },
  {
    "type": "get",
    "url": "/api/role",
    "title": "Fetch Roles",
    "name": "FetchRole",
    "group": "Role",
    "permission": [
      {
        "name": "Admin"
      }
    ],
    "sampleRequest": [
      {
        "url": "http://localhost:8080/api/role"
      }
    ],
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "id",
            "description": "<p>objectID()</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "name",
            "description": "<p>Name of role</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "hash",
            "description": "<p>Hash of role</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success",
          "content": "HTTP/1.1 200 OK\n\n {\n     succes: true,\n     doc: [{\n         \"id\": \"5a95a2921f4de421bec83ed1\",\n         \"name\": \"Admin\",\n         \"hash\": \"21232F297A57A5A743894A0E4A801FC3\"\n     }]\n  }",
          "type": "json"
        }
      ]
    },
    "version": "1.0.0",
    "error": {
      "examples": [
        {
          "title": "List error",
          "content": "HTTP/1.1 500 Internal Server Error",
          "type": "json"
        }
      ]
    },
    "filename": "routes/role.js",
    "groupTitle": "Role"
  },
  {
    "type": "get",
    "url": "/api/role/:id",
    "title": "Get role",
    "name": "GetRole",
    "group": "Role",
    "permission": [
      {
        "name": "none"
      }
    ],
    "sampleRequest": [
      {
        "url": "http://localhost:8080/api/role"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "id",
            "description": "<p>Role unique ID.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "id",
            "description": "<p>objectID()</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "name",
            "description": "<p>Name of role</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "hash",
            "description": "<p>Hash of role</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success",
          "content": "HTTP/1.1 200 OK\n\n {\n     succes: true,\n     doc: {\n         \"id\": \"5a95a2921f4de421bec83ed1\",\n         \"name\": \"Admin\",\n         \"hash\": \"21232F297A57A5A743894A0E4A801FC3\"\n     }\n  }",
          "type": "json"
        }
      ]
    },
    "version": "1.0.0",
    "error": {
      "examples": [
        {
          "title": "List error",
          "content": "HTTP/1.1 500 Internal Server Error",
          "type": "json"
        }
      ]
    },
    "filename": "routes/role.js",
    "groupTitle": "Role"
  },
  {
    "type": "put",
    "url": "/api/role/:id",
    "title": "Update role",
    "name": "UpdateRole",
    "group": "Role",
    "permission": [
      {
        "name": "admin, moderator"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "id",
            "description": "<p>Role unique ID.</p>"
          }
        ],
        "JSON": [
          {
            "group": "JSON",
            "type": "String",
            "optional": true,
            "field": "name",
            "description": "<p>Name of role</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request-Example:",
          "content": "\n{\n  \"Name\": \"Moderator\"\n}",
          "type": "json"
        }
      ]
    },
    "sampleRequest": [
      {
        "url": "http://localhost:8080/api/role"
      }
    ],
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "id",
            "description": "<p>objectID()</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "name",
            "description": "<p>Name of role</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "hash",
            "description": "<p>Hash of role</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success",
          "content": "HTTP/1.1 200 OK\n\n {\n     succes: true,\n     doc: {\n         \"id\": \"5a95a2921f4de421bec83ed1\",\n         \"name\": \"Admin\",\n         \"hash\": \"21232F297A57A5A743894A0E4A801FC3\"\n     }\n  }",
          "type": "json"
        }
      ]
    },
    "version": "1.0.0",
    "error": {
      "examples": [
        {
          "title": "List error",
          "content": "HTTP/1.1 500 Internal Server Error",
          "type": "json"
        }
      ]
    },
    "filename": "routes/role.js",
    "groupTitle": "Role"
  },
  {
    "type": "post",
    "url": "/api/tax",
    "title": "Add tax",
    "name": "AddTax",
    "group": "Tax",
    "permission": [
      {
        "name": "admin, moderator"
      }
    ],
    "parameter": {
      "fields": {
        "JSON": [
          {
            "group": "JSON",
            "type": "String",
            "optional": false,
            "field": "name",
            "description": "<p>Name of tax</p>"
          },
          {
            "group": "JSON",
            "type": "String",
            "optional": false,
            "field": "taxRate",
            "description": "<p>Rate of tax</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request-Example:",
          "content": "\n{\n  \"Name\": \"23%\",\n  \"taxRate\": 0.23\n}",
          "type": "json"
        }
      ]
    },
    "sampleRequest": [
      {
        "url": "http://localhost:8080/api/tax"
      }
    ],
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "id",
            "description": "<p>objectID()</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "name",
            "description": "<p>Name of tax</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "taxRate",
            "description": "<p>Rate of tax</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success",
          "content": "HTTP/1.1 200 OK\n\n {\n     succes: true,\n     doc: {\n         \"id\": \"5a95a2921f4de421bec83ed1\",\n         \"name\": \"Admin\",\n         \"taxRate\": \"0.23\"\n     }\n  }",
          "type": "json"
        }
      ]
    },
    "version": "1.0.0",
    "error": {
      "examples": [
        {
          "title": "List error",
          "content": "HTTP/1.1 500 Internal Server Error",
          "type": "json"
        }
      ]
    },
    "filename": "routes/tax.js",
    "groupTitle": "Tax"
  },
  {
    "type": "delete",
    "url": "/api/tax/:id",
    "title": "Delete tax",
    "name": "DeleteTax",
    "group": "Tax",
    "permission": [
      {
        "name": "none"
      }
    ],
    "sampleRequest": [
      {
        "url": "http://localhost:8080/api/tax"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "id",
            "description": "<p>Tax unique ID.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "id",
            "description": "<p>objectID()</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "name",
            "description": "<p>Name of tax</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "taxRate",
            "description": "<p>Rate of tax</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success",
          "content": "HTTP/1.1 200 OK\n\n {\n     succes: true,\n     doc: {\n         \"id\": \"5a95a2921f4de421bec83ed1\",\n         \"name\": \"Admin\",\n         \"taxRate\": \"0.23\"\n     }\n  }",
          "type": "json"
        }
      ]
    },
    "version": "1.0.0",
    "error": {
      "examples": [
        {
          "title": "List error",
          "content": "HTTP/1.1 500 Internal Server Error",
          "type": "json"
        }
      ]
    },
    "filename": "routes/tax.js",
    "groupTitle": "Tax"
  },
  {
    "type": "get",
    "url": "/api/tax",
    "title": "Fetch Taxes",
    "name": "FetchTax",
    "group": "Tax",
    "permission": [
      {
        "name": "Admin"
      }
    ],
    "sampleRequest": [
      {
        "url": "http://localhost:8080/api/tax"
      }
    ],
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "id",
            "description": "<p>objectID()</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "name",
            "description": "<p>Name of tax</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "taxRate",
            "description": "<p>Rate of tax</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success",
          "content": "HTTP/1.1 200 OK\n\n {\n     succes: true,\n     doc: [{\n         \"id\": \"5a95a2921f4de421bec83ed1\",\n         \"name\": \"Admin\",\n         \"taxRate\": \"0.23\"\n     }]\n  }",
          "type": "json"
        }
      ]
    },
    "version": "1.0.0",
    "error": {
      "examples": [
        {
          "title": "List error",
          "content": "HTTP/1.1 500 Internal Server Error",
          "type": "json"
        }
      ]
    },
    "filename": "routes/tax.js",
    "groupTitle": "Tax"
  },
  {
    "type": "get",
    "url": "/api/tax/:id",
    "title": "Get tax",
    "name": "GetTax",
    "group": "Tax",
    "permission": [
      {
        "name": "none"
      }
    ],
    "sampleRequest": [
      {
        "url": "http://localhost:8080/api/tax"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "id",
            "description": "<p>Tax unique ID.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "id",
            "description": "<p>objectID()</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "name",
            "description": "<p>Name of tax</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "taxRate",
            "description": "<p>Rate of tax</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success",
          "content": "HTTP/1.1 200 OK\n\n {\n     succes: true,\n     doc: {\n         \"id\": \"5a95a2921f4de421bec83ed1\",\n         \"name\": \"Admin\",\n         \"taxRate\": \"0.23\"\n     }\n  }",
          "type": "json"
        }
      ]
    },
    "version": "1.0.0",
    "error": {
      "examples": [
        {
          "title": "List error",
          "content": "HTTP/1.1 500 Internal Server Error",
          "type": "json"
        }
      ]
    },
    "filename": "routes/tax.js",
    "groupTitle": "Tax"
  },
  {
    "type": "put",
    "url": "/api/tax/:id",
    "title": "Update tax",
    "name": "UpdateTax",
    "group": "Tax",
    "permission": [
      {
        "name": "admin, moderator"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "id",
            "description": "<p>Tax unique ID.</p>"
          }
        ],
        "JSON": [
          {
            "group": "JSON",
            "type": "String",
            "optional": true,
            "field": "name",
            "description": "<p>Name of tax</p>"
          },
          {
            "group": "JSON",
            "type": "String",
            "optional": true,
            "field": "taxRate",
            "description": "<p>Rate of tax</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request-Example:",
          "content": "\n{\n  \"Name\": \"23%\",\n  \"taxRate\": 0.23\n}",
          "type": "json"
        }
      ]
    },
    "sampleRequest": [
      {
        "url": "http://localhost:8080/api/tax"
      }
    ],
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "id",
            "description": "<p>objectID()</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "name",
            "description": "<p>Name of tax</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "taxRate",
            "description": "<p>Rate of tax</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success",
          "content": "HTTP/1.1 200 OK\n\n {\n     succes: true,\n     doc: {\n         \"id\": \"5a95a2921f4de421bec83ed1\",\n         \"name\": \"Admin\",\n         \"taxRate\": \"0.23\"\n     }\n  }",
          "type": "json"
        }
      ]
    },
    "version": "1.0.0",
    "error": {
      "examples": [
        {
          "title": "List error",
          "content": "HTTP/1.1 500 Internal Server Error",
          "type": "json"
        }
      ]
    },
    "filename": "routes/tax.js",
    "groupTitle": "Tax"
  }
] });
