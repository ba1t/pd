const LocalStrategy = require('passport-local');
const bcrypt = require('bcryptjs');
const { User } = require('../models');
module.exports = (keys, passport) => {
    passport.serializeUser((user, done) => {

        done(null, user.id)
    })
    passport.deserializeUser( async (id, done)=> {
        const existingUser = await User.findById(id);
        done(null, existingUser);
    })
    passport.use(
        new LocalStrategy ( async ( username, password, done ) => {
                const existingUser = await User.findOne({ username: username });
                if (!existingUser) { return done(null, false); }
                if (!bcrypt.compareSync(password, existingUser.password)) { return done(null, false); }
                return done(null, existingUser);
            }
        )
    )
}


