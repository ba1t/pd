'use strict';
const mongoose = require('mongoose');

const { Schema } = mongoose;

const categorySchema = new Schema ({
    name : { type: String, required: true},
    enable: { type: Boolean, require: true, default: true },
    image: {
        target: { type: Schema.ObjectId, ref: 'Image' },
        attributes : {
            alt : String, 
            title : String
        }
    }, 
    weight : {type: Number, default: 0}, 
    parentCategory : { type: Schema.ObjectId, ref: 'Category' },
    path: {type: String},
});

module.exports = mongoose.model('Category', categorySchema);