'use strict';
const mongoose = require('mongoose');

const { Schema } = mongoose;

const orderStatusSchema = new Schema ({
    name: {type: String},
    notification: {
        email: {type: Boolean, default: false},
        phone: {type: Boolean, default: false}
    }
});

module.exports = mongoose.model('OrderStatus', orderStatusSchema);