'use strict';
const mongoose = require('mongoose');
const md5 = require('md5');

const { Schema } = mongoose;
const roleSchema = new Schema ({
    name: { type: String, require: true },
    hash: { type: String }
})


// roleSchema.pre('save', next => {
//     var role = this;
//     role.hash = md5(role.name);
//     next()
// })
module.exports = mongoose.model('Role', roleSchema);
