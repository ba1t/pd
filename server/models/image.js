'use strict';
const mongoose = require('mongoose');

const { Schema } = mongoose;

const imageSchema = new Schema ({
    path: { type: String, required: true},
    name: {type: String},
    height: { type: String},
    width: {type: String},
    extension: {type: String},
}, {timestamps:true});

module.exports = mongoose.model('Image', imageSchema);