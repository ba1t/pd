'use strict';
const mongoose = require('mongoose');

const { Schema } = mongoose;

const paymentSchema = new Schema ({
    name : { type: String, required: true},
    enable: { type: Boolean, require: true, default: true },
    image: {
        target: { type: Schema.ObjectId, ref: 'Image' },
        attributes : {
            alt : String, 
            title : String
        }
    }, 
    price: {type: String},
    weight : {type: Number, default: 0}, 
});

module.exports = mongoose.model('Payment', paymentSchema);