module.exports = {
    User: require('./user'),
    Role: require('./role'),
    Product: require('./product'),
    ProductStatus: require('./productStatus'),
    Category: require('./category'),
    Tax: require('./tax'),
    Manufacturer: require('./manufacturer'),
    Image: require('./image'),
    Customer: require('./customer'),
    Delivery: require('./delivery'),
    Payment: require('./payment'),
    Order: require('./order'),
    OrderStatus: require('./orderStatus')
}