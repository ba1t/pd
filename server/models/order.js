'use strict';
const mongoose = require('mongoose');

const { Schema } = mongoose;

const orderSchema = new Schema ({
    products: [
        {type: Object} 
    ],
    paymentName: {type: String},
    delivery: {
        name: {type: String},
        price: {type: String}
    },
    adress: {
        adress1: {type: String},
        adress2: {type: String},
        postcode: {type: String},
        city: {type: String}
    },
    phone: {
        type: String
    },
    status: { type: Schema.ObjectId, ref: 'OrderStaus' }
},{timestamp: true});

module.exports = mongoose.model('Order', orderSchema);