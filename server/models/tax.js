'use strict';
const mongoose = require('mongoose');
const { Schema } = mongoose;

const taxSchema = new Schema ({
    name: { type: String, require: true},
    taxRate: {type: Number, required: true}
})

module.exports = mongoose.model('Tax', taxSchema);