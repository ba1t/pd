'use strict';
const mongoose = require('mongoose');

const { Schema } = mongoose;

const productStatusSchema = new Schema ({
    name: {type: String},
    canBuy: {type: Boolean, default: true}
});

module.exports = mongoose.model('ProductStatus', productStatusSchema);