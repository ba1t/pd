'use strict';
const mongoose = require('mongoose');

const { Schema } = mongoose;

const productSchema = new Schema ({
    name: { type: String, require: true },
    body: String,//
    published: { type: Boolean, default: false },//
    status: { type: Schema.ObjectId, ref: 'ProductStatus'},//
    categoryId: { type: Schema.ObjectId, ref: 'Category' },//
    additionalCategoriesIds: [ { type: Schema.ObjectId, ref: 'Category' } ],//
    manufacturerId: { type: Schema.ObjectId, ref: 'Manufacturer' },
    taxId: { type: Schema.ObjectId, ref: 'Tax' },
    created: {
        userId: { type: Schema.ObjectId, ref: 'User'},
        date: { type: Date, default: Date.now }
    },//
    available: Date, //
    stock: { type: Number, default: 0 }, //    
    price: { type: Number },
    promotion: {
        active: { type: Boolean, default: false },
        newPrice: String,
    },
    image: {
        target: { type: Schema.ObjectId, ref: 'Image' },
        attributes : {
            alt : String, 
            title : String
        }
    }, 
    relatedIds: [{ 
        type: Schema.ObjectId, ref: 'Product' 
    }],
    path: String,   
    metatags: {
        title: { type: String },
        description: { typ: String },
        keywords: String
    }
})

module.exports = mongoose.model('Product', productSchema);
