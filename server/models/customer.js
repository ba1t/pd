const mongoose = require('mongoose');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const {keys} = require('./../config');
const SALT_I = 10;

const { Schema } = mongoose;

const customerSchema = new Schema ({
    email: { type: String, unique: true, require: true, validate: /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/ },
    password: { type: String, require: true },
    firstName: { type: String },
    lastName: { type: String },
    token:{type: String},
    ordersIds: [{type: Schema.ObjectId, ref: 'Order'}],
    cart: [
        {
            item: {type: Schema.ObjectId, ref: 'Product'},
            count: {type: Number}
        }],
    enable: {type: Boolean, default: true},
    address: {
        street: {type: String},
        houseNumber: {type: String},
        flatNumber: {type: String},
        postcode: {type: String},
        city: {type: String}
    },
    phone: {type: String, max: 9}
})
customerSchema.pre('save',function(next){
    var customer = this;

    if(customer.isModified('password')){
        bcrypt.genSalt(SALT_I,function(err,salt){
            if(err) return next(err);

            bcrypt.hash(customer.password,salt,function(err,hash){
                if(err) return next(err);
                customer.password = hash;
                next();
            })
        })
    } else {
        next()
    }
})

customerSchema.methods.comparePassword = function(candidatePassword,cb){
    bcrypt.compare(candidatePassword,this.password,function(err,isMatch){
        if(err) return cb(err);
        cb(null,isMatch);
    })
}

customerSchema.methods.generateToken = function(cb){
    var customer = this;
    var token = jwt.sign(customer._id.toHexString(),keys.jwt);

    customer.token = token;
    customer.save(function(err,customer){
        if(err) return cb(err);
        cb(null,customer)
    })
}

customerSchema.statics.findByToken = function(token,cb){
    var customer  = this;

    jwt.verify(token,keys.jwt,function(err,decode){
        customer.findOne({"_id":decode,"token":token},function(err,customer){
            if(err) return cb(err);
            cb(null,customer)
        })
    })
}


customerSchema.methods.deleteToken = function(token,cb){
    var customer = this;

    customer.update({$unset:{token:1}},(err,customer)=>{
        if(err) return cb(err);
        cb(null,customer)
    })
}
module.exports = mongoose.model('Cusomer', customerSchema);