'use strict';
const mongoose = require('mongoose');
const { Schema } = mongoose;

const manufacturerSchema = new Schema ({
    name : { type: String, required: true},
    image: {
        target: { type: Schema.ObjectId, ref: 'Image' },
        attributes : {
            alt : String, 
            title : String
        }
    }, 
});

module.exports = mongoose.model('Manufacturer', manufacturerSchema);