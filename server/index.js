global.__base = __dirname + '/';
const path = require("path");
const multer = require("multer");

const {keys, db, cookieKey} = require(__base + 'config');

const express = require('express');
const cookieParser = require('cookie-parser')
const bodyParser = require('body-parser');
const cors = require('cors');
const mongoose = require('mongoose');

mongoose.connect(db, { useNewUrlParser: true })

const app = express();

app.use(cors());

app.use(bodyParser.urlencoded({ extended: true })) 
app.use(bodyParser.json())
app.use(cookieParser());
app.use('/public', express.static('./public'))
app.use('/apidoc', express.static('./public/apidoc'))
app.use(express.static(path.join(__dirname, '..', 'client' , 'build')));
require(__base + 'routes')(app)

app.get('/*', function(req, res) {
   res.sendFile(path.join(__dirname, '..', 'client', 'build', 'index.html'));
 });


const storage = multer.diskStorage({
    destination: "./public/uploads/",
    filename: function(req, file, cb){
       cb(null,"IMAGE-" + Date.now() + path.extname(file.originalname));
    }
 });
 
 const upload = multer({
    storage: storage,
    limits:{fileSize: 1000000},
 }).single("image");


 app.post("/api/upload", upload, (req, res) => {
    console.log("Request ---", req.body);
    console.log("Request file ---", req.file);//Here you get file.
    return res.json({success: true, file: req.file});
})

app.use(require('morgan')('dev'));
const PORT = process.env.PORT || 8080;

app.listen(PORT);
