const proxy = require('http-proxy-middleware');

module.exports = function(app) {
  app.use(proxy('/api', { target: 'http://localhost:8080/' }));
  app.use(proxy('/cart', { target: 'http://localhost:8080/' }));
  app.use(proxy('/public',{ target: 'http://localhost:8080/'}));
};