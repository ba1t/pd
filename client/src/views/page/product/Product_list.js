import React  from 'react';
import {ProductListClient} from '../../../modules/product/';

function ProductList (props) {
    return (
        <ProductListClient route={props.route}/>
    )
}

export default ProductList;