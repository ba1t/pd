import React  from 'react';
import { DeliveryFormAdmin }  from '../../../../modules/delivery'

function DeliveryEdit (props) {
    return (
        <DeliveryFormAdmin {...props} edit />
    )
}

export default DeliveryEdit;