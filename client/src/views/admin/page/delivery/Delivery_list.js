import React from 'react';
import {DeliveryTableAdmin} from '../../../../modules/delivery'

function DeliveryList (props) {
    return (
        <DeliveryTableAdmin {...props} />
    )
}

export default DeliveryList;