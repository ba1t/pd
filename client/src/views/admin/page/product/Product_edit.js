import React from 'react';
import { ProductFormAdmin }  from '../../../../modules/product'


function ProductEdit(props) {
    return (
        <ProductFormAdmin {...props} edit/>
    )
}

export default ProductEdit;