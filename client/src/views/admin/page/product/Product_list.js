import React from 'react';
import {ProductTableAdmin} from '../../../../modules/product/'


function ProductList(props) {
    return (
        <ProductTableAdmin {...props} />
    )
}

export default ProductList;