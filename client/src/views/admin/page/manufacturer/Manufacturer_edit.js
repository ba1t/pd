import React  from 'react';
import { ManufacturerFormAdmin }  from '../../../../modules/manufacturer'

function ManufacturerEdit (props) {
    return (
        <ManufacturerFormAdmin {...props} edit />
    )
}

export default ManufacturerEdit;