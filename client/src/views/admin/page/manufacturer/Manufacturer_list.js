import React from 'react';
import {ManufacturerTableAdmin} from '../../../../modules/manufacturer/'

function ManufacturerList (props) {
    return (
        <ManufacturerTableAdmin {...props} />
    )
}

export default ManufacturerList;