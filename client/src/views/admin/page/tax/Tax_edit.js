import React  from 'react';
import { TaxFormAdmin }  from '../../../../modules/tax'

function TaxEdit (props) {
    return (
        <TaxFormAdmin {...props} edit />
    )
}

export default TaxEdit;