import React from 'react';
import {TaxTableAdmin} from '../../../../modules/tax'

function TaxList (props) {
    return (
        <TaxTableAdmin {...props} />
    )
}

export default TaxList;