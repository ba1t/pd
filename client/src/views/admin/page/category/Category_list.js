import React from 'react';
import {CategoryTableAdmin} from '../../../../modules/category/'


function CategoryList(props) {
    return (
        <CategoryTableAdmin {...props} />
    )
}

export default CategoryList;