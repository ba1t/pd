import React from 'react';
import { CategoryFormAdmin }  from '../../../../modules/category'



function CategoryEdit(props) {
    return (
        <CategoryFormAdmin {...props} edit />
    )
}

export default CategoryEdit;