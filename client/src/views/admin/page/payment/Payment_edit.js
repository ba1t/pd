import React  from 'react';
import { PaymentFormAdmin }  from '../../../../modules/payment'

function PaymentEdit (props) {
    return (
        <PaymentFormAdmin {...props} edit />
    )
}

export default PaymentEdit;