import React from 'react';
import {PaymentTableAdmin} from '../../../../modules/payment'

function PaymentList (props) {
    return (
        <PaymentTableAdmin {...props} />
    )
}

export default PaymentList;