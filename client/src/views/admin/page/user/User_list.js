import React from 'react';
import {UserTableAdmin} from '../../../../modules/user/'


function UserList(props) {
    return (
        <UserTableAdmin {...props} />
    )
}

export default UserList;