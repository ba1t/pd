import React from 'react';
import { Link as RouterLink } from 'react-router-dom'
import Link from '@material-ui/core/Link';

import PropTypes from 'prop-types';
import clsx from 'clsx';
import { withStyles } from '@material-ui/core/styles';
import Divider from '@material-ui/core/Divider';
import Drawer from '@material-ui/core/Drawer';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import HomeIcon from '@material-ui/icons/Home';
import PeopleIcon from '@material-ui/icons/People';
import DnsRoundedIcon from '@material-ui/icons/DnsRounded';
import PermMediaOutlinedIcon from '@material-ui/icons/PhotoSizeSelectActual';
import PublicIcon from '@material-ui/icons/Public';
import SettingsEthernetIcon from '@material-ui/icons/SettingsEthernet';
import SettingsInputComponentIcon from '@material-ui/icons/SettingsInputComponent';
import TimerIcon from '@material-ui/icons/Timer';
import SettingsIcon from '@material-ui/icons/Settings';
import PhonelinkSetupIcon from '@material-ui/icons/PhonelinkSetup';

const categories = [
  {
    id: 'Catalog',
    children: [
      { id: 'Product', icon: <PeopleIcon />, link: '/admin/product' },
      { id: 'Category', icon: <DnsRoundedIcon />, link: '/admin/category', },
      { id: 'Manufacturer', icon: <PermMediaOutlinedIcon />, link: '/admin/manufacturer', },
    ],
  },
  {
    id: 'E-commerce',
    children: [
      { id: 'Order', icon: <SettingsIcon />, link: '/admin/order', },
      { id: 'Cart', icon: <TimerIcon />, link: '/admin/basket', },
      { id: 'Customer', icon: <PhonelinkSetupIcon />, link: '/admin/customer', },
    ],
  },
  {
    id: 'Config',
    children: [
      { id: 'User', icon: <SettingsIcon />, link: '/admin/user', },
      { id: 'Status', icon: <TimerIcon />, link: '/admin/status', },
      { id: 'Tax', icon: <PhonelinkSetupIcon />, link: '/admin/tax'},
      { id: 'Payment', icon: <PhonelinkSetupIcon />, link: '/admin/payment', },
      { id: 'Delivery', icon: <PhonelinkSetupIcon />, link: '/admin/delivery', },
    ],
  },
];

const styles = theme => ({
  categoryHeader: {
    paddingTop: 16,
    paddingBottom: 16,
  },
  categoryHeaderPrimary: {
    color: theme.palette.common.white,
  },
  item: {
    paddingTop: 15,
    paddingBottom: 15,
    color: 'rgba(255, 255, 255, 0.7)',
  },
  itemCategory: {
    backgroundColor: '#363636',
    // boxShadow: '0 -1px 0 #404854 inset',
    paddingTop: 16,
    paddingBottom: 16,
  },
  firebase: {
    fontSize: 24,
    fontFamily: theme.typography.fontFamily,
    color: theme.palette.common.white,
  },
  itemActionable: {
    '&:hover': {
      backgroundColor: 'rgba(255, 255, 255, 0.08)',
    },
  },
  itemActiveItem: {
    color: '#f50057',
  },
  itemPrimary: {
    color: 'inherit',
    fontSize: theme.typography.fontSize,
    '&$textDense': {
      fontSize: theme.typography.fontSize,
    },
  },
  textDense: {},
  divider: {
    marginTop: theme.spacing(2),
  },
});

function Navigator(props) {
  const { classes, ...other } = props;
  console.log(props);
  return (
    <Drawer variant="permanent" {...other}>
      <List disablePadding>
        <ListItem className={clsx(classes.firebase, classes.item, classes.itemCategory)}>
          Bartosz Duda
        </ListItem>
        <ListItem component={RouterLink} to="/admin" className={clsx(
                  classes.item,
                  classes.itemActionable,
                  '/admin' === props.route.path && classes.itemActiveItem,
                )}>
          <ListItemIcon>
            <HomeIcon />
          </ListItemIcon>
          <ListItemText
            classes={{
              primary: classes.itemPrimary,
            }}
          >
            Dashboard
          </ListItemText>
        </ListItem>
        {categories.map(({ id, children }) => (
          <React.Fragment key={id}>
            <ListItem className={classes.categoryHeader}>
              <ListItemText
                classes={{
                  primary: classes.categoryHeaderPrimary,
                }}
              >
                {id}
              </ListItemText>
            </ListItem>
            {children.map(({ id: childId, icon, active, link }) => (
              <ListItem
                component={RouterLink}
                to={link}
                button
                dense
                key={childId}
                className={clsx(
                  classes.item,
                  classes.itemActionable,
                  link === props.route.path && classes.itemActiveItem,
                )}
              >
                <ListItemIcon>{icon}</ListItemIcon>
                <ListItemText
                  classes={{
                    primary: classes.itemPrimary,
                    textDense: classes.textDense,
                  }}
                >
                  {childId}
                </ListItemText>
              </ListItem>
            ))}
            <Divider className={classes.divider} />
          </React.Fragment>
        ))}
      </List>
    </Drawer>
  );
}

Navigator.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(Navigator);