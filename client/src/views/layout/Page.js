import React, {Suspense} from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Header from './Header';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import LinearProgress from '@material-ui/core/LinearProgress';
import Container from '@material-ui/core/Container';
import Footer from './Footer';
import _ from 'lodash';
const styles = theme => ({
  root: {
  },
  paper: {
    padding: theme.spacing(2),
    textAlign: 'center',
    color: theme.palette.text.secondary,
  },
});

function CenteredGrid(props) {
  const { classes } = props;
  return (
    <Container>
    <Grid container className={classes.root}>
      <Grid item xs={12}>
        <Header />     
      </Grid>
      <Grid item xs={3}>
        { props.regions.left? 
          _.map(props.regions.left, Widget => {
            return (
              <Suspense fallback={<LinearProgress color="secondary"/>}>
                <Widget route={props.match}/>
              </Suspense>
            )
          }) : null
        }
      </Grid>
      <Grid item xs={9}>
        <Suspense fallback={<LinearProgress color="secondary"/>}>
          <props.regions.content route={props.match}/>
        </Suspense>
      </Grid>
    </Grid>
    </Container>
  );
}

CenteredGrid.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(CenteredGrid);