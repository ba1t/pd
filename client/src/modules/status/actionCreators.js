import * as Types from "../status/actionTypes";
import axios from "axios";

export const statusTest = (test) => dispatch => {
    dispatch({type: Types.STATUS_TEST, payload: test})
};