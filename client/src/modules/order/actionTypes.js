export const FETCH_ORDER = 'fetch order'; 
export const GET_ORDER = 'get order';
export const ADD_ORDER = 'add order';
export const UPDATE_ORDER = 'update order';
export const DELETE_ORDER = 'destroy order';
export const CLEAR_ORDER = 'clear order';