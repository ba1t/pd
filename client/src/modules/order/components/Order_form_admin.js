import React from 'react';
import { connect } from 'react-redux';
import {withRouter} from 'react-router-dom';
import CreateForm from '../../../helpers/CreateForm';
import { actions } from '..';

class OrderFormAdmin extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            init: false,
            data: {},
        };
        this._handleSubmit = this._handleSubmit.bind(this);
    }
    _handleSubmit(data,event) {
        event.preventDefault();
        this.props.addOrder(data, () => (
            this.props.history.push("/admin/order")
        ));
    }
    componentDidMount(){
        this.props.fetchOrder()
        this.props.edit ? this.setState({
            init: true,
            data: {},
        }) : this.setState({
            init: true
        });
    }
    render(){
        return(
            this.state.init && this.props.order.list ?
            <CreateForm init={this.props.edit && this.state.data} 
            submit={this._handleSubmit}
            form={
                [
                    {
                        type: "group",
                        props: {
                            row: true
                        },
                        children: [
                            {
                                type: "checkbox",
                                value: "enable",
                                label: "Enable"
                            },
                            {
                                type: "text",
                                value: "weight",
                                label: "Weight"
                            }
                        ]
                    },
                    {
                        type: "group",
                        props: {
                            row: true
                        },
                        children: [
                            {
                                type: 'text',
                                label: 'Nazwa',
                                value: 'name'
                            },
                            {
                                type: "select",
                                value: "parentOrder",
                                label: "Parent Order",
                                options: this.props.order.list
                            }
                        ]
                    },
                    {
                        type: "group",
                        props: {
                            row: true
                        },
                        children: [
                            {
                                type: "text",
                                value: "path",
                                label: "Path",
                            }
                        ]
                    }
                ]
            } /> : "loading"
        )
    }

}

const mapStateToProps = ({order}) => (
    {order}
)

export default connect(mapStateToProps, {...actions})(withRouter(OrderFormAdmin));