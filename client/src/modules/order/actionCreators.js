import * as Types from "../order/actionTypes";
import axios from "axios";

export const fetchOrder = () => async dispatch => {
    const res = await axios.get('/api/order');
    dispatch({type: Types.FETCH_ORDER, payload: {list: res.data.doc, count: res.data.count}});
}
export const getOrder = (id) => async dispatch => {
    const res = await axios.get(`/api/order/id/${id}`);
    dispatch({type: Types.GET_ORDER, payload: res.data.doc})
}
export const addOrder = (data, cb) => async dispatch => {
    const res = await axios.post(`/api/order`, data);
    dispatch({type: Types.ADD_ORDER, payload: res.data.doc});
    cb();
}
export const updateOrder = (id,data) => async dispatch => {
    const res = await axios.put(`/api/order/id/${id}`, data);
    dispatch({type: Types.UPDATE_ORDER, payload: res})
}
export const deleteOrder = (id) => dispatch => {
    axios.delete(`/api/order/${id}`);
    dispatch({type: Types.DELETE_ORDER, payload: id})
}
export const clearOrder = () => dispatch => {
    dispatch({type: Types.CLEAR_ORDER, payload: {order: false, updateOrder: false, productDeleted: false}})
}