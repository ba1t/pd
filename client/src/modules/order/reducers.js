
import * as Types from "./actionTypes";

export default function(state = {}, action) {
    switch (action.type) {
        case Types.FETCH_ORDER:
            return { ...state, ...action.payload }
        case Types.GET_ORDER:
            return {...state, order: action.payload}
        case Types.ADD_ORDER:
            return {...state,newOrder: action.payload}
        case Types.UPDATE_ORDER:
            return {
                ...state,
                updateOrder: action.payload.success,
                order: action.payload.doc
            }
        case Types.DELETE_ORDER:
            return {
                ...state,
                orderDeleted: action.payload
            }
        case Types.CLEAR_ORDER:
            return {
                ...state,
                updateOrder: action.payload.updateOrder,
                order: action.payload.order,
                orderDeleted: action.payload.orderDeleted
            }
        default:
            return state;
    }
}