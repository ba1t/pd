import React, { Fragment } from 'react';
import CreateTable from '../../../helpers/CreateTable';
import { connect } from 'react-redux';
import {actions } from '../';

const columns = [
    { Header: 'id', accessor: '_id' },
    { Header: 'email', accessor: 'email' },
    { Header: 'Enabled', accessor: 'enable' }
]


class UserTableAdmin extends React.Component {
    componentDidMount() {
        this.props.fetchUser();
    }
    render() {
        return (
            <Fragment>

            { this.props.user.list ?
                <CreateTable
                    columns={columns} 
                    route={this.props.route} 
                    rows={this.props.user.list} 
                    edit 
                    delete={index => this.props.deleteUser(index)}  
                /> 
            : null }
            </Fragment>

        )
    }
}

const mapStateToProps =  ({user}) => (
    {user}
) 

export default connect(mapStateToProps, { ...actions })(UserTableAdmin);