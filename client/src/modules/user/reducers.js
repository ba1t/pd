
import * as Types from "./actionTypes";

export default function(state = {}, action) {
    switch (action.type) {
        case Types.FETCH_USER:
            return { ...state, ...action.payload }
        case Types.GET_USER:
            return {...state,user: action.payload}
        case Types.ADD_USER:
            return {...state,newUser: action.payload}
        case Types.UPDATE_USER:
            return {
                ...state,
                updateUser: action.payload.success,
                user: action.payload.doc
            }
        case Types.DELETE_USER:
            return {
                ...state,
                userDeleted: action.payload
            }
        case Types.CLEAR_USER:
            return {
                ...state,
                updateUser: action.payload.updateUser,
                user: action.payload.user,
                userDeleted: action.payload.userDeleted
            }
        default:
            return state;
    }
}