import * as Types from "../user/actionTypes";
import axios from "axios";

export const fetchUser = () => async dispatch => {
    const res = await axios.get('/api/user');
    dispatch({ type: Types.FETCH_USER, payload: {list: res.data.doc, count: res.data.count}})
}
export const getUser = (id) => async dispatch => {
    const res = await axios.get(`/api/user/${id}`);
    dispatch({ type: Types.GET_USER, payload: res.data.doc})
}
export const addUser = (data) => async dispatch => {
    const res = await axios.post(`/api/user`, data);
    dispatch({type: Types.ADD_USER, payload: res.data.doc })
}
export const updateUser = (id, data) => async dispatch => {
    const res = await axios.put(`/api/user/${id}`, data);
    dispatch({type: Types.UPDATE_USER, payload: res.data.doc })
}
export const deleteUser = (id) => dispatch => {
    axios.delete(`/api/user/${id}`);
    dispatch({type: Types.DELETE_USER, payload: id})
}
export const clearUser = () => dispatch => {
    dispatch({type: Types.CLEAR_USER, payload:{user:false, updateUser:false, userDeleted:false}})
}