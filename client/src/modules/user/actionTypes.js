export const FETCH_USER = 'fetch_user';
export const ADD_USER = 'add_user';
export const GET_USER = 'get_user';
export const UPDATE_USER = 'edit_user';
export const DELETE_USER = 'delete_user';
export const CLEAR_USER = 'clear_user';