
import * as Types from "./actionTypes";
import _ from 'lodash';

export default function(state = {}, action) {
    switch (action.type) {
        case Types.FETCH_CART:
            return { ...state, ...action.payload }
        case Types.GET_CART:
            return {...state, customer: action.payload}
        default:
            return state;
    }
}