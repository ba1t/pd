import React, { Component } from 'react';
import { connect } from 'react-redux';
import { actions } from '..'
import _ from 'lodash';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';
import { Link as RouterLink } from 'react-router-dom'
class Cart extends Component {
    constructor(props) {
        super(props);
        this.state = {
            open: true
        }
        this.handleClose = this.handleClose.bind(this);
        this.handleClick = this.handleClick.bind(this);
    }
    componentDidMount() {
        this.props.getUserCart();
    }
    handleClose() {
        this.setState({open: false});
    }
    handleClick() {
        this.setState({open: !this.state.open});
    }
    render() {
        return (
            this.props.cart.customer ? 
                    
                <Button  component={RouterLink} to={`/koszyk`} color="inherit">
                    Koszyk ({this.props.cart.customer.length})
                </Button>
            

            : null
        )
    }
}
const mapStateToProps = ({cart}) => (
    {cart}
);
export default connect(mapStateToProps, { ...actions })(Cart);