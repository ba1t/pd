import React, { Fragment } from 'react';
import CreateTable from '../../../helpers/CreateTable';
import { connect } from 'react-redux';
import {actions } from '..';

import Button from '@material-ui/core/Button';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';

import { Link as RouterLink } from 'react-router-dom'
import Link from '@material-ui/core/Link';

const columns = [
    { Header: 'id', accessor: '_id' },
    { Header: 'Name', accessor: 'name' },
    { Header: 'Enabled', accessor: 'enable' },
]


class OrderTableAdmin extends React.Component {
    componentDidMount() {
        this.props.fetchOrder();
    }
    render() {
        return (
            <Fragment>

            { this.props.order.list ?
                <CreateTable
                    columns={columns} 
                    route={this.props.route} 
                    rows={this.props.order.list} 
                    edit 
                    delete={this.props.deleteOrder}  
                /> 
            : null }
            </Fragment>

        )
    }
}

const mapStateToProps =  ({order}) => (
    {order}
) 

export default connect(mapStateToProps, { ...actions })(OrderTableAdmin);