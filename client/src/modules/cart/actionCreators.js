import * as Types from "./actionTypes";
import axios from "axios";


export const getUserCart= () => async dispatch => {
    const res = await axios.get('/api/currentCustomerCart');
    dispatch({type: Types.GET_CART, payload: res.data.doc.cart});
}