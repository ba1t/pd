import React, { Fragment } from 'react';
import CreateTable from '../../../helpers/CreateTable';
import { connect } from 'react-redux';
import {actions } from '..';

import Button from '@material-ui/core/Button';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';

import { Link as RouterLink } from 'react-router-dom'
import Link from '@material-ui/core/Link';

const columns = [
    { Header: 'Image', accessor: 'image.target.path' },
    { Header: 'Name', accessor: 'name' },
    { Header: 'Enabled', accessor: 'enable' },
    { Header: 'Parent', accessor: 'parentCategory.name' }
]


class CategoryTableAdmin extends React.Component {
    componentDidMount() {
        this.props.fetchCategory();
    }
    render() {
        return (
            <Fragment>

            { this.props.category.list ?
                <CreateTable
                    lp
                    columns={columns} 
                    route={this.props.route} 
                    rows={this.props.category.list} 
                    edit 
                    delete={this.props.deleteCategory}  
                /> 
            : null }
            </Fragment>

        )
    }
}

const mapStateToProps =  ({category}) => (
    {category}
) 

export default connect(mapStateToProps, { ...actions })(CategoryTableAdmin);