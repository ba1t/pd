import React, { Component } from 'react';
import { connect } from 'react-redux';
import { actions } from '..';
import _ from 'lodash';

import { withStyles } from '@material-ui/core/styles';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import Divider from '@material-ui/core/Divider';
import { Link as RouterLink } from 'react-router-dom'
import Typography from '@material-ui/core/Typography';

const styles = theme => ({
    root: {
        width: '100%',
        maxWidth: '360px',
        backgroundColor: theme.palette.background.paper,
      },
  });

class CategoryWidget extends Component {
    constructor(props) {
        super(props);
    }
    componentDidMount() {
        this.props.fetchCategory();
    }
    render() {
        const { classes } = this.props;
        return (
            this.props.category.list ?
            <React.Fragment>
                <Typography variant="h4" gutterBottom>
                    KATEGORIE
                </Typography>
                <List color="primary" light={true} component="nav" className={classes.root} aria-label="Mailbox folders">
                    {_.map(this.props.category.list, item => (
                        <React.Fragment>
                            <ListItem button>
                                <ListItemText primary={item.name} />
                            </ListItem>
                            <Divider />
                        </React.Fragment>
                    ))}
                </List>
            </React.Fragment>
            : null 
        )
    }
}
const mapStateToProps = ({category}) => (
    {category}
);
export default connect(mapStateToProps, { ...actions })(withStyles(styles)(CategoryWidget));