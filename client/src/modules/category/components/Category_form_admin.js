import React from 'react';
import { connect } from 'react-redux';
import {withRouter} from 'react-router-dom';
import CreateForm from '../../../helpers/CreateForm';
import { actions } from '../';

class CategoryFormAdmin extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            init: false,
            data: {},
        };
        this._handleSubmit = this._handleSubmit.bind(this);
    }
    _handleSubmit(data,event) {
        event.preventDefault();
        if(this.props.edit) {
            this.props.updateCategory(this.props.match.params.id, data, () => (
                this.props.history.push("/admin/category")
            ));
        } else {
            this.props.addCategory(data, () => (
                this.props.history.push("/admin/category")
            ));
        }
    }
    componentDidMount(){
        this.props.fetchCategory()
        if(this.props.edit && this.props.match.params.id !== undefined) {
            this.props.getCategory(this.props.match.params.id, (item) => (
                this.setState({
                    init: true,
                    data: item
                })
            ))
        } else {
            this.setState({
                init: true
            });
        }
    }
    render(){
        return(
            this.state.init && this.props.category.list ?
            <CreateForm init={this.props.edit && this.state.data} 
            submit={this._handleSubmit}
            form={
                [
                    {
                        type: "group",
                        props: {
                            row: true
                        },
                        children: [
                            {
                                type: "checkbox",
                                value: "enable",
                                label: "Enable"
                            },
                            {
                                type: "text",
                                value: "weight",
                                label: "Weight"
                            }
                        ]
                    },
                    {
                        type: "group",
                        props: {
                            row: true
                        },
                        children: [
                            {
                                type: 'text',
                                label: 'Nazwa',
                                value: 'name'
                            },
                            {
                                type: "select",
                                value: "parentCategory",
                                label: "Parent Category",
                                options: this.props.category.list
                            }
                        ]
                    },
                    {
                        type: "group",
                        props: {
                            row: true
                        },
                        children: [
                            {
                                type: "text",
                                value: "path",
                                label: "Path",
                            }
                        ]
                    },
                    {
                        type: "group",
                        props: {
                            row: true
                        },
                        children: [
                            {
                                type: 'file',
                                value: 'image',
                                label: 'product image'
                            }
                        ]
                    }
                ]
            } /> : "loading"
        )
    }

}

const mapStateToProps = ({category}) => (
    {category}
)

export default connect(mapStateToProps, {...actions})(withRouter(CategoryFormAdmin));