import * as Types from "../category/actionTypes";
import axios from "axios";

export const fetchCategory = () => async dispatch => {
    const res = await axios.get('/api/category');
    dispatch({type: Types.FETCH_CATEGORY, payload: {list: res.data.doc, count: res.data.count}});
}
export const getCategoryByPath = (path) => async dispatch => {
    const res = await axios.get(`/api/category/path/${path}`);
    dispatch({type: Types.GET_CATEGORY, payload: res.data.doc})
}
export const getCategory = (id, cb) => async dispatch => {
    const res = await axios.get(`/api/category/${id}`);
    dispatch({type: Types.GET_CATEGORY, payload: res.data.doc})
    cb(res.data.doc);
}
export const addCategory = (data, cb) => async dispatch => {
    const res = await axios.post(`/api/category`, data);
    dispatch({type: Types.ADD_CATEGORY, payload: res.data.doc});
    cb();
}
export const updateCategory = (id,data, cb) => async dispatch => {
    const res = await axios.put(`/api/category/${id}`, data);
    dispatch({type: Types.UPDATE_CATEGORY, payload: res})
    cb();
}
export const deleteCategory = (id) => dispatch => {
    axios.delete(`/api/category/${id}`);
    dispatch({type: Types.DELETE_CATEGORY, payload: id})
}
export const clearCategory = () => dispatch => {
    dispatch({type: Types.CLEAR_CATEGORY, payload: {category: false, updateCategory: false, productDeleted: false}})
}