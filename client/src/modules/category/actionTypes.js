export const FETCH_CATEGORY = 'get categories'; 
export const GET_CATEGORY = 'get categoriy';
export const ADD_CATEGORY = 'add category';
export const UPDATE_CATEGORY = 'update category';
export const DELETE_CATEGORY = 'destroy category';
export const CLEAR_CATEGORY = 'clear category';