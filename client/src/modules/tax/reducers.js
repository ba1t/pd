
import * as Types from "./actionTypes";
import _ from 'lodash';

export default function(state = {}, action) {
    switch (action.type) {
        case Types.FETCH_TAX:
            return { ...state, ...action.payload }
        case Types.GET_TAX:
            return {...state,tax:action.payload}
        case Types.ADD_TAX:
            return {...state,newTax:action.payload}
        case Types.UPDATE_TAX:
            return {
                ...state,
                updateTax:action.payload.success,
                tax:action.payload.doc
            }
        case Types.DELETE_TAX:
            const newState = [...state.list];
            const deleteItem = _.findIndex(newState, ['_id', action.payload]);
            newState.splice(deleteItem, 1);
            return {
                ...state,
                count: state.count - 1,
                list: newState,
                taxDeleted:action.payload
            }
        case Types.CLEAR_TAX:
            return {
                ...state,
                updateTax:action.payload.updateTax,
                tax:action.payload.tax,
                deleteTax:action.payload.deleteTax
            }
        default:
            return state;
    }
}