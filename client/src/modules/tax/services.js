import axios from 'axios';

export default {
    fetchTaxs() {
        return axios.get('/api/tax')
    },
    getTax(id) {
        return axios.get(`/api/tax/${id}`);
    },
    addTax(data) {
        return axios.post(`/api/tax`, data)
    },
    updateTax(id, data) {
        return axios.put(`/api/tax/${id}`, data);
    },
    deleteTax(id) {
        return axios.delete(`/api/tax/${id}`);
    }
}