import * as Types from "./actionTypes";
import { services } from "./"

export const fetchTax = () => async dispatch => {
    const res = await services.fetchTaxs();
    dispatch({type: Types.FETCH_TAX, payload: {list: res.data.doc, count: res.data.count }});
}
export const getTax = (id, cb) => async dispatch => {
    const res = await services.getTax(id);
    dispatch({type: Types.GET_TAX, payload: res.data.doc})
    cb(res.data.doc);
}
export const addTax = (data, cb) => async dispatch => {
    const res = await services.addTax(data);
    dispatch({type: Types.ADD_TAX, payload: res.data.doc});
    cb();
}
export const updateTax = (id,data, cb) => async dispatch => {
    const res = await services.updateTax(id, data);
    dispatch({type: Types.UPDATE_TAX, payload: res});
    cb()
}
export const deleteTax = (id) => dispatch => {
    services.deleteTax(id);
    dispatch({type: Types.DELETE_TAX, payload: id})
}
export const clearTax = () => dispatch => {
    dispatch({type: Types.CLEAR_TAX, payload: {tax: false, updateTax: false, deleteTax: false}})
}