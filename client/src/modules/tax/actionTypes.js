export const FETCH_TAX = 'Fetch Tax';
export const GET_TAX = 'get Tax';
export const ADD_TAX = 'add Tax';
export const UPDATE_TAX = 'update Tax';
export const DELETE_TAX = 'delete Tax';
export const CLEAR_TAX = 'clear Tax';