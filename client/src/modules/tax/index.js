import * as actions from "./actionCreators";
import reducer from "./reducers";


// // Store/state related stuff:
export default reducer;
export { actions };

export { default as services } from './services'; 


// // Components:
export { default as Tax } from './components/Tax';
export { default as TaxTableAdmin } from './components/Tax_table_admin';
export { default as TaxFormAdmin } from './components/Tax_form_admin';