export const DELIVERY_TEST = '@@bajrent/delivery/DELIVERY_TEST';

export const FETCH_DELIVERY = '@@bajrent/delivery/FETCH_DELIVERY';
export const GET_DELIVERY = '@@bajrent/delivery/GET_DELIVERY';
export const ADD_DELIVERY = '@@bajrent/delivery/ADD_DELIVERY';
export const UPDATE_DELIVERY = '@@bajrent/delivery/UPDATE_DELIVERY';
export const DELETE_DELIVERY = '@@bajrent/delivery/DELETE_DELIVERY';
export const CLEAR_DELIVERY = '@@bajrent/delivery/CLEAR_DELIVERY';