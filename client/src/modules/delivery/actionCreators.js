import * as Types from "./actionTypes";
import { services } from "."

export const fetchDelivery = () => async dispatch => {
    const res = await services.fetchDeliverys();
    dispatch({type: Types.FETCH_DELIVERY, payload: {list: res.data.doc, count: res.data.count }});
}
export const getDelivery = (id, cb) => async dispatch => {
    const res = await services.getDelivery(id);
    dispatch({type: Types.GET_DELIVERY, payload: res.data.doc})
    cb(res.data.doc);
}
export const addDelivery = (data, cb) => async dispatch => {
    const res = await services.addDelivery(data);
    dispatch({type: Types.ADD_DELIVERY, payload: res.data.doc});
    cb();
}
export const updateDelivery = (id,data, cb) => async dispatch => {
    const res = await services.updateDelivery(id, data);
    dispatch({type: Types.UPDATE_DELIVERY, payload: res});
    cb()
}
export const deleteDelivery = (id) => dispatch => {
    services.deleteDelivery(id);
    dispatch({type: Types.DELETE_DELIVERY, payload: id})
}
export const clearDelivery = () => dispatch => {
    dispatch({type: Types.CLEAR_DELIVERY, payload: {delivery: false, updateDelivery: false, deleteDelivery: false}})
}