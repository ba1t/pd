
import * as Types from "./actionTypes";
import _ from 'lodash';

export default function(state = {}, action) {
    switch (action.type) {
        case Types.FETCH_DELIVERY:
            return { ...state, ...action.payload }
        case Types.GET_DELIVERY:
            return {...state,delivery:action.payload}
        case Types.ADD_DELIVERY:
            return {...state,newDelivery:action.payload}
        case Types.UPDATE_DELIVERY:
            return {
                ...state,
                updateDelivery:action.payload.success,
                delivery:action.payload.doc
            }
        case Types.DELETE_DELIVERY:
            const newState = [...state.list];
            const deleteItem = _.findIndex(newState, ['_id', action.payload]);
            newState.splice(deleteItem, 1);
            return {
                ...state,
                count: state.count - 1,
                list: newState,
                deliveryDeleted:action.payload
            }
        case Types.CLEAR_DELIVERY:
            return {
                ...state,
                updateDelivery:action.payload.updateDelivery,
                delivery: action.payload.delivery,
                deleteDelivery:action.payload.deleteDelivery
            }
        default:
            return state;
    }
}