import axios from 'axios';

export default {
    fetchDeliverys() {
        return axios.get('/api/delivery')
    },
    getDelivery(id) {
        return axios.get(`/api/delivery/${id}`);
    },
    addDelivery(data) {
        return axios.post(`/api/delivery`, data)
    },
    updateDelivery(id, data) {
        return axios.put(`/api/delivery/${id}`, data);
    },
    deleteDelivery(id) {
        return axios.delete(`/api/delivery/${id}`);
    }
}