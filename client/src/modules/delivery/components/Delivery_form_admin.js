import React from 'react';
import { connect } from 'react-redux';
import {withRouter} from 'react-router-dom';
import CreateForm from '../../../helpers/CreateForm';
import { actions } from '..';

class DeliveryFormAdmin extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            init: false,
            data: {},
        };
        this._handleSubmit = this._handleSubmit.bind(this);
    }
    _handleSubmit(data,event) {
        event.preventDefault();
        if(this.props.edit) {
            this.props.updateDelivery(this.props.match.params.id, data, () => (
                this.props.history.push("/admin/delivery")
            ));
        } else {
            this.props.addDelivery(data, () => (
                this.props.history.push("/admin/delivery")
            ));
        }

    }
    componentDidMount(){
        if(this.props.edit && this.props.match.params.id !== undefined) {
            this.props.getDelivery(this.props.match.params.id, (item) => (
                this.setState({
                    init: true,
                    data: item
                })
            ))
        } else {
            this.setState({
                init: true
            });
        }
    }
    render(){
        return(
            this.state.init ?
            <CreateForm init={this.props.edit && this.state.data} 
            submit={this._handleSubmit}
            form={
                [
                    {
                        type: "group",
                        props: {
                            row: true
                        },
                        children: [
                            {
                                type: "text",
                                value: "name",
                                label: "Name",
                            }
                        ]
                    },
                    {
                        type: "group",
                        props: {
                            row: true
                        },
                        children: [
                            {
                                type: 'file',
                                value: 'image',
                                label: 'product image'
                            }
                        ]
                    }
                ]
            } /> : "loading"
        )
    }

}

const mapStateToProps = ({delivery}) => (
    {delivery}
)

export default connect(mapStateToProps, {...actions})(withRouter(DeliveryFormAdmin));