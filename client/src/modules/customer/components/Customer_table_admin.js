import React, { Fragment } from 'react';
import CreateTable from '../../../helpers/CreateTable';
import { connect } from 'react-redux';
import {actions } from '..';

const columns = [
    { Header: 'id', accessor: '_id' },
    { Header: 'email', accessor: 'email' },
    { Header: 'Enabled', accessor: 'enable' }
]


class CustomerTableAdmin extends React.Component {
    componentDidMount() {
        this.props.fetchCustomer();
    }
    render() {
        return (
            <Fragment>

            { this.props.customer.list ?
                <CreateTable
                    columns={columns} 
                    route={this.props.route} 
                    rows={this.props.customer.list} 
                    edit 
                    delete={index => this.props.deleteCustomer(index)}  
                /> 
            : null }
            </Fragment>

        )
    }
}

const mapStateToProps =  ({customer}) => (
    {customer}
) 

export default connect(mapStateToProps, { ...actions })(CustomerTableAdmin);