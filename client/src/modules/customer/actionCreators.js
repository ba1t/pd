import * as Types from "./actionTypes";
import axios from "axios";

export const fetchCustomer = () => async dispatch => {
    const res = await axios.get('/api/customer');
    dispatch({ type: Types.FETCH_CUSTOMER, payload: {list: res.data.doc, count: res.data.count}})
}
export const getCustomer = (id) => async dispatch => {
    const res = await axios.get(`/api/customer/${id}`);
    dispatch({ type: Types.GET_CUSTOMER, payload: res.data.doc})
}
export const addCustomer = (data) => async dispatch => {
    const res = await axios.post(`/api/customer`, data);
    dispatch({type: Types.ADD_CUSTOMER, payload: res.data.doc })
}
export const updateCustomer = (id, data) => async dispatch => {
    const res = await axios.put(`/api/customer/${id}`, data);
    dispatch({type: Types.UPDATE_CUSTOMER, payload: res.data.doc })
}
export const deleteCustomer = (id) => dispatch => {
    axios.delete(`/api/customer/${id}`);
    dispatch({type: Types.DELETE_CUSTOMER, payload: id})
}
export const clearCustomer = () => dispatch => {
    dispatch({type: Types.CLEAR_CUSTOMER, payload:{customer:false, updateCustomer:false, customerDeleted:false}})
}

