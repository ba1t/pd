
export const FETCH_CUSTOMER = 'fetch Customer';
export const ADD_CUSTOMER = 'add Customer';
export const GET_CUSTOMER = 'get Customer';
export const UPDATE_CUSTOMER = 'edit Customer';
export const DELETE_CUSTOMER = 'delete Customer';
export const CLEAR_CUSTOMER = 'clear Customer';

