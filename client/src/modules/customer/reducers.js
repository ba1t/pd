
import * as Types from "./actionTypes";
import _ from 'lodash';

export default function(state = {}, action) {
    switch (action.type) {
        case Types.FETCH_CUSTOMER:
            return { ...state, ...action.payload }
        case Types.GET_CUSTOMER:
            return {...state,customer: action.payload}
        case Types.ADD_CUSTOMER:
            return {...state,newCustomer: action.payload}
        case Types.UPDATE_CUSTOMER:
            return {
                ...state,
                updateCustomer: action.payload.success,
                customer: action.payload.doc
            }
        case Types.DELETE_CUSTOMER:
            const newState = [...state.list];
            const deleteItem = _.findIndex(newState, ['_id', action.payload]);
            newState.splice(deleteItem, 1);
            return {
                ...state,
                count: state.count - 1,
                list: newState,
                customerDeleted:action.payload
            }
        case Types.CLEAR_CUSTOMER:
            return {
                ...state,
                updateCustomer: action.payload.updateCustomer,
                customer: action.payload.customer,
                customerDeleted: action.payload.customerDeleted
            }
        default:
            return state;
    }
}
