import * as actions from "./actionCreators";
import reducer from "./reducers";

// // Store/state related stuff:
export default reducer;
export { actions };

// // Components:
export { default as Customer } from './components/Customer';
export { default as CustomerTableAdmin } from './components/Customer_table_admin';