import React, { Component } from 'react';
import { withRouter, Redirect } from 'react-router-dom';
import CreateForm from '../../../helpers/CreateForm';
import { connect } from 'react-redux';
import { actions } from '../';
import Modal from '@material-ui/core/Modal';
import { withStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import { Link as RouterLink } from 'react-router-dom'
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';

import {CartWidget }from '../../cart'
const styles = theme => ({
  paper: {
    position: 'absolute',
    width: 400,
    backgroundColor: theme.palette.background.paper,
    boxShadow: theme.shadows[5],
    padding: theme.spacing(4),
    outline: 'none',
  },
});

class AuthCustomer extends Component {
    constructor(props) {
        super(props);
        this._handleSubmit = this._handleSubmit.bind(this);
        this._handleClose = this._handleClose.bind(this);
        this._handleOpen = this._handleOpen.bind(this);
        this.state = {
          openModal: false
        }
    }
  
    _handleSubmit(data, event) {
        event.preventDefault();
        this.props.loginCustomer(data, () => this.props.history.push("/cart"));
    }
    _handleClose() {
      this.setState({openModal: false});
    }
    _handleOpen() {
      this.setState({openModal: true});
    }
    componentDidMount() {
      this.props.authCustomer()
    }
    _renderContent() {
      const {classes} = this.props;
      switch(this.props.auth.customer) {
        case null: 
          return null
        case false:
          return (
            <div>
              <div onClick={this._handleOpen}>Zaloguj się</div>
              <Modal
                aria-labelledby="simple-modal-title"
                aria-describedby="simple-modal-description"
                open={this.state.openModal}
                onClose={this._handleClose}
              >
                <Paper className={classes.paper}>
                <CreateForm
                  submit={this._handleSubmit}
                  form={
                      [
                          {
                              type: "group",
                              props: {
                                  row: true
                              },
                              children: [
                                  {
                                      type: "text",
                                      value: "email",
                                      label: "Email"
                                  },
                                  {
                                      type: "text",
                                      value: "password",
                                      label: "Password"
                                  }
                              ]
                          }
                      ]
                  }
                />
                </Paper>
              </Modal>
              
              </div>
          )
        default:
          return (
              <div><Typography style={{display: "inline-block"}} variant="h6" className={classes.title}>Witaj, {this.props.auth.customer.firstName}</Typography> <CartWidget /> <Button component={RouterLink} to={`/cart/logout`} color="inherit">logout</Button></div>
          )
      }
    }
    render() {
      return (
        this._renderContent()
      );
    }
  }

const mapStateToProsp = ({auth}) => (
    {auth}
  ) 

export default connect(mapStateToProsp, {...actions})(withRouter(withStyles(styles)(AuthCustomer)));