import React, { Component } from 'react';
import { withRouter, Redirect } from 'react-router-dom';
import CreateForm from '../../../helpers/CreateForm';
import { connect } from 'react-redux';
import { actions } from '../';

class AuthUser extends Component {
    constructor(props) {
        super(props);
        this._handleSubmit = this._handleSubmit.bind(this);
    }
  
    _handleSubmit(data, event) {
        event.preventDefault();
        this.props.loginUser(data, () => this.props.history.push("/admin"));
    }
    componentDidMount() {
      this.props.authUser()
    }
    _renderContent() {
      switch(this.props.auth.user) {
        case null: 
          return null
        case false:
          return (
            <CreateForm
                submit={this._handleSubmit}
                form={
                    [
                        {
                            type: "group",
                            props: {
                                row: true
                            },
                            children: [
                                {
                                    type: "text",
                                    value: "email",
                                    label: "Email"
                                },
                                {
                                    type: "text",
                                    value: "password",
                                    label: "Password"
                                }
                            ]
                        }
                    ]
                }
            />
          )
        default:
          return <Redirect to={{pathname: '/admin'}} />
      }
    }
    render() {
      return (
        this._renderContent()
      );
    }
  }

const mapStateToProsp = ({auth}) => (
    {auth}
  ) 

export default connect(mapStateToProsp, {...actions})(withRouter(AuthUser));