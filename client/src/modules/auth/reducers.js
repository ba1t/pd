
import * as Types from "./actionTypes";
import { combineReducers } from 'redux';
const user =  function(state = null, action) {
    switch (action.type) {
        case Types.AUTH_USER:
            return  action.payload || false ;
        default: 
            return state;
    }
}

const customer = function(state = null, action) {
    switch (action.type) {
        case Types.AUTH_CUSTOMER:
            return  action.payload || false ;
        default: 
            return state;
    }
}
export default combineReducers({
    user,
    customer
})