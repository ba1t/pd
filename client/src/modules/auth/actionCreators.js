import * as Types from "../auth/actionTypes";
import axios from "axios";

export const authUser = () => async dispatch => {
    try {
        const res = await axios.get('/api/auth');
        dispatch({ type: Types.AUTH_USER, payload: res.data})
    } catch(err) {
    }
}

export const loginUser = (data, cb) => async dispatch => {
    const res = await axios.post(`/api/login`, data);
    dispatch({type: Types.AUTH_USER, payload: res.data.doc});
    cb();
}

export const authCustomer = () => async dispatch => {
    try {
        const res = await axios.get('/cart/auth');
        dispatch({ type: Types.AUTH_CUSTOMER, payload: res.data})
    } catch(err) {
    }
}

export const loginCustomer = (data, cb) => async dispatch => {
    const res = await axios.post(`/cart/login`, data);
    dispatch({type: Types.AUTH_CUSTOMER, payload: res.data.doc});
    cb();
}