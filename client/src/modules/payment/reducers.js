
import * as Types from "./actionTypes";
import _ from 'lodash';

export default function(state = {}, action) {
    switch (action.type) {
        case Types.FETCH_PAYMENT:
            return { ...state, ...action.payload }
        case Types.GET_PAYMENT:
            return {...state,payment:action.payload}
        case Types.ADD_PAYMENT:
            return {...state,newPayment:action.payload}
        case Types.UPDATE_PAYMENT:
            return {
                ...state,
                updatePayment:action.payload.success,
                payment:action.payload.doc
            }
        case Types.DELETE_PAYMENT:
            const newState = [...state.list];
            const deleteItem = _.findIndex(newState, ['_id', action.payload]);
            newState.splice(deleteItem, 1);
            return {
                ...state,
                count: state.count - 1,
                list: newState,
                paymentDeleted:action.payload
            }
        case Types.CLEAR_PAYMENT:
            return {
                ...state,
                updatePayment:action.payload.updatePayment,
                payment:action.payload.payment,
                deletePayment:action.payload.deletePayment
            }
        default:
            return state;
    }
}