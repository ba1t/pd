import * as Types from "./actionTypes";
import { services } from "./"

export const fetchPayment = () => async dispatch => {
    const res = await services.fetchPayments();
    dispatch({type: Types.FETCH_PAYMENT, payload: {list: res.data.doc, count: res.data.count }});
}
export const getPayment = (id, cb) => async dispatch => {
    const res = await services.getPayment(id);
    dispatch({type: Types.GET_PAYMENT, payload: res.data.doc})
    cb(res.data.doc);
}
export const addPayment = (data, cb) => async dispatch => {
    const res = await services.addPayment(data);
    dispatch({type: Types.ADD_PAYMENT, payload: res.data.doc});
    cb();
}
export const updatePayment = (id,data, cb) => async dispatch => {
    const res = await services.updatePayment(id, data);
    dispatch({type: Types.UPDATE_PAYMENT, payload: res});
    cb()
}
export const deletePayment = (id) => dispatch => {
    services.deletePayment(id);
    dispatch({type: Types.DELETE_PAYMENT, payload: id})
}
export const clearPayment = () => dispatch => {
    dispatch({type: Types.CLEAR_PAYMENT, payload: {payment: false, updatePayment: false, deletePayment: false}})
}