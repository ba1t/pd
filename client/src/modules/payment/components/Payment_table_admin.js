import React, { Fragment } from 'react';
import CreateTable from '../../../helpers/CreateTable';
import { connect } from 'react-redux';
import {actions } from '..';

import Button from '@material-ui/core/Button';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';

import { Link as RouterLink } from 'react-router-dom'
import Link from '@material-ui/core/Link';

const columns = [
    { Header: 'Image', accessor: 'image.target.path' },
    { Header: 'Name', accessor: 'name' },
]


class PaymentTableAdmin extends React.Component {
    componentDidMount() {
        this.props.fetchPayment();
    }
    render() {
        return (
            <Fragment>

            { this.props.payment.list ?
                <CreateTable
                    lp
                    columns={columns} 
                    route={this.props.route} 
                    rows={this.props.payment.list} 
                    edit 
                    delete={this.props.deletePayment}  
                /> 
            : null }
            </Fragment>

        )
    }
}

const mapStateToProps =  ({payment}) => (
    {payment}
) 

export default connect(mapStateToProps, { ...actions })(PaymentTableAdmin);