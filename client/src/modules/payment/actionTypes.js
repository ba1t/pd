export const PAYMENT_TEST = '@@bajrent/payment/PAYMENT_TEST';

export const FETCH_PAYMENT = '@@bajrent/payment/FETCH_PAYMENT';
export const GET_PAYMENT = '@@bajrent/payment/GET_PAYMENT';
export const ADD_PAYMENT = '@@bajrent/payment/ADD_PAYMENT';
export const UPDATE_PAYMENT = '@@bajrent/payment/UPDATE_PAYMENT';
export const DELETE_PAYMENT = '@@bajrent/payment/DELETE_PAYMENT';
export const CLEAR_PAYMENT = '@@bajrent/payment/CLEAR_PAYMENT';