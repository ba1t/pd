import axios from 'axios';

export default {
    fetchPayments() {
        return axios.get('/api/payment')
    },
    getPayment(id) {
        return axios.get(`/api/payment/${id}`);
    },
    addPayment(data) {
        return axios.post(`/api/payment`, data)
    },
    updatePayment(id, data) {
        return axios.put(`/api/payment/${id}`, data);
    },
    deletePayment(id) {
        return axios.delete(`/api/payment/${id}`);
    }
}