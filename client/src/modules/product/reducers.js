
import * as Types from "./actionTypes";
import _ from 'lodash';
export default function(state = {}, action) {
    switch (action.type) {
        case Types.FETCH_PRODUCT:
            return { ...state, ...action.payload }
        case Types.GET_PRODUCT:
            return {...state, product:action.payload}
        case Types.ADD_PRODUCT:
            return {...state,newProduct:action.payload}
        case Types.UPDATE_PRODUCT:
            return {
                ...state,
                updateProduct:action.payload.success,
                product:action.payload.doc
            }
        case Types.DELETE_PRODUCT:
            const newState = [...state.list];
            const deleteItem = _.findIndex(newState, ['_id', action.payload]);
            newState.splice(deleteItem, 1);
            return {
                ...state,
                count: state.count - 1,
                list: newState,
                productDeleted:action.payload
            }
        case Types.CLEAR_PRODUCT:
            return {
                ...state,
                updateProduct:action.payload.updateProduct,
                product:action.payload.product,
                productDeleted:action.payload.productDeleted
            }
        default:
            return state;
    }
}