import axios from 'axios';
export default {
    fetchProducts() {
        return axios.get(`/api/product`);
    },
    getProduct(id) {
        return axios.get(`/api/product/${id}`);
    },
    getProductByPath(path) {
        return axios.get(`/api/product/path/${path}`);
    },
    addProduct(data) {
        return axios.post(`/api/product/`, data)
    },
    updateProduct(id, data) {
        return axios.put(`/api/product/${id}`, data);
    },
    deleteProduct(id) {
        return axios.delete(`/api/product/${id}`);
    }
}