export const PRODUCT_TEST = '@@bajrent/product/PRODUCT_TEST';
export const FETCH_PRODUCT = '@@bajrent/product/FETCH_PRODUCT';
export const GET_PRODUCT = '@@bajrent/product/GET_PRODUCT';
export const ADD_PRODUCT = '@@bajrent/product/ADD_PRODUCT';
export const UPDATE_PRODUCT = '@@bajrent/product/UPDATE_PRODUCT';
export const DELETE_PRODUCT = '@@bajrent/product/DELETE_PRODUCT';
export const CLEAR_PRODUCT = '@@bajrent/product/CLEAR_PRODUCT'