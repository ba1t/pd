import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';

export default function ImgMediaCard(props) {

  return (
    <Card style={{"box-shadow": "none"}}>
      <CardActionArea>
      {props.product.image ?
        <CardMedia
          style={
            {
             width:"auto",
             "max-width": "100%",
             height: "auto",
             "max-height": "280px"
            }
          }
          component="img"
          alt="Contemplative Reptile"


          image={"/" + props.product.image.target.path}
          title="Contemplative Reptile"
        /> : null }
        <CardContent>
          <Typography gutterBottom variant="h5" component="h2">
            {props.product.title}
          </Typography>
          <Typography variant="body2" color="textSecondary" component="p">
            {props.product.body.slice(0,100)}...
          </Typography>
        </CardContent>
      </CardActionArea>
      <CardActions>
        <Button               variant="contained"
              color="secondary" size="small" >
          Szczegóły
        </Button>
        <Button variant="contained" size="small" color="primary">
          Dodaj do koszyka
        </Button>
      </CardActions>
    </Card>
  );
}