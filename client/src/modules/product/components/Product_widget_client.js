import React, { Component } from 'react';
import { connect } from 'react-redux';
import { actions } from '..';
import _ from 'lodash';

import MenuList from '@material-ui/core/MenuList';
import MenuItem from '@material-ui/core/MenuItem';
import Paper from '@material-ui/core/Paper';
import { makeStyles } from '@material-ui/core/styles';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import Typography from '@material-ui/core/Typography';
import DraftsIcon from '@material-ui/icons/Drafts';
import SendIcon from '@material-ui/icons/Send';
import PriorityHighIcon from '@material-ui/icons/PriorityHigh';

import { Link as RouterLink } from 'react-router-dom'

class ProductRandomWidget extends Component {
    constructor(props) {
        super(props);
        this.state = {
            item: null
        }
    }
    componentDidMount() {
        this.props.fetchProduct();
        this.setState({item: })
    }
    render() {
        console.log(this.props);
        return (
            this.props.product.list ?
                
            : null 
        )
    }
}
const mapStateToProps = ({category}) => (
    {category}
);
export default connect(mapStateToProps, { ...actions })(ProductRandomWidget);