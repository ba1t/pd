import React, { Component } from 'react';
import { connect } from 'react-redux';
import { actions } from '..'
import _ from 'lodash'
import {ProductCartClient} from '..';

import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import LinearProgress from '@material-ui/core/LinearProgress';
import Container from '@material-ui/core/Container';

class ProductList extends Component {
    constructor(props) {
        super(props);
    }
    componentDidMount() {
        this.props.fetchProduct();
        console.log(this.props);
    }
    render() {
        return (
            this.props.product.list ? 
                <Grid spacing={2} container alignItems={'flex-end'}>
                    {_.map(this.props.product.list, product => (
                        <Grid item sm={4}>
                            <ProductCartClient product={product}/>
                        </Grid>
                    ))}
                </Grid>
            : null
        )
    }
}
const mapStateToProps = ({product}) => (
    {product}
);
export default connect(mapStateToProps, { ...actions })(ProductList);