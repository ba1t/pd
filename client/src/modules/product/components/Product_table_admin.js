import React, { Fragment } from 'react';
import CreateTable from '../../../helpers/CreateTable';
import { connect } from 'react-redux';
import {actions } from '../';

import Button from '@material-ui/core/Button';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';

import { Link as RouterLink } from 'react-router-dom'
import Link from '@material-ui/core/Link';

const columns = [
    { Header: 'Image', accessor: 'image.target.path' },
    { Header: 'Name', accessor: 'name' },
    { Header: 'Manufacturer', accessor: 'manufacturerId.name' },
    { Header: 'Category', accessor: 'categoryId.name' },
    { Header: 'Published', accessor: 'published' }
    
]


class ProductTableAdmin extends React.Component {
    componentDidMount() {
        this.props.fetchProduct();
    }
    render() {
        return (
            <Fragment>

            { this.props.product.list ?
                <CreateTable
                    lp
                    columns={columns} 
                    route={this.props.route} 
                    rows={this.props.product.list} 
                    edit 
                    delete={index => this.props.deleteProduct(index)}  
                /> 
            : null }
            </Fragment>

        )
    }
}

const mapStateToProps =  ({product}) => (
    {product}
) 

export default connect(mapStateToProps, { ...actions })(ProductTableAdmin);