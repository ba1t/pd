import React from 'react';
import { connect } from 'react-redux';
import {withRouter} from 'react-router-dom';
import CreateForm from '../../../helpers/CreateForm';
import { actions } from '../';
import { actions as categoryActions } from '../../category/'
import { actions as manufacturerActions } from '../../manufacturer/'

class ProductFormAdmin extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            init: false,
            data: {},
        };
        this._handleSubmit = this._handleSubmit.bind(this);
    }
    _handleSubmit(data,event) {
        event.preventDefault();
        if(this.props.edit) {
            this.props.updateProduct(this.props.match.params.id, data, () => (
                this.props.history.push("/admin/product")
            ));
        } else {
            this.props.addProduct(data, () => (
                this.props.history.push("/admin/product")
            ));
        }

    }
    componentDidMount(){
        console.log('mount', this.props.match.params.id)

        this.props.fetchCategory()
        this.props.fetchManufacturer()
        if(this.props.edit && this.props.match.params.id !== undefined) {
            this.props.getProduct(this.props.match.params.id, (item) => (
                this.setState({
                    init: true,
                    data: item
                })
            ))
        } else {
            this.setState({
                init: true
            });
        }
    }
    render(){
        return(
            this.state.init && this.props.category.list && this.props.manufacturer.list ?
            <React.Fragment>
            <CreateForm init={this.props.edit && this.state.data} 
            submit={this._handleSubmit}
            form={
                [
                    {
                        type: "group",
                        props: {
                            row: true
                        },
                        children: [
                            {
                                type: "checkbox",
                                value: "published",
                                label: "Published"
                            },
                            {
                                type: "text",
                                value: "stock",
                                label: "Na magazynie"
                            }
                        ]
                    },
                    {
                        type: "group",
                        props: {
                            row: true
                        },
                        children: [
                            {
                                type: 'text',
                                label: 'Nazwa',
                                value: 'name'
                            },
                            {
                                type: "select",
                                value: "categoryId",
                                label: "Główna kategori",
                                options: this.props.category.list
                            }
                        ]
                    },
                    {
                        type: "group",
                        props: {
                            row: true
                        },
                        children: [
                            {
                                type: "text",
                                value: "price",
                                label: "Price",
                            },
                            {
                                type: "select",
                                value: "manufacturerId",
                                label: "Producent",
                                options: this.props.manufacturer.list
                            },
                        ]
                    },
                    {
                        type: "group",
                        props: {
                            row: true
                        },
                        children: [
                            {
                                type: "text",
                                label: "Opis",
                                value: "body",
                                props: {
                                    multiline: true,
                                    rowsMax: "5"
                                }
                            },

                        ]
                    },
                    {
                        type: "group",
                        props: {
                            row: true
                        },
                        children: [
                            {
                                type: 'file',
                                value: 'image',
                                label: 'product image'
                            }
                        ]
                    }
                ]
                
            } />
            </React.Fragment> : "loading"
        )
    }

}

const mapStateToProps = ({product, category, manufacturer}) => (
    {product, category, manufacturer}
)

export default connect(mapStateToProps, {...actions,...manufacturerActions, ...categoryActions})(withRouter(ProductFormAdmin));