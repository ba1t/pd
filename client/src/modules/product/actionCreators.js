import * as Types from "../product/actionTypes";
import services from './services';

export const fetchProduct = (limit = 10, start = 0, order = 'asc', list = '') => async dispatch => {
    const res = await services.fetchProducts()
    dispatch({type: Types.FETCH_PRODUCT, payload: {list: list ? [...list,...res.data.doc] : res.data.doc , count: res.data.count }})
}
export const getProduct = (id, cb) => async dispatch => {
    const res = await services.getProduct(id);
    dispatch({type: Types.GET_PRODUCT, payload: res.data.doc}) 
    cb(res.data.doc);
}
export const getProductByPath = (path) => async dispatch => {
    const res = await services.getProductByPath(path);
    dispatch({type: Types.GET_PRODUCT, payload: res.data.doc})
}
export const addProduct = (data, cb) => async dispatch => {
    const res = await services.addProduct(data);
    dispatch({type: Types.ADD_PRODUCT, payload: res.data.doc })
    cb();
}
export const updateProduct = (id, data, cb) => async dispatch => {
    const res = await services.updateProduct(id, data);
    dispatch({type: Types.UPDATE_PRODUCT, payload: res.data.doc });
    cb();
}
export const deleteProduct = (id) => dispatch => {
    services.deleteProduct(id)
    dispatch({type: Types.DELETE_PRODUCT, payload: id})
}
export const clearProduct = () => dispatch => {
    dispatch({type: Types.CLEAR_PRODUCT, payload:{product:false, updateProduct:false, productDeleted:false}})
}