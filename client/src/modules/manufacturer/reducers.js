
import * as Types from "./actionTypes";
import _ from 'lodash';
export default function(state = {}, action) {
    switch (action.type) {
        case Types.FETCH_MANUFACTURER:
            return { ...state, ...action.payload }
        case Types.GET_MANUFACTURER:
            return {...state,manufacturer:action.payload}
        case Types.ADD_MANUFACTURER:
            return {...state,newManufacturer:action.payload}
        case Types.UPDATE_MANUFACTURER:
            return {
                ...state,
                updateManufacturer:action.payload.success,
                manufacturer:action.payload.doc
            }
        case Types.DELETE_MANUFACTURER:
            const newState = [...state.list];
            const deleteItem = _.findIndex(newState, ['_id', action.payload]);
            newState.splice(deleteItem, 1);
            return {
                ...state,
                count: state.count - 1,
                list: newState,
                manufacturerDeleted:action.payload
            }
        case Types.CLEAR_MANUFACTURER:
            return {
                ...state,
                updateManufacturer:action.payload.updateManufacturer,
                manufacturer:action.payload.manufacturer,
                manufacturerDeleted:action.payload.manufacturerDeleted
            }
        default:
            return state;
    }
}