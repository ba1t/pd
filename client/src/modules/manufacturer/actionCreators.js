import * as Types from "./actionTypes";
import { services } from "./"

export const fetchManufacturer = () => async dispatch => {
    const res = await services.fetchManufacturers();
    dispatch({type: Types.FETCH_MANUFACTURER, payload: {list: res.data.doc, count: res.data.count }});
}
export const getManufacturer = (id, cb) => async dispatch => {
    const res = await services.getManufacturer(id);
    dispatch({type: Types.GET_MANUFACTURER, payload: res.data.doc})
    cb(res.data.doc);
}
export const addManufacturer = (data, cb) => async dispatch => {
    const res = await services.addManufacturer(data);
    dispatch({type: Types.ADD_MANUFACTURER, payload: res.data.doc});
    cb();
}
export const updateManufacturer = (id,data, cb) => async dispatch => {
    const res = await services.updateManufacturer(id, data);
    dispatch({type: Types.UPDATE_MANUFACTURER, payload: res});
    cb()
}
export const deleteManufacturer = (id) => dispatch => {
    services.deleteManufacturer(id);
    dispatch({type: Types.DELETE_MANUFACTURER, payload: id})
}
export const clearManufacturer = () => dispatch => {
    dispatch({type: Types.CLEAR_MANUFACTURER, payload: {manufacturer: false, updateManufacturer: false, productManufacturer: false}})
}