import * as actions from "./actionCreators";
import reducer from "./reducers";

// // Store/state related stuff:
export default reducer;
export { actions };

export { default as services } from './services'; 

// // Components:
export { default as Manufacturer } from './components/Manufacturer';
export { default as ManufacturerFormAdmin } from './components/Manufacturer_form_admin';
export { default as ManufacturerTableAdmin } from './components/Manufacturer_table_admin';