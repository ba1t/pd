import axios from 'axios';

export default {
    fetchManufacturers() {
        return axios.get('/api/manufacturer')
    },
    getManufacturer(id) {
        return axios.get(`/api/manufacturer/${id}`);
    },
    addManufacturer(data) {
        return axios.post(`/api/manufacturer`, data)
    },
    updateManufacturer(id, data) {
        return axios.put(`/api/manufacturer/${id}`, data);
    },
    deleteManufacturer(id) {
        return axios.delete(`/api/manufacturer/${id}`);
    }
}