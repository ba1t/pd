export const FETCH_MANUFACTURER = 'Fetch Manufacturer';
export const GET_MANUFACTURER = 'get Manufacturer';
export const ADD_MANUFACTURER = 'add Manufacturer';
export const UPDATE_MANUFACTURER = 'update Manufacturer';
export const DELETE_MANUFACTURER = 'delete manufacturer';
export const CLEAR_MANUFACTURER = 'clear manufacturer';