import Page from './views/layout/Page'
import Page_admin from './views/admin/layout/Page'
import React from 'react'
import Table from './helpers/CreateTable'
const Front  = () => (
    <div>xxx</div>
)
const Xxx = () => (
    <div>sss</div>
)
export default [
    {
        path: '/admin/product',
        layout: Page_admin,
        exact: true,
        regions: {
            header: {
                title: "Produkt: List",
            },
            content: React.lazy(()=> import('./views/admin/page/product/Product_list'))
        },
        private: true
    },
    {
        path: '/admin/product/add',
        layout: Page_admin,
        exact: true,
        regions: {
            header: {
                title: "Produkt: Add"
            },
            content: React.lazy(()=> import('./views/admin/page/product/Product_add'))
        },
        private: true
    },
    {
        path: '/admin/product/edit/:id',
        layout: Page_admin,
        exact: true,
        props: {
            edit: true
        },
        regions: {
            header: {
                title: "Produkt: Edit"
            },
            content: React.lazy(()=> import('./views/admin/page/product/Product_edit'))
        },
        private: true
    },
    {
        path: '/admin/category',
        layout: Page_admin,
        exact: true,
        regions: {
            header: {
                title: "Category: List",
            },
            content: React.lazy(()=> import('./views/admin/page/category/Category_list'))
        },
        private: true
    },
    {
        path: '/admin/category/add',
        layout: Page_admin,
        exact: true,
        regions: {
            header: {
                title: "Category: Add"
            },
            content: React.lazy(()=> import('./views/admin/page/category/Category_add'))
        },
        private: true
    },
    {
        path: '/admin/category/edit/:id',
        layout: Page_admin,
        exact: true,
        regions: {
            header: {
                title: "Category: Edit"
            },
            content: React.lazy(()=> import('./views/admin/page/category/Category_edit'))
        },
        private: true
    },
    {
        path: '/admin/manufacturer',
        layout: Page_admin,
        exact: true,
        regions: {
            header: {
                title: "Manufacturer: List",
            },
            content: React.lazy(()=> import('./views/admin/page/manufacturer/Manufacturer_list'))
        },
        private: true
    },
    {
        path: '/admin/manufacturer/add',
        layout: Page_admin,
        exact: true,
        regions: {
            header: {
                title: "Manufacturer: Add"
            },
            content: React.lazy(()=> import('./views/admin/page/manufacturer/Manufacturer_add'))
        },
        private: true
    },
    {
        path: '/admin/manufacturer/edit/:id',
        layout: Page_admin,
        exact: true,
        regions: {
            header: {
                title: "Manufacturer: Edit"
            },
            content: React.lazy(()=> import('./views/admin/page/manufacturer/Manufacturer_edit'))
        },
        private: true
    },
    {
        path: '/admin/basket',
        layout: Page_admin,
        exact: true,
        regions: {
            header: {
                title: "Basket: List",
            },
            content: React.lazy(()=> import('./views/admin/page/basket/Basket_list'))
        },
        private: true
    },
    {
        path: '/admin/basket/add',
        layout: Page_admin,
        exact: true,
        regions: {
            header: {
                title: "Basket: Add"
            },
            content: React.lazy(()=> import('./views/admin/page/basket/Basket_add'))
        },
        private: true
    },
    {
        path: '/admin/basket/edit/:id',
        layout: Page_admin,
        exact: true,
        regions: {
            header: {
                title: "Basket: Edit"
            },
            content: React.lazy(()=> import('./views/admin/page/basket/Basket_edit'))
        },
        private: true
    },
    {
        path: '/admin/order',
        layout: Page_admin,
        exact: true,
        regions: {
            header: {
                title: "Order: List",
            },
            content: React.lazy(()=> import('./views/admin/page/order/Order_list'))
        },
        private: true
    },
    {
        path: '/admin/order/add',
        layout: Page_admin,
        exact: true,
        regions: {
            header: {
                title: "Order: Add"
            },
            content: React.lazy(()=> import('./views/admin/page/order/Order_add'))
        },
        private: true
    },
    {
        path: '/admin/order/edit/:id',
        layout: Page_admin,
        exact: true,
        regions: {
            header: {
                title: "Order: Edit"
            },
            content: React.lazy(()=> import('./views/admin/page/order/Order_edit'))
        },
        private: true
    },
    {
        path: '/admin/customer',
        layout: Page_admin,
        exact: true,
        regions: {
            header: {
                title: "Customer: List",
            },
            content: React.lazy(()=> import('./views/admin/page/customer/Customer_list'))
        },
        private: true
    },
    {
        path: '/admin/customer/add',
        layout: Page_admin,
        exact: true,
        regions: {
            header: {
                title: "Customer: Add"
            },
            content: React.lazy(()=> import('./views/admin/page/customer/Customer_add'))
        },
        private: true
    },
    {
        path: '/admin/customer/edit/:id',
        layout: Page_admin,
        exact: true,
        regions: {
            header: {
                title: "Customer: Edit"
            },
            content: React.lazy(()=> import('./views/admin/page/customer/Customer_edit'))
        },
        private: true
    },
    {
        path: '/admin/tax',
        layout: Page_admin,
        exact: true,
        regions: {
            header: {
                title: "Tax: List",
            },
            content: React.lazy(()=> import('./views/admin/page/tax/Tax_list'))
        },
        private: true
    },
    {
        path: '/admin/tax/add',
        layout: Page_admin,
        exact: true,
        regions: {
            header: {
                title: "Tax: Add"
            },
            content: React.lazy(()=> import('./views/admin/page/tax/Tax_add'))
        },
        private: true
    },
    {
        path: '/admin/tax/edit/:id',
        layout: Page_admin,
        exact: true,
        regions: {
            header: {
                title: "Tax: Edit"
            },
            content: React.lazy(()=> import('./views/admin/page/tax/Tax_edit'))
        },
        private: true
    },
    {
        path: '/admin/payment',
        layout: Page_admin,
        exact: true,
        regions: {
            header: {
                title: "Payment: List",
            },
            content: React.lazy(()=> import('./views/admin/page/payment/Payment_list'))
        },
        private: true
    },
    {
        path: '/admin/payment/add',
        layout: Page_admin,
        exact: true,
        regions: {
            header: {
                title: "Payment: Add"
            },
            content: React.lazy(()=> import('./views/admin/page/payment/Payment_add'))
        },
        private: true
    },
    {
        path: '/admin/payment/edit/:id',
        layout: Page_admin,
        exact: true,
        regions: {
            header: {
                title: "Payment: Edit"
            },
            content: React.lazy(()=> import('./views/admin/page/payment/Payment_edit'))
        },
        private: true
    },
    {
        path: '/admin/delivery',
        layout: Page_admin,
        exact: true,
        regions: {
            header: {
                title: "Delivery: List",
            },
            content: React.lazy(()=> import('./views/admin/page/delivery/Delivery_list'))
        },
        private: true
    },
    {
        path: '/admin/delivery/add',
        layout: Page_admin,
        exact: true,
        regions: {
            header: {
                title: "Delivery: Add"
            },
            content: React.lazy(()=> import('./views/admin/page/delivery/Delivery_add'))
        },
        private: true
    },
    {
        path: '/admin/delivery/edit/:id',
        layout: Page_admin,
        exact: true,
        regions: {
            header: {
                title: "Delivery: Edit"
            },
            content: React.lazy(()=> import('./views/admin/page/delivery/Delivery_edit'))
        },
        private: true
    },
    {
        path: '/admin/user',
        layout: Page_admin,
        exact: true,
        regions: {
            header: {
                title: "User: List",
            },
            content: React.lazy(()=> import('./views/admin/page/user/User_list'))
        },
        private: true
    },
    {
        path: '/admin/user/add',
        layout: Page_admin,
        exact: true,
        regions: {
            header: {
                title: "User: Add"
            },
            content: React.lazy(()=> import('./views/admin/page/user/User_add'))
        },
        private: true
    },
    {
        path: '/admin/user/edit/:id',
        layout: Page_admin,
        exact: true,
        regions: {
            header: {
                title: "User: Edit"
            },
            content: React.lazy(()=> import('./views/admin/page/user/User_edit'))
        },
        private: true
    },
    {
        path: '/admin',
        layout: Page_admin,
        exact: true,
        regions: {
            header: {
                title: "Dashboard"
            },
            content: Front
        },
        private: true
    },
    {
        path: '/login',
        layout: Page,
        exact: true,
        regions: {
            content: React.lazy(()=> import('./views/admin/page/Login'))
        },
        private: false
    },
    {
        path: '/',
        layout: Page,
        exact: true,
        regions: {
            content: React.lazy(()=> import('./views/page/product/Product_list')),
            left: [
                React.lazy(()=> import('./modules/category/components/CategoryWidget'))
            ],
        },
        private: false
    },
    {
        path: '/category',
        layout: Page,
        exact: true,
        regions: {
            content: React.lazy(()=> import('./views/page/product/Product_list')),
            left: [
                React.lazy(()=> import('./modules/category/components/CategoryWidget'))
            ],
        },
        private: false
    },
    {
        path: '/category/:id',
        layout: Page,
        exact: true,
        regions: {
            content: React.lazy(()=> import('./views/page/product/Product_list')),
            left: [
                React.lazy(()=> import('./modules/category/components/CategoryWidget'))
            ],
        },
        private: false
    }
];