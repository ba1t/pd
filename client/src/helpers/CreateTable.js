import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Toolbar from '@material-ui/core/Toolbar';
import IconButton from '@material-ui/core/IconButton';

import { Link as RouterLink } from 'react-router-dom'
import Link from '@material-ui/core/Link';
import _ from 'lodash'
import { Edit, DeleteForever, CheckBox, CheckBoxOutlineBlank, Search } from '@material-ui/icons';

const styles = theme => ({
  root: {
    width: '100%',
    marginTop: theme.spacing(2),
    marginBottom: theme.spacing(4),
    overflowX: 'auto',
    borderRadius: 0
  },
  tbc: {
    paddingTop: 0,
    paddingBottom: 0,
    height: 68
  }, 
  table: {
    minWidth: 650,
  },
  topBar: {
    padding: theme.spacing(0),
  },
});

var deep_value = function(obj, path){
  for (var i=0, path=path.split('.'), len=path.length; i<len; i++){
      if(!_.isNil(obj[path[i]])) {
        obj = obj[path[i]];
      } else {
        obj = '';
        break;
      }
  };
    return obj;
};

function renderElement(element) {
  switch(true) {
    case typeof element === 'boolean':
      if(element) {
        return <CheckBox />
      } else {
        return <CheckBoxOutlineBlank />
      }
    case isNaN(element) && element.indexOf('IMAGE') !== -1:
      return <img style={{maxHeight: 68}} src={`/${element}`} />
    default: 
      return element
  }
}

class  CreateTable extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      filter: null
    };
    this._filterItemsByName = this._filterItemsByName.bind(this);
  }
  _filterItemsByName(store) {
    if(this.state.filter !== null) {
      return store.filter((item) => {
        return item.name.toLowerCase().search(
          this.state.filter.toLowerCase()) !== -1;
      });
    } else {
      return store;
    }
  };
  _chandeChange(e) {
    let val = e.target.value;
    if(val.length) {
      this.setState({filter: val})
    } else {
      this.setState({filter: null})
    }
  }
  render() {
    const { classes } = this.props;
    console.log(this.state);
    return (
      <Fragment>
      <Toolbar className={classes.topBar}>
          <TextField
          style={{ marginRight: 25 }}
          placeholder="Szukaj ..."
          fullWidth
          onChange={(e)=>this.setState({filter: e.target.value})}
          margin="normal"
          InputLabelProps={{
            shrink: true,
          }}
        />
        <Button component={RouterLink} to={`${this.props.route.path}/add`} variant="contained" color={"secondary"}>Dodaj</Button>
      </Toolbar>
      <Paper className={classes.root}>
        <Table className={classes.table}>
          <TableHead>
            <TableRow>
              {this.props.lp && <TableCell> lp. </TableCell>}
              {this.props.columns.map(({Header}) => ( <TableCell key={Header}> { Header } </TableCell> ))} 
                {this.props.edit || this.props.delete ? <TableCell>Action</TableCell> : null}
            </TableRow>
          </TableHead>
          <TableBody>
            {this._filterItemsByName(this.props.rows).map((row, index) => (
                <TableRow key={row._id + "tr"}> 
                    {this.props.lp && <TableCell className={classes.tbc}> {index + 1} </TableCell>}
                    {this.props.columns.map(({accessor}, index) => {
                      const acc = deep_value(row,accessor);
                        return (
                          <TableCell className={classes.tbc} key={row._id + "td"}>
                              {renderElement(acc)}
                          </TableCell>)
                    })}
                    {!!this.props.edit || !!this.props.delete ? 
                        <TableCell>
                            { !!this.props.edit ? <Link color="inherit" component={RouterLink} to={`${this.props.route.path}/edit/${row._id}`}><Edit /></Link> : null}
                            { !!this.props.delete ? <div onClick={() => (this.props.delete(row._id)) }><DeleteForever /></div>: null}
                        </TableCell>
                    : null} 
                </TableRow>
            ))}
          </TableBody>
        </Table>
      </Paper>
      </Fragment>
    );
  }
}

CreateTable.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(CreateTable);