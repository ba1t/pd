// import 'date-fns';
import React, { Fragment } from 'react';
import TextField from '@material-ui/core/TextField';
import { withStyles } from '@material-ui/core/styles';
import Input from '@material-ui/core/Input';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormHelperText from '@material-ui/core/FormHelperText';
import FormControl from '@material-ui/core/FormControl';
import FormGroup from '@material-ui/core/FormGroup';
import Select from '@material-ui/core/Select';
import Switch from '@material-ui/core/Switch';
import DateFnsUtils from '@date-io/date-fns';
import Button from '@material-ui/core/Button';
import { MuiPickersUtilsProvider, DatePicker } from 'material-ui-pickers';

import _ from 'lodash';
import axios from 'axios'

const styles = theme => ({
    root: {
      display: 'flex',
      flexWrap: 'wrap',
    },
    formControl: {
      margin: theme.spacing(1),
      minWidth: 100,
      flex: 1
    },
    group: {
        alignItems: "flex-end"
    },
    select: {
        width: '100%'
    },
    selectEmpty: {
      marginTop: theme.spacing(2),
    },
  });


class CreateForm extends React.Component {
    constructor(props) {
        super(props);
        this._handleChange = this._handleChange.bind(this);
        this.onFormFileSubmit = this.onFormFileSubmit.bind(this);
        this.state = {

        };
    }
    onFormFileSubmit = name => e =>{
        e.preventDefault();
        const formData = new FormData();
        formData.append('image',this.state[name]);
        const config = {
            headers: {
                'content-type': 'multipart/form-data'
            }
        };
        var _this = this;
        axios.post("/api/upload",formData,config)
            .then((response) => {
                console.log( 'respons', response.data.file);
                const {path, filename, mimetype} = response.data.file;
                axios.post('/api/image', {path: path, name: filename, extension: mimetype})
                    .then((image)=>(
                        _this.setState({[name]: { target: image.data.doc._id}})
                    ))
            }).catch((error) => {
        });
    }
    componentDidMount() {
        this.setState({...this.props.init})
    }
    _handleFileChange = name => event => {
        this.setState({ [name]: event.target.files[0] });
    };
    _handleChange = name => event => {
        this.setState({ [name]: event.target.value });
    };
    _handleCheck = name => event => {
        this.setState({ ...this.state, [name]: event.target.checked });
    };
    _handleDateChange = name => date => (
        this.setState({ ...this.state, [name]: date })
    );
    handleChangeMultiple = name => event => {
        const { options } = event.target;
        const value = [];
        for (let i = 0, l = options.length; i < l; i += 1) {
          if (options[i].selected) {
            value.push(options[i].value);
          }
        }
        this.setState({ ...this.state, [name]: value })
      }
    _renderElement(element) {
        let props = element.props || {}; 
        const { classes } = this.props
        switch(element.type) {
            case 'checkbox': 
                return (
                    <FormControlLabel className={classes.formControl}
                    control={
                      <Switch checked={this.state[element.value]} onChange={this._handleCheck(element.value)} value={element.value} />
                    }
                    label={element.label}
                  />
                )
            case 'file':
                return (
                    <div>
                        <Input type="file" name={element.value} onChange= {this._handleFileChange(element.value)} />
                        <Button  onClick={this.onFormFileSubmit(element.value)} variant="contained" color="secondary" >
                            upload
                        </Button>
                        </div>
                )
            case 'text':
                return (
                    <TextField className={classes.formControl}
                        id={element.value}
                        label={element.label}
                        value={this.state[element.value]}
                        onChange={this._handleChange(element.value)}
                        {...props}
                        margin="normal"
                        fullWidth={true}
                    />
                )
            case 'multipleSelect':
                    return(
                        <FormControl className={classes.formControl}>
                            <FormHelperText>{element.label}</FormHelperText>
                            <Select
                            multiple
                            className={classes.select}
                            id={element.value}
                            value={this.state[element.value]}
                            onChange={this.handleChangeMultiple(element.value)}
                            input={<Input id={element.label} />}
                            >
                                {element.options.map(({name, _id}) => (
                                    <MenuItem key={name+_id} value={_id}>{name}</MenuItem>
                                ))}
                            </Select>
                      </FormControl>
                    )
            case 'select':
                return (
                    <FormControl className={classes.formControl}>
                        <FormHelperText>{element.label}</FormHelperText>
                        <Select
                            className={classes.select}
                            id={element.value}
                            value={this.state[element.value]}
                            onChange={this._handleChange(element.value)}
                        >
                            {element.options.map(({name, _id}) => (
                                <MenuItem key={name+_id} value={_id}>{name}</MenuItem>
                            ))}
                        </Select>
                    </FormControl>        
                )
            case 'date':
                return (
                    <MuiPickersUtilsProvider  utils={DateFnsUtils}>    
                        <DatePicker
                            className={classes.formControl}
                            margin="normal"
                            label={element.label}
                            value={this.state[element.value]}
                            onChange={this._handleDateChange(element.value)}
                        />
                    </MuiPickersUtilsProvider>
                )
            case 'group':
                return (
                    <FormGroup {...props} className={classes.group}>
                        {element.children.map(child => (
                            this._renderElement(child)
                        ))}
                    </FormGroup>
                )
        }
    }
    render(){
        console.log(this.state);
        return(
            <div>
                {this.props.form.map(formElement => (
                    <React.Fragment key={formElement.value}>{this._renderElement(formElement)}</React.Fragment>
                ))}
                <Button onClick={(e) => (this.props.submit(this.state,e))} variant="contained" color="secondary" >
                    Zapisz
                </Button>
            </div>
        )
    }

}

export default withStyles(styles)(CreateForm);