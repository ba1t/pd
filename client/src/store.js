import { combineReducers } from 'redux';
import auth from './modules/auth';
import cart from './modules/cart';
import category from './modules/category';
import customer from './modules/customer';
import delivery from './modules/delivery';
import manufacturer from './modules/manufacturer';
import order from './modules/order';
import product from './modules/product';
import user from './modules/user';
import tax from './modules/tax';
import payment from './modules/payment';
const rootReducer = combineReducers({
    auth,
    cart,
    category,
    customer,
    delivery,
    manufacturer,
    order,
    product,
    user,
    tax,
    payment
});

export default rootReducer;