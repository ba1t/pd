import React from 'react';
import ReactDOM from 'react-dom';

import { Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';
import reduxThunk from 'redux-thunk';
import Routes from './Routes';
import rootReducer from './store';

import { composeWithDevTools } from 'redux-devtools-extension';

import * as serviceWorker from './helpers/serviceWorker';

const store = createStore(
    rootReducer, 
    composeWithDevTools(
        applyMiddleware(reduxThunk),
    )
  );

const App = () => (
    <Provider store={store}>
        <Routes />
    </Provider>
)


ReactDOM.render(<App />, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
