import React, { Component } from 'react';
import { BrowserRouter, Switch,  Route, Redirect } from 'react-router-dom';
import { connect } from 'react-redux';
import createPage from './helpers/createPage'
import config from './config';
import { actions } from './modules/auth'
class Routes extends Component {
    componentDidMount() {
        this.props.authUser();
    }
    _checkAndRedirect(route, routeProps) {
        if(!route.private) {
            return createPage(route.layout)({...routeProps, ...{regions: route.regions}})
        } else {
            if (this.props.auth.user) {
                return createPage(route.layout)({...routeProps, ...{regions: route.regions}}) 
            } else {
                return <Redirect to="/login" />
            }
        }
    }
    
    render() {
        return (    
            this.props.auth.user !== null ?
                <BrowserRouter>
                    <Switch>
                        {config.map((route) => (
                            <Route 
                                exact={route.exact} 
                                key={route.path + "route"} 
                                path={route.path} 
                                render={ (routeProps) =>  this._checkAndRedirect(route, routeProps) } 
                            />
                        ))}
                        <Redirect to="/" />
                    </Switch>
                </BrowserRouter> 
            : null
        )
    }
}

const mapStateToProps = ({auth}) => (
    {auth}
)

export default connect(mapStateToProps, {...actions})(Routes);

