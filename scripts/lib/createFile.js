var exports = module.exports = {};
function jsUcfirst(string)
{
    return string.charAt(0).toUpperCase() + string.slice(1);
}
exports.actionCreators = (name) => (
`import * as Types from "../${name}/actionTypes";
import axios from "axios";

export const ${name}Test = (test) => dispatch => {
    dispatch({type: Types.${name.toUpperCase()}_TEST, payload: test})
};`
);

exports.actionTypes = (name) => (
`export const ${name.toUpperCase()}_TEST = '@@artibo/${name}/${name.toUpperCase()}_TEST';`
);

exports.reducers = (name) => (
`
import * as Types from "./actionTypes";

export default (state = {}, action) => {
    switch (action.type) {
        case Types.${name.toUpperCase()}_TEST:
            return state;
        default : {
            return state;
        }
    }
};`
);

exports.index = (name) => (
`import * as actions from "./actionCreators";
import reducer from "./reducers";

// // Store/state related stuff:
export default reducer;
export { actions };

// // Components:
export { default as ${jsUcfirst(name)} } from './components/${jsUcfirst(name)}';`
);

exports.component = (name) => (
`import React, { Component } from 'react';
import { connect } from 'react-redux';
import { actions } from '../'
class ${jsUcfirst(name)} extends Component {
    constructor(props) {
        super(props);
    }
    componentDidMount() {
        this.props.${name}Test('test');
    }
    render() {
        return (
            <div>test</div>
        )
    }
}
const mapStateToProps = null;
export default connect(mapStateToProps, { ...actions })(${jsUcfirst(name)});`
);


exports.page = (name) => {
const newName = name.split('_').map(word => (
    jsUcfirst(word)
))
return (`import React,  from 'react';

function ${newName.join('')} () {
    return (
        <div>${name}</div>
    )
}

export default ${newName.join('')};`
)};