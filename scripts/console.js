const fs = require('fs');
const appRoot = require('app-root-path');
const yargs = require('yargs');
const chalk = require('chalk');
const files = require('./lib/createFile');
const path = {
    modules: `${appRoot}/client/src/modules/`,
    views: `${appRoot}/client/src/views/`
};

function jsUcfirst(string)
{
    return string.charAt(0).toUpperCase() + string.slice(1);
}


yargs.command({
    command: 'module',
    describe: 'Add new module',
    builder: {
        name: {
            describe: 'module name',
            demandOption: true,
            type: 'string'
        }
    },
    handler: function (argv) {
        if(!fs.existsSync(path.modules)) {
            fs.mkdirSync(path.modules)
        }
        
        const moduleDir = `${path.modules}${argv.name}`;
        if(fs.existsSync(moduleDir)) {
            console.log(chalk.red('Module exist'));
        } else {
            fs.mkdirSync(moduleDir);
            fs.mkdirSync(`${moduleDir}/components`);
            fs.writeFileSync(`${moduleDir}/components/${jsUcfirst(argv.name)}.js`, files.component(argv.name));
            fs.writeFileSync(`${moduleDir}/actionCreators.js`, files.actionCreators(argv.name));
            fs.writeFileSync(`${moduleDir}/actionTypes.js`, files.actionTypes(argv.name));
            fs.writeFileSync(`${moduleDir}/index.js`, files.index(argv.name));
            fs.writeFileSync(`${moduleDir}/reducers.js`, files.reducers(argv.name));
        }
    }
})

// Create remove command
yargs.command({
    command: 'page',
    describe: 'add new view page',
    builder: {
        admin: {
            describe: 'if admin page',
            default: false,
            demandOption: true,
            type: 'boolean'
        },
        name: {
            describe: 'Page name',
            demandOption: true,
            type: 'string',
            require: true
        },
        type: {
            describe: 'Dir',
            demandOption: true,
            default: "page",
            type: 'string'
        },
        dir: {
            describe: 'Dir',
            demandOption: true,
            default: false,
            type: 'string'
        }
    },
    handler: function (argv) {
        if(!fs.existsSync(path.views)) {
            fs.mkdirSync(path.views)
            fs.mkdirSync(path.views + "admin/")
        }
        
        const pageDir = `${path.views}${argv.admin ? 'admin/' : ''}${argv.type}/${argv.dir ? argv.dir + "/" : ""}`;
        if(fs.existsSync(pageDir + '/' + argv.name + '.js')) {
            console.log(chalk.red('file exist'));
        } else {
            if(!fs.existsSync(pageDir)) {
                fs.mkdirSync(pageDir);
            }
            fs.writeFileSync(`${pageDir}${jsUcfirst(argv.name)}.js`, files.page(argv.name));
        }
    }
})

// Create list command
yargs.command({
    command: 'list',
    describe: 'List your notes',
    handler: function () {
        console.log('Listing out all notes')
    }
})

// Create read command
yargs.command({
    command: 'read',
    describe: 'Read a note',
    handler: function () {
        console.log('Reading a note')
    }
})

yargs.parse()


